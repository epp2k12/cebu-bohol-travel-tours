<?php
/**
 * @package Cebu Bohol Travel Tours
 * @subpackage CBTT
 * @since CBTT 1.0
 */
?>

<div class="clearfix"></div>
<div id="popular_group" class="clearfix">
<div class="mid_blue_title_bars" >MOST TOURED PACKAGES</div>
<?php
$args=array(
'cat' => 5,
'order' => ASC,
'posts_per_page' => -1
);
$packagePosts = new WP_Query( $args );

	if( $packagePosts->have_posts() ) {
	//loop through related posts based on the tag
		while ( $packagePosts->have_posts() ) :
			$packagePosts->the_post();
?>

		<div class="popularpost col-sm-2 col-xs-6">
			<a href="<?php the_permalink(); ?>">

			<?php

					$discount = get_post_meta( get_the_ID(), 'discount', true );
					$discount_percent = get_post_meta( get_the_ID(), 'is_discount_percent', true );
					if($discount > 0) {
						if($discount_percent == "true") {
							$promo_text = "<span class='discount_rate'>LESS ". $discount . "% </span> ";
						}else{
							$promo_text = "<span class='discount_rate'>LESS - P" . $discount . "</span>";
						}
					} else {
						$promo_text = "";
					}

			if ($discount > 0) {
			?>
				<div class="small_promo">
				<span class="label"><?php echo $promo_text; ?></span>
				<!--
				<img src="<?php bloginfo('stylesheet_directory')?>/img/small_captions/small_featured.jpg" alt="featured promo">
				-->
				</div>

			<?php }	?>
			<div class="populartbnail">
			<?php the_post_thumbnail(); ?>
			</div>
			<div class="populartitle">
			<?php the_title(); ?>
			<!-- <button type="button" class="btn btn-primary" style="float:right">Warning</button> -->
			</div>
			</a>
		</div>

		<?php endwhile; ?>
		<?php wp_reset_postdata(); ?>

	<?php } ?>
</div>

<div id="fast_booking" class="clearfix">

	<div id="fast_booking_status" ></div>
	<div class="col-sm-12 mid_blue_title_bars">
	FAST WAY TO BOOK YOUR TOUR!
	</div>

<form name="fast_booking" id="form_fast_booking" >

<div class="col-sm-6 form_fast_booking">
<h1>FILL-IN REQUIRED DETAILS</h1>

<div class="row">
<div class="col-xs-4 frow">
Name
</div>
<div class="col-xs-8">
<input type="text" class="form-control" name="firstname" placeholder="Enter Full Name" required>
</div>
</div>

<div class="row">
<div class="col-xs-4 frow">
Email
</div>
<div class="col-xs-8">
<input type="email" class="form-control" name="email" placeholder="Enter Best Email" required>
</div>
</div>

<div class="row">
<div class="col-xs-4 frow">
Contact
</div>
<div class="col-xs-8">
<input type="text" class="form-control" name="contact" placeholder="Enter Contact No.">
</div>
</div>

<div class="row">
<div class="col-xs-4 frow">
Pick-up Loc.
</div>
<div class="col-xs-8">
<input type="text" class="form-control" name="pick_up_location" placeholder="HOTEL/AIRPORT" required>
</div>
</div>

<div class="row">
<div class="col-xs-4 frow">
Pick-up Date
</div>
<div class="col-xs-8">
<input type="text" class="form-control" name="pick_up_date" placeholder="MM/DD/YYYY" required>
</div>
</div>

<div class="row">
<div class="col-xs-7 frow">
No. of Persons
</div>
<div class="col-xs-5">
<select name="no_of_persons" class="form-control">
<?php
  for($i=1;$i<=100;$i++) {
  echo "<option value='" . $i ."'>" .$i. "</option>";
  }
?>
</select>
</div>
</div>

<div class="row">
<div class="col-xs-7 frow">
Children 3 Below
</div>
<div class="col-xs-5">
<select name="no_of_children" class="form-control">
 <?php
  for($i=0;$i<=20;$i++) {
    echo "<option value='" . $i ."'>" .$i. "</option>";
  }
 ?>
</select>
</div>
</div>

<div class="row">
<div class="col-xs-7 frow">
No of Foreigners
</div>
<div class="col-xs-5">
<select name="no_of_foreign_guest" class="form-control">
 <?php
  for($i=0;$i<=100;$i++) {
  echo "<option value='" . $i ."'>" .$i. "</option>";
  }
 ?>
</select>
</div>
</div>

<div class="row">
<div class="col-xs-7 frow">
No of Days Tour
</div>
<div class="col-xs-5">
<select name="no_of_days_tour" class="form-control">
 <?php
  for($i=1;$i<=20;$i++) {
  echo "<option value='" . $i ."'>" .$i. "</option>";
  }
 ?>
</select>
</div>
</div>

</div>


<div class="col-sm-6 list_quick_booking">
<h1>SELECT YOUR DESIRED TOUR(S)</h1>
<ul class="">
<li>
<input name="tour1" type="checkbox" value="Oslob Whale-Tumalog-Kawasan Canyoneering" /> Oslob Whale-Tumalog-Kawasan Canyoneering
</li>

<li>
<input name="tour2" type="checkbox" value="Oslob Whale-Sumilon-Kawasan Canyoneering" /> Oslob Whale-Sumilon-Kawasan Canyoneering
</li>

<li>
<input name="tour3" type="checkbox" value="Oslob Whale-tumalog-Sumilon Island" /> Oslob Whale-tumalog-Sumilon Island
</li>
<li>
<input name="tour4" type="checkbox" value="Oslob Whale-Tumalog-Simala" /> Oslob Whale-Tumalog-Simala
</li>

<li>
<input name="tour5" type="checkbox" value="Mactan Island Hopping" /> Mactan Island Hopping
</li>

<li>
<input name="tour6" type="checkbox" value="Kawasan Falls-Canyoneering" /> Kawasan Falls-Canyoneering
</li>

<li>
<input name="tour7" type="checkbox" value="Cebu City Day Tour" /> Cebu City Day Tour
</li>

<li>
<input name="tour8"  type="checkbox" value="Bohol Country Side Tour" /> Bohol Country Side Tour
</li>

<li>
<input name="tour9"  type="checkbox" value="Cebu and Bohol 3D/2N Tour" /> Cebu and Bohol 3D/2N Tour
</li>

<li>
<input name="tour10"  type="checkbox" value="Oslob Overnight Budget" /> Oslob Overnight Budget</label>
</li>

<li>
<input name="tour11"  type="checkbox" value="Oslob Overnight Full Adventure" /> Oslob Overnight Full Adventure</label>
</li>

<li>
<input name="tour12"  type="checkbox" value="Kalanggaman Island Day Tour" /> Kalanggaman Island Day Tour</label>
</li>

<li>
<input name="tour13"  type="checkbox" value="Malapascua Island Day Tour" /> Malapascua Island Day Tour</label>
</li>

<li>
<input name="tour14"  type="checkbox" value="Overnight-Kalanggaman and Malapascua Island Tour" /> Overnight-Kalanggaman &amp; Malapascua Island Tour</label>
</li>

<li>
<input name="tour15"  type="checkbox" value="Pescador Island Hopping-Kawasan Canyoneering" /> Pescador Island Hopping-Kawasan Canyoneering</label>
</li>

<li>
<input name="tour16"  type="checkbox" value="4 Days 3 Nights Cebu Tour Package" /> 4 Days &amp; 3 Nights Cebu Tour Package</label>
</li>

<li>
<input name="tour17"  type="checkbox" value="5 Days 4 Nights Cebu Bohol Tour Package" /> 5 Days &amp; 4 Nights Cebu Bohol Tour Package</label>
</li>

<li>
<input name="tour18"  type="checkbox" value="6 Days 5 Nights Ultimate Cebu Bohol Tour Package" /> 6 Days &amp; 5 Nights Ultimate Cebu Bohol Tour
</li>

</ul>


</div>
<div class="col-sm-12">
<input id="easy_book" type="submit" class="form-control" value="SUBMIT BOOKING!">
</div>
</form>
</div>

<div id="customer_feedback" class="clearfix">

  <div id="myCustomers" class="col-sm-6">
    <img src="<?php bloginfo('stylesheet_directory')?>/img/customer_images/customer01.jpg" class="active" alt="cebu tours"/>
    <img src="<?php bloginfo('stylesheet_directory')?>/img/customer_images/customer02.jpg" alt="cebu tours"/>
    <img src="<?php bloginfo('stylesheet_directory')?>/img/customer_images/customer03.jpg" alt="cebu tours"/>
    <img src="<?php bloginfo('stylesheet_directory')?>/img/customer_images/customer04.jpg" alt="cebu tours"/>
    <img src="<?php bloginfo('stylesheet_directory')?>/img/customer_images/customer05.jpg" alt="cebu tours"/>
    <img src="<?php bloginfo('stylesheet_directory')?>/img/customer_images/customer06.jpg" alt="cebu tours"/>
    <img src="<?php bloginfo('stylesheet_directory')?>/img/customer_images/customer07.jpg" alt="cebu tours"/>
    <img src="<?php bloginfo('stylesheet_directory')?>/img/customer_images/customer10.jpg" alt="cebu tours"/>
    <img src="<?php bloginfo('stylesheet_directory')?>/img/customer_images/customer11.jpg" alt="cebu tours"/>
    <img src="<?php bloginfo('stylesheet_directory')?>/img/customer_images/customer12.jpg" alt="cebu tours"/>
  </div>

  <div id="customer_feedback_text" class="col-sm-6">
  <h3 class="mid_blue_title_bars">What our customers say:</h3>
  <p>"We had such a great time. The tour was very convenient and relaxing. Cozy van service and the package was great!
  Thank you Cebu Bohol Travel and Tours We had a blast, truly an adventure of a lifetime ..."</p>
  </div>

</div>

<div align="center">
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- cbtt_ads_01 -->
<ins class="adsbygoogle"
     style="display:inline-block;width:728px;height:90px"
     data-ad-client="ca-pub-6097410298968540"
     data-ad-slot="7345216483"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
</div>



	</div><!-- .inner-wrap -->
	</div><!-- #main -->



<div class="clearfix logo_portion">

<div class="col-sm-3 logo_others">
<a href="https://www.paypal.com/us/home">
<img src="<?php bloginfo('stylesheet_directory')?>/img/logo/paypal.jpg" alt="Pay with Paypal"/>
</a>
  </div>
  <div class="col-sm-3 logo_others">
  <a href="http://www.tourism.gov.ph/">
<img src="<?php bloginfo('stylesheet_directory')?>/img/logo/itsmorefun.jpg" alt="Philippines Tourism"/>
 </a>
  </div>
  <div class="col-sm-3 logo_others">
  <a href="https://whalesharkoslob.com/">
<img src="<?php bloginfo('stylesheet_directory')?>/img/logo/logo_label_whaleshark.jpg" alt="Cebu Bohol Travel and Tours"/>
</a>
  </div>
  <div class="col-sm-3 logo_others">
  <a href="http://canyoneeringcebutours.com/">
<img src="<?php bloginfo('stylesheet_directory')?>/img/logo/logo-mail.jpg" alt="Kawasan Canyoneering"/>
</a>
  </div>

</div>


	<?php do_action( 'spacious_before_footer' ); ?>
		<footer id="colophon" class="clearfix">
			<?php get_sidebar( 'footer' ); ?>
			<div class="footer-socket-wrapper clearfix">
				<div class="inner-wrap">
					<div class="footer-socket-area">
						<?php // do_action( 'spacious_footer_copyright' ); ?>
						<div class="footer_add_ons">
						<?php
							include('common/socials.php');
						?>
						<a href="https://cebuboholtraveltours.com/terms-condition/" target="_blank" class="btn btn-info" role="button">View and Download Terms and Condition</a> </div>
						<div class="footer_profile_company">Copyright &copy; <?php echo date("Y") ?> EA Cebu Tours and Travels. Powered by
						<a href="http://bawing.wordpress.com">EPP.</a></div>
						<nav class="small-menu clearfix">
							<?php
								if ( has_nav_menu( 'footer' ) ) {
										wp_nav_menu( array( 'theme_location' => 'footer',
																 'depth'           => -1
																 ) );
								}
							?>
		    			</nav>
					</div>
				</div>
			</div>
		</footer>
		<a href="#masthead" id="scroll-up"></a>

	</div><!-- #page -->
	<?php wp_footer(); ?>
</body>


<script type="text/javascript">

$(document).ready(function() {

	//triggers the kawasan image gallery ------------------------------------------------ image gallery
  	setInterval('swapImages("myCustomers")', 3000);
  	setInterval('swapImages("myGallery")', 5000);
  	setInterval('swapImages("quick_booking_instructions")', 5000);

    // initialize the date picker
      var date_input=$('input[name="pick_up_date"]'); //our date input has the name "date"
      var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
      var options={
        format: 'mm/dd/yyyy',
        container: container,
        todayHighlight: true,
        autoclose: true,
      };
      date_input.datepicker(options);

    // initialize the date picker
      var date_input=$('input[name="pick_date"]'); //our date input has the name "date"
      var container=$('.bootstrap-iso form').length>0 ? $('.bootstrap-iso form').parent() : "body";
      var options={
        format: 'mm/dd/yyyy',
        container: container,
        todayHighlight: true,
        autoclose: true,
      };
      date_input.datepicker(options);

submit_quick_booking();

});

</script>


</html>