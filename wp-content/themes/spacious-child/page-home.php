<?php
get_header();
?>

<div id="the_home_page">

  <div id="homeCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#homeCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#homeCarousel" data-slide-to="1"></li>
      <li data-target="#homeCarousel" data-slide-to="2"></li>
      <li data-target="#homeCarousel" data-slide-to="3"></li>
      <li data-target="#homeCarousel" data-slide-to="4"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
      <div class="item active">
        <img src="<?php bloginfo('stylesheet_directory')?>/img/home_carousel/home_carousel01.jpg" alt="Cebu Oslob Whale">
      </div>

      <div class="item">
        <img src="<?php bloginfo('stylesheet_directory')?>/img/home_carousel/home_carousel02.jpg" alt="Cebu Oslob Whale">
      </div>

      <div class="item">
        <img src="<?php bloginfo('stylesheet_directory')?>/img/home_carousel/home_carousel03.jpg" alt="Cebu Oslob Whale">
      </div>

      <div class="item">
        <img src="<?php bloginfo('stylesheet_directory')?>/img/home_carousel/home_carousel04.jpg" alt="Cebu Oslob Whale">
      </div>

      <div class="item">
        <img src="<?php bloginfo('stylesheet_directory')?>/img/home_carousel/home_carousel05.jpg" alt="Cebu Oslob Whale">
      </div>

    </div>
  </div>

<div id="tour_locations_containter" class="col-sm-7">

		<div id="promo_box1">
		<h3>PROMO FOR THIS MONTH<br/>
		SELECTED CEBU TOURS<br/>
		IS NOW LESS 5%<br/>
		SO HURRY BOOK NOW!</h3>
		</div>

	<div class="home_butt_containers">

	<div class="col-xs-12 col-sm-5 button_images">
	<a href="https://cebuboholtraveltours.com/cebu-home-page/">
	<img src="<?php bloginfo('stylesheet_directory')?>/img/home_images/buttons/cebu.jpg" alt="cebu tours"></a>
	</div>

	<div class="location_body col-xs-12 col-sm-7">
	<h1>CEBU TOUR</h1>
	Canyoneering tours with Kawasan Falls, whale shark oslob watching and snorkeling, Sumilon island sand bars, tumalog falls, mactan island hopping, kalanggaman island, malapascua island, pescador island hopping with sadines run and turtle watching, moalboal swimming diving, cebu city tours with temple of lea, 10,000 roses, tops, sirao flower garden, Bohol countryside tour with tarsier, chocolate hills and loboc river lunch buffet and more. Affordable and discounted rates.
	</div>

	<div class="col-xs-12 col-sm-12">
	<a href="https://cebuboholtraveltours.com/cebu-home-page/" class="btn btn-primary btn-lg btn-home-page" role="button">
	BEST DAY TOURS</a>
	</a>
	</div>

	</div> <!-- home_butt_containers -->

	<div class="home_butt_containers">
	<div class="col-xs-12 col-sm-5 button_images">
	<a href="https://cebuboholtraveltours.com/overnight-packages/">
	<img src="<?php bloginfo('stylesheet_directory')?>/img/home_images/buttons/overnight.jpg" alt="cebu bohol tours"></a>
	</div>

	<div class="location_body col-xs-12 col-sm-7">
	<h1>OSLOB WHALE SHARK &amp; CANYONEERING TOUR (OVERNIGHTS)</h1>
	Overnight and multi-day tours with Canyoneering Kawasan Falls, oslob whale shark cebu watching and snorkeling, Sumilon island sand bars, tumalog falls, mactan island hopping, kalanggaman island, malapascua island, pescador island hopping with sadines run and turtle watching, moalboal swimming diving, cebu city tours with temple of lea, 10,000 roses, tops, sirao flower garden, Bohol countryside tour with tarsier, chocolate hills and loboc river lunch buffet and more. Affordable and discounted rates.
	</div>

	<div class="col-xs-12 col-sm-12">
	<a href="https://cebuboholtraveltours.com/overnight-packages/" class="btn btn-primary btn-lg btn-home-page" role="button">
	OVERNIGHT TOUR</a>
	</a>
	</div>

	</div> <!-- home_butt_containers -->

	<div class="home_butt_containers">

	<div class="col-xs-12 col-sm-5 button_images">
	<a href="http://cebuboholtraveltours.com/bohol-tours-and-travels/">
	<img src="<?php bloginfo('stylesheet_directory')?>/img/home_images/buttons/bohol.jpg" alt="cebu bohol oslob whale tours"></a>
	</div>

	<div class="location_body col-xs-12 col-sm-7">
	   <h1>BOHOL TOUR</h1>
	   Blood Compact Site, Baclayon Church, St. Peter's Ruins Church, Bohol Biggest Snake in Captivity, River Cruise Floating Restaurant with Buffet Lunch,
	   Bilar Man-made Forest, Tarsier Visit, Chocolate Hills and Adventure Rides (Bike Zip &amp; ATV Ride), Butterfly Garden &amp; many more ...
	</div>

	<div class="col-xs-12 col-sm-12">
	<a href="http://cebuboholtraveltours.com/bohol-tours-and-travels/" class="btn btn-primary btn-lg btn-home-page" role="button">
	BOHOL TOUR</a>
	</a>
	</div>

	</div> <!-- home_butt_containers -->



	<div class="home_butt_containers">
	<div class="mid_blue_title_bars" ><h2>CEBU TOURS PROMO</h2></div>

		<?php
		$args=array(
		'cat' => 6,
		'order' => DESC
		);
		$packagePosts = new WP_Query( $args );
			if( $packagePosts->have_posts() ) {
			//loop through related posts based on the tag
				while ( $packagePosts->have_posts() ) :
					$packagePosts->the_post();
		?>



	<div class="home_promotions clearfix">
		<div class="col-xs-12 col-sm-5 home_items promo_images">
		<a href="<?php the_permalink(); ?>" >
					<?php the_post_thumbnail(); ?>
		</a>
		</div>
		<div class="col-xs-12 col-sm-7 location_body">
					<h1><?php the_title(); ?></h1>
					<?php the_excerpt(); ?>
		</div>
		<div class="col-xs-12 col-sm-12 home_items">
					<a href="<?php the_permalink(); ?>" class="btn btn-info btn-lg btn-home-page" role="button">
					<?php
					$promo_text = "";
					$discount = get_post_meta( get_the_ID(), 'discount', true );
					$discount_percent = get_post_meta( get_the_ID(), 'is_discount_percent', true );
					if($discount > 0) {
						if($discount_percent == "true") {
							$promo_text = "<span class='discount_rate'>LESS ". $discount . "% </span>- ";
						}else{
							$promo_text = "<span class='discount_rate'>LESS - P" . $discount . "</span>";
						}
					} else {
						$promo_text = "";
					}
					?>
					<?php echo $promo_text; ?> BOOK NOW </a>
		</div>

	</div>

				<?php endwhile; ?>
				<?php wp_reset_postdata(); ?>
			<?php } ?>
	</div>

</div>

<div class="home_side_portion col-sm-5">

<div class="page_home_side_containter">
<div class="mid_blue_title_bars" >SEARCH DESTINATION</div>

<div class="search_form_home">
<form action="http://cebuboholtraveltours.com/" class="search-form searchform clearfix" method="get">
	<div class="search-wrap">
		<input type="text" placeholder="Search" class="s field search_input_home" name="s">
		<button class="search-icon" type="submit"></button>
	</div>
</form>
</div>

</div> <!-- page_home_side_containter -->
<div class="page_home_side_containter">

<?php
	include('common/socials.php');
?>

<!--
  <div id="rent_a_car" style="overflow:auto;">
  <table class="table table-condensed">
  <tr><th colspan="2" style="color:#ffffff;background-color:#000000;font-size:1.2em">FOR CAR RENTAL - <span style="color:#F5FFB3">BEST DEALS</span></th></tr>
  <tr><td>
  <div style="text-align:center">
	<a href="http://earentacarcebu.com/">
	<img src="<?php bloginfo('stylesheet_directory')?>/img/home_images/logo7.png">
	</a>
	</div>
  <div id="myGallery" style="margin:0 auto">
    <img src="<?php bloginfo('stylesheet_directory')?>/img/cars/car01.jpg" class="active" />
    <img src="<?php bloginfo('stylesheet_directory')?>/img/cars/car02.jpg" />
    <img src="<?php bloginfo('stylesheet_directory')?>/img/cars/car03.jpg" />
    <img src="<?php bloginfo('stylesheet_directory')?>/img/cars/car04.jpg" />
  </div>
  </td></tr>
  <tr><td>
   <p style="font-size:.8em;padding:5px;text-align:center;line-height:1.2em">
    CEBU's MOST AFFORDABLE VAN/CAR RENTAL<br/>
  All Van and Car Rentals start at Php 1,200 for the first 3 hours.
  </p>
  </td></tr>
  <tr><td style="text-align:center;"><a href="http://earentacarcebu.com/" class="btn btn-danger" role="button">BOOK YOUR RIDE NOW</a></td></tr>
  </table>
  </div>
-->
</div> <!-- page_home_side_containter -->
<div class="page_home_side_containter">
	<div class="contact_numbers">
		<div class="mid_blue_title_bars">Contact Us</div>
		<h6><span class="label label-warning"><span class="glyphicon glyphicon-phone"></span> +63 917 704 3508</span></h6>
		<h6><span class="label label-warning"><span class="glyphicon glyphicon-phone"></span> +63 916 696 5614</span></h6>
		<h6><span class="label label-warning"><span class="glyphicon glyphicon-envelope"></span> support@cebuboholtraveltours.com</span></h6>
		<div class="whatsup_section">
			<div class="col-xs-5 whatsup_section_logo">
			<img src="<?php bloginfo('stylesheet_directory')?>/img/logo/whatsapp-icon.png" alt="whatsup">
			</div>
			<div class="col-xs-7 whatsup_section_numbers"><strong>
				WhatsApp :<br/> </strong>
				+639177043508<br/>
				+639166965614<br/>
			</div>
		</div>
		<div class="contact_us_button">
		<a href="https://cebuboholtraveltours.com/contact-us/" class="btn btn-danger" role="button">CONTACT US!</a>
		</div>
	</div>
</div> <!-- page_home_side_containter -->

<!--
<div id="page_home_side_containter">
<iframe width="560" height="300" src="https://www.youtube.com/embed/xoePM7uj-B4?autoplay=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
<iframe width="560" height="300" src="https://www.youtube.com/embed/ADNgEHFDYzo?autoplay=0" frameborder="0" allowfullscreen></iframe>
</div>
-->
<!-- page_home_side_containter -->

<div class="page_home_side_containter">
<iframe width="560" height="315" src="https://www.youtube.com/embed/ARIrBTfUnz0?autoplay=1" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
</div>
<div class="page_home_side_containter">
<iframe width="560" height="300" src="https://www.youtube.com/embed/ADNgEHFDYzo?autoplay=0" frameborder="0" allowfullscreen></iframe>
</div>

<div class="page_home_side_containter" class="recent_news">
<div class="mid_blue_title_bars" >RECENT NEWS ...</div>
<?php
		$args=array(
		'cat' => 8,
		'posts_per_page'=> 1
		);
		$blogPosts = new WP_Query( $args );
			if( $blogPosts->have_posts() ) {
			//loop through related posts based on the tag
				while ( $blogPosts->have_posts() ) {

					$blogPosts->the_post();

						echo '<a href="'. get_the_permalink() .'">';
						the_post_thumbnail();
						echo '</a>';

						echo '<br/>';
						echo '<div class="dark_blue_title_bars">';
						echo '<a href="'. get_the_permalink() .'">';
						the_title();
						echo '</a>';
						echo '</div>';

						the_content();


				}
			}
		wp_reset_postdata();
		echo "<div class='mid_blue_title_bars'>OTHER NEWS</div>";
			$args=array(
			'cat' => 8,
			'offset' => 1,
			'posts_per_page'=> 6
			);
			$blogPosts = new WP_Query( $args );
				if( $blogPosts->have_posts() ) {
				//loop through related posts based on the tag
					echo '<ul class="other_news_ul">';

					while ( $blogPosts->have_posts() ) {

						$blogPosts->the_post();
							echo '<a href="'. get_the_permalink() .'">';
							echo '<li><strong>';
							the_title();
							echo '</strong></li>';
							echo '</a>';
					}
					echo '</ul>';
				}
?>
</div>

<div class="page_home_side_containter">
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- cbtt_ads_02 -->
<ins class="adsbygoogle"
	 style="display:block"
     data-ad-client="ca-pub-6097410298968540"
     data-ad-slot="6884506898"
     data-ad-format="auto"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
</div> <!-- page_home_side_containter -->
</div> <!-- home_side_portion -->

<div id="TA_rated760" class="TA_rated"><ul id="r8bW4Wo" class="TA_links 3xyrAvLKlKyH"><li id="mAriMoPvxQ9H" class="Rj1ehuOP2Nm"><a target="_blank" href="https://www.tripadvisor.com/"><img src="https://www.tripadvisor.com/img/cdsi/img2/badges/ollie-11424-2.gif" alt="TripAdvisor"/></a></li></ul></div>
<script async src="https://www.jscache.com/wejs?wtype=rated&amp;uniq=760&amp;locationId=12938127&amp;lang=en_US&amp;display_version=2"></script>

<?php // spacious_sidebar_select(); ?>
<?php get_footer(); ?>
