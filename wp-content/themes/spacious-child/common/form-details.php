        <tr><td colspan="2" style="background-color:gray; color:white;">Personal Information (Fill-up required details)</td></tr>
        <tr><td style="width:30%">
        Firstname <span class="required">*</span>
        </td>
        <td style="width:70%">
        <input type="text" class="form-control" name="first_name" minlength="2" id="first_name" placeholder="Enter Firstname" required>
        </td>
        </tr>
        <tr><td>
        Lastname: <span class="required">*</span>
        </td><td>
        <input type="text" class="form-control" name="last_name" id="last_name" placeholder="Enter Lastname" required>
        </td></tr>
        <tr><td>
        Contact: <span class="required">*</span>
        </td><td>
        <input type="text" class="form-control" name="contact" id="contact" placeholder="Enter Contact Number" required>
        </td></tr>
        <tr><td>
        Email: <span class="required">*</span>
        </td><td>
        <input type="email" class="form-control" name="email" id="email" placeholder="Enter Best Email" required>
        </td></tr>
        <tr><td>
        Pick Up Location: <span class="required">*</span>
        </td><td>
        <input type="text" class="form-control" name="pick_location" id="pick_location" placeholder="Airport/Hotel" required>
        </td></tr>
        <tr><td>
        Pick Up Date: <span class="required">*</span>
        </td><td>
        <input type="text" class="form-control" name="pick_date" id="pick_date" placeholder="mm/dd/yyyy" required>
        </td></tr>
        <tr><td>
        Pick Up Time: <span class="required">*</span>
        </td><td>
        <input type="text" class="form-control" name="pick_time" id="pick_time" placeholder="08:00 AM" required>
        </td></tr>
        <tr><td colspan="2">
        Additional Information
        <textarea class="form-control noresize" name="other_info" id="other_info"  rows="3" placeholder="Additional Info"></textarea>
        </td></tr>