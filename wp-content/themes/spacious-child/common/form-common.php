  <form id="form_compute">
  <input type="hidden" name="submitted" value="true">
  <input type="hidden" name="reservation_mode" id="reservation_mode" value="">
  <input type="hidden" name="other_payment_details" id="other_payment_details" value="">
  <input type="hidden" name="children_discount" id="children_discount" value="">

  <div id="form_compute_status"></div>
  <table class="table table-condensed table_compute" id="table_compute" >

  <tr><td colspan="4" style="background-color:#000000;color:#ffffff" >
  <input type="text" class="hilite_value" name="tour_name" id="tour_name" readonly style="background-color:#000000;color:#ffffff;border:none;">
  </td></tr>

        <tr><td colspan="4" style="background-color:gray; color:white;">Personal Information (Fill-up required details)</td></tr>
        <tr><td colspan="2">
        Full Name <span class="required">*</span>
        </td>
        <td colspan="2">
        <input type="text" class="form-control compute_form_required_control" name="first_name" id="first_name" placeholder="Enter First Name" required>
        </td>
        </tr>
        <tr><td colspan="2">
        Contact: <span class="required">*</span>
        </td><td colspan="2">
        <input type="text" class="form-control compute_form_required_control" name="contact" id="contact" placeholder="Enter Contact Number" required>
        </td></tr>
        <tr><td colspan="2">
        Email: <span class="required">*</span>
        </td><td colspan="2">
        <input type="email" class="form-control compute_form_required_control" name="email" id="email" placeholder="Enter Best Email" required>
        </td></tr>
        <tr><td colspan="2">
        Pick Up Location: <span class="required">*</span>
        </td><td colspan="2">
        <input type="text" class="form-control compute_form_required_control" name="pick_location" id="pick_location" placeholder="Airport/Hotel" required>
        </td></tr>
        <tr><td colspan="2">
        Pick Up Date: <span class="required">*</span>
        </td><td colspan="2">
        <input type="text" class="form-control compute_form_required_control" name="pick_date" id="pick_date" placeholder="mm/dd/yyyy" required>
        </td></tr>
        <tr><td colspan="4">
        Additional Information
        <textarea class="form-control noresize" name="other_info" id="other_info"  rows="3" placeholder="Additional Info"></textarea>
        </td></tr>

  <tr><td colspan="2"><span class="impt_text" style="font-style:strong;color:yellow">NO. OF PERSONS</td>
  <td colspan="2">
    <select name="no_of_persons" id="no_of_persons" style="width:100%">
    </select>
  </td>
  </tr>
  <tr class="children_row"><td colspan="2">CHILDREN(3-7 YO)</td>
  <td colspan="2">
    <select name="no_of_children" id="no_of_children" style="width:100%">
    </select>
  </td>
  </tr>
  <tr><td colspan="4" style="padding:14px">
    <p style="text-align:center;padding:0px;margins:0px;border:1px solid orange">ADD-ONS AND DISCOUNTS</p>
    <input type="checkbox" name="add_on_head" id="full_include_meals" description="INCLUDE MEALS" value="1200">
    <label for="full_include_meals" style="display:inline" >INCLUDE MEALS</label><br/>
 <!--
   <input type="checkbox" name="add_on_head" id="tops_temple_of_lea" description="Add-on of 400/px (TEMPLE OF LEA/TOPS)" value="400">
    <label for="add_on_head" style="display:inline" >Add-on of 400/px (TEMPLE OF LEA/TOPS) </label><br/>
     <input type="checkbox" name="add_on_head" id="tops_temple_of_lea" description="Add-on 1000/px (Canyoneering)" value="1000" >
    <label for="add_on_head" style="display:inline">Add-on 1000/px (Canyoneering)</label><br/>
    <input type="checkbox" name="add_on_total" id="tops_temple_of_lea" description="Add-on 500 (Bamboo Shade)" value="500">
    <label for="add_on_total" style="display:inline">Add-on 500 (Bamboo Shade)</label><br/>
      <input type="checkbox" name="add_on_head" id="tops_temple_of_lea" value="1">
    <label for="tops_temple_of_lea" style="display:inline">add 1000 head</label><br/>
      <input type="checkbox" name="add_on_total" id="tops_temple_of_lea" value="2">
    <label for="tops_temple_of_lea" style="display:inline">add 400 total</label><br/>
      <input type="checkbox" name="add_on_total" id="tops_temple_of_lea" value="3">
    <label for="tops_temple_of_lea" style="display:inline">add 1000 total</label><br/>
      <input type="checkbox" name="discount_head" id="tops_temple_of_lea" value="4">
    <label for="tops_temple_of_lea" style="display:inline">less 400 head</label><br/>
      <input type="checkbox" name="discount_head" id="tops_temple_of_lea" value="5">
    <label for="tops_temple_of_lea" style="display:inline">less 1000 head</label><br/>
      <input type="checkbox" name="discount_total" id="tops_temple_of_lea" value="6">
    <label for="tops_temple_of_lea" style="display:inline">less 400 total</label><br/>
        <input type="checkbox" name="discount_total" id="tops_temple_of_lea" value="7">
    <label for="tops_temple_of_lea" style="display:inline">less 1000 total</label><br/>
    -->
  </td></tr>
  <tr><td colspan="4">



    <?php
    $discount = get_post_meta( get_the_ID(), 'discount', true );
    $discount_percent = get_post_meta( get_the_ID(), 'is_discount_percent', true );
    if($discount>0) {
    echo "THE DISCOUNT " . $discount . "<br/>";
    echo "<div id='promo_box1'>PROMO FOR THIS MONTH";
    echo "<h4 style='margin:0px;padding:0px'>PER HEAD IS LESS ";
    echo $discount;
	  echo ($discount_percent=='true')?'%':'';
    echo "!</h4>";
    echo "</div>";
    }
    ?>
    <style type="text/css">
    .dark {
      background-color:#000000;
    }

    </style>

	  <tr class="dark"><td colspan="4" style="text-align:center"><span style="font-size:1.1em;color:#fdffc9">SAMPLE RATE CALCULATIONS (PHP)</span></td></tr>
	  <tr class="dark"><td>&nbsp;</td><td>RATE/HEAD

    <?php
    if($discount>0) {
    echo "<br/><span style='color:yellow'>(";
    echo $discount;
    echo ($discount_percent=='true')?'%':'';
    echo  " OFF)";
    echo  "</span>";
    }
    ?>

	  </td><td>NO.</td><td>TOTAL</td></tr>
	  <tr class="dark">
	  <td>ADULT</td>
	  <td><input type="text" class="hilite_value" name="price_head_adult" id="price_head_adult" readonly></td>
	  <td><input type="text" class="hilite_value" name="no_of_adults" id="no_of_adults" readonly></td>
	  <td><input type="text" class="hilite_value" name="total_price_adult" id="total_price_adult" readonly></td>
	  </tr>
	  <tr class="children_row dark">
	  <td>CHILDREN</td>
	  <td><input type="text" class="hilite_value" name="price_head_children" id="price_head_children" readonly></td>
	  <td><input type="text" class="hilite_value" name="no_of_3to7" id="no_of_3to7" readonly></td>
	  <td><input type="text" class="hilite_value" name="total_price_children" id="total_price_children" readonly></td>
	  </tr>
	  <tr class="children_row dark">
	  <td colspan="3">SUB TOTAL</td>
	  <td><input type="text" class="hilite_value" name="sub_total" id="sub_total" readonly></td>
	  </tr>
	  <tr class="dark">
	  <td colspan="3">Total Add-On</td>
	  <td><input type="text" class="hilite_value" name="total_addon" id="total_addon" readonly></td>
	  </tr>
	  <tr class="dark">
	  <td colspan="3">Total Discount</td>
	  <td><input type="text" class="hilite_value" name="total_less" id="total_less" readonly></td>
	  </tr>
	  <tr class="dark"><td colspan="2" style="text-align:right">TOTAL PRICE</td><td colspan="2"><input type="text" class="hilite_value" name="computed_total_price" id="computed_total_price" readonly style="font-size:1.2em;text-align:right"></td></tr>


  </td></tr>

  <tr class="dark"><td colspan="4" style="text-align:center">
  <input  type="submit" value="RESERVE ONLY NOW" id="reserve_14"><br/>
  </td></tr>

  </table>
  </form>

<div id="page_home_side_containter">
  <div class="contact_numbers">
    <div class="mid_blue_title_bars">Contact Us</div>
    <h6><span class="label label-warning"><span class="glyphicon glyphicon-phone"></span> +63 917 704 3508</span></h6>
    <h6><span class="label label-warning"><span class="glyphicon glyphicon-phone"></span> +63 916 696 5614</span></h6>
    <h6><span class="label label-warning"><span class="glyphicon glyphicon-envelope"></span> support@cebuboholtraveltours.com</span></h6>
    <div class="whatsup_section">
      <div class="col-xs-5 whatsup_section_logo">
      <img src="<?php bloginfo('stylesheet_directory')?>/img/logo/whatsapp-icon.png" alt="whatsup">
      </div>
      <div class="col-xs-7 whatsup_section_numbers"><strong>
        WhatsApp :<br/> </strong>
        +639177043508<br/>
        +639166965614<br/>
      </div>
    </div>
      <div class="contact_us_button">
      <!-- <button type="button" class="btn btn-danger" onclick="window.location='http://cebuboholtraveltours.com/?p=19'">CONTACT US!</button> -->
      <a href="http://cebuboholtraveltours.com/?p=19" class="btn btn-danger" role="button">CONTACT US!</a>
      </div>
  </div>
</div> <!-- page_home_side_container -->

<div id="page_home_side_containter">
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<!-- cbtt_ads_02 -->
<ins class="adsbygoogle"
     style="display:block"
     data-ad-client="ca-pub-6097410298968540"
     data-ad-slot="6884506898"
     data-ad-format="auto"></ins>
<script>
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
</div> <!-- page_home_side_containter -->
