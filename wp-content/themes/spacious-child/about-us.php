<?php
/*
Template Name: who-we-are
*/
get_header();
?>




<style type="text/css">


</style>


<div class="col-sm-8">

<span class='st_facebook_large' st_title='<?php the_title(); ?>' st_url='<?php the_permalink(); ?>'></span>
<span st_via='https://twitter.com/EACebuTours' st_username='https://twitter.com/EACebuTours' class='st_twitter_large' st_title='<?php the_title(); ?>' st_url='<?php the_permalink(); ?>'></span>
<span class='st_linkedin_large' st_title='<?php the_title(); ?>' st_url='<?php the_permalink(); ?>'></span>
<span class='st_email_large' st_title='<?php the_title(); ?>' st_url='<?php the_permalink(); ?>'></span>
<span class='st_sharethis_large' st_title='<?php the_title(); ?>' st_url='<?php the_permalink(); ?>'></span>
<span class='st_fblike_large' st_title='<?php the_title(); ?>' st_url='<?php the_permalink(); ?>'></span>
<span class='st_plusone_large' st_title='<?php the_title(); ?>' st_url='<?php the_permalink(); ?>'></span>
<span class='st_pinterest_large' st_title='<?php the_title(); ?>' st_url='<?php the_permalink(); ?>'></span>

  <div class="mid_blue_title_bars">ABOUT US</div>
  <div id="myOwners">
    <img src="<?php bloginfo('stylesheet_directory')?>/img/about_us/owner1.jpg" width="60%" class="active" />
  </div>

<p>
<strong>Cebu Bohol Travel and Tours</strong> is a company of EA Tours and Travels which is owned
by a dynamic and travel geek person, Alvin M. He formed EA Tours and Travels about 5 years ago with its
aim to help travelers with their transportation needs ( van/car rental ).
He first made <a href="http://earentacarcebu.com/">EA Rent a Car</a> which is a site that caters
to most of your (car/van) transportation needs. With it's fleet of new
vehicles and very reliable drivers they were able to help both local and
foreign clients with its safe and enjoyable travels within Cebu, Bohol and neighboring provinces like Negros Oriental and Negros Occidental.
<br><br>
With the booming tourism, EA group has always been actively participating with servicing tourists and locals as they travel to every wonderful places in Cebu and Bohol. With this, <strong>"Cebu Bohol Travel and Tours"</strong> was formed and over the years has grown with staff that are caring and experts in promoting, interacting and assisting thousands of tourists from different contries each year. Now, with a collective experience of 5 years in travel and tours and almost 15 years in Car Rental Business we can assure you of a safe, convenient, enjoyable and most of all very affordable package tours in Cebu and Bohol.
<br><br>
One thing we emphasize when tourists tour with <strong>Cebu Bohol Travel and Tours</strong>, is -- quality and memorable experience!
</p>
</div>

<!-- packgroup -->



<div class="col-sm-4">
<?php
include 'common/side-bar-pages.php';
?>
</div>



<script type="text/javascript">
$(document).ready(function(){

	setInterval('swapImages("myGallery")',3000);
	$('.white_box').hover(function(){
		$(this).css(
		{
			'background-color': 'rgba(0, 0, 255, 0.7)'
		}

		);
		$(this).find('p').css (
		{
			'color':'#ffffff'
		}
		);
	},function(){
		$(this).css(
		{
			'background-color': 'rgba(255, 255, 255, 0.3)'
		}

		);
		$(this).find('p').css (
		{
			'color':'#000000'
		}
		);
	});

});

</script>

<?php // spacious_sidebar_select(); ?>
<?php get_footer(); ?>
