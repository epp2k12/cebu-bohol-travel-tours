<?php
$discount = get_post_meta( get_the_ID(), 'discount', true );
$discount_percent = get_post_meta( get_the_ID(), 'is_discount_percent', true );
?>

<div class="pack_container">
<div class="col-sm-7 parts_main">

    <?php
    if($discount>0) {
    echo "<div id='promo_box1'>PROMO FOR THIS MONTH";
    echo "<h4 style='margin:0px;padding:0px'>PER HEAD IS LESS ";
    echo $discount;
	  echo ($discount_percent=='true')?'%':'';
    echo "!</h4>";
    echo "</div>";
    }
    ?>

<div class="mid_blue_title_bars" >MACTAN ISLAND HOPPING
<div style="font-size:.7em;">Island Destinations : NALUSUAN, HILUTUNGAN and CAOHAGAN islands
<br/>
<strong style="color:yellow">NOTE : HILUTUNGAN Island ( We only dock near the island for fish feeding, swimming &amp; snorkelling)</strong>
</div>
</div>
  <div id="tour_Gallery" class="clearfix">
    <img src="<?php bloginfo('stylesheet_directory')?>/img/mactan_page/mactan01.jpg" class="active" alt="mactan island hopping cebu tours"/>
    <img src="<?php bloginfo('stylesheet_directory')?>/img/mactan_page/mactan02.jpg" alt="mactan island hopping cebu tours"/>
    <img src="<?php bloginfo('stylesheet_directory')?>/img/mactan_page/mactan03.jpg" alt="mactan island hopping cebu tours"/>
    <img src="<?php bloginfo('stylesheet_directory')?>/img/mactan_page/mactan04.jpg" alt="mactan island hopping cebu tours"/>
    <img src="<?php bloginfo('stylesheet_directory')?>/img/mactan_page/mactan05.jpg" alt="mactan island hopping cebu tours"/>
    <img src="<?php bloginfo('stylesheet_directory')?>/img/mactan_page/mactan06.jpg" alt="mactan island hopping cebu tours"/>
    <img src="<?php bloginfo('stylesheet_directory')?>/img/mactan_page/mactan07.jpg" alt="mactan island hopping cebu tours"/>
    <img src="<?php bloginfo('stylesheet_directory')?>/img/mactan_page/mactan08.jpg" alt="mactan island hopping cebu tours"/>
    <img src="<?php bloginfo('stylesheet_directory')?>/img/mactan_page/mactan09.jpg" alt="mactan island hopping cebu tours"/>
  </div>
<br/>

<div class="panel-group clearfix" id="accordion" >
        <div class="panel panel-default" >
              <div class="panel-heading">
                  <h4 class="panel-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">RATE CHART</a>
                  </h4>
              </div>
              <div id="collapseOne" class="panel-collapse collapse in">
                <div class="panel-body tour_details">
                  <table class="table table-condensed">
                  <tr><td style="width:20%"><strong>No. of Person(s)</strong></td>
                  <td style="width:40%">Price per Head (Hopping at Nalusuan, Hilutungan and Caohagan islands)</td>
                  </tr>
                  <tr><td>1</td><td>6,100/head</td></tr>
                  <tr><td>2</td><td>4,150/head </td></tr>
                  <tr><td>3</td><td>3,500/head</td></tr>
                  <tr><td>4</td><td>3,300/head</td></tr>
                  <tr><td>5</td><td>3,200/head </td></tr>
                  <tr><td>6</td><td>3,100/head</td></tr>
                  <tr><td>7</td><td>3,000/head</td></tr>
                  <tr><td>8</td><td>2,900/head </td></tr>
                  <tr><td>9</td><td>2,800/head</td></tr>
                  <tr><td>10</td><td>2,700/head</td></tr>
                  <tr><td>11</td><td>2,600/head</td></tr>
                  <tr><td>12</td><td>2,500/head</td></tr>
                  <tr><td>13</td><td>2,400/head</td></tr>
                  <tr><td>14 and Above</td><td>Contact us for the price quotation</td>
                  </tr>
                  <tr><td colspan="2">
                  <strong>NOTE:</strong><br/>HILUTUNGAN Island ( We only dock near the island for fish feeding, swimming &amp; snorkelling)<br/><br/>
                    <strong>Children / Kids Pricing</strong>
                    <ul>
                    <li>Child below 2 years old - FREE/Not Included in Head Count</li>
                    <li>Child 3 to 5 years old - Less 20%</li>
                    <li>Child above 5 years old - Full Charge</li>
                    </ul>
                  </td></tr>
                  </table>
                </div>
              </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">PACKAGE INCLUSIONS</a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse">
                <div class="panel-body tour_details">
                  <table class="table table-condensed">
                  <tr><td colspan="2">
                  Entrance fees<br>
                  Lunch<br>
                  Life Jacket <br>
                  Fully air-conditioned Car or Van Service <br>
                  Boat service (Motorized Banca/Pump Boat)<br>
                  </td></tr>
                  </table>
                </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">PACKAGE EXCLUSIONS</a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse">
                <div class="panel-body tour_details">
                  <table class="table table-condensed">
                  <tr><td colspan="2">
                  Breakfast and Dinnter<br>
                  Snacks<br>
                  Underwater Camera<br>
                  Accommodations<br/>
                  Air Fare<br/>
                  </td></tr>
                  </table>
                </div>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">ITINERARY</a>
                </h4>
            </div>
            <div id="collapseFour" class="panel-collapse collapse">
                <div class="panel-body tour_details">
                  <table class="table table-condensed">
                  <tr><td colspan="2">Recommended pick up time from Metro Cebu, Mandaue and Lapu-lapu City between 6:00AM to 9:00AM</td></tr>
                  <tr><td style="width:35%">8:00 AM</td><td style="width:65%">Pick up and departure time from Hotel </td></tr>
                  <tr><td>9:00 AM</td><td> Arrival at Mactan and departure time </td></tr>
                  <tr><td>9:00 AM </td><td> Tour at islands, swimming and snorkeling  </td></tr>
                  <tr><td>1200 NN to 1:30 PM </td><td> Lunch and cool off  </td></tr>
                  <tr><td>2:00PM </td><td> back to Mactan  </td></tr>
                  <tr><td>2:30 PM </td><td> depart back to Hotel  </td></tr>
                  <tr><td>3:30 PM </td><td> Arrival at hotel </td></tr>
                  <tr><td colspan="2">Island Tour duration is maximum for 10 hrs. In case you want to extend/side trip (ex: Cebu City Tour), we charge P 300.00/hr.</td></tr>
                  </table>
                </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive">DAY TOUR SERVICE</a>
                </h4>
            </div>
            <div id="collapseFive" class="panel-collapse collapse">
                <div class="panel-body tour_details">
                <table class="table table-condensed">
                <tr><td colspan="2">1 Day private tour (8-10 hours duration)</td></tr>
                <tr><td colspan="2">Boatman as guide</td></tr>
                <tr><td colspan="2">Fully air-conditioned vehicle service</td></tr>
                 <tr><td colspan="2">
                 <ul>
                 <li>1 to 3 persons &#45; sedan type vehicle</li>
                 <li>4 to 6 persons &#45; minivan type vehicle</li>
                 <li>7 to 13 persons &#45; van type vehicle</li>
                 <li>14 and above &#45; 2 vehicles or it depends on the number of heads</li>
                 </ul>
                 </td></tr>
                </table>
                </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseSix">CHECKLIST FOR ISLAND HOPPING</a>
                </h4>
            </div>
            <div id="collapseSix" class="panel-collapse collapse">
                <div class="panel-body tour_details">
                <table class="table table-condensed">
                <tr><td colspan="2">
                Bring extra shorts and shirts <br>
                Sandals or boots <br>
                Extra bottled water <br>
                Camera / underwater camera<br>
                Snorkeling gear if you have<br>
                </td></tr>
                </table>
                </div>
            </div>
          </div>

</div> <!-- accordion -->


</div> <!-- col-sm-7 -->
<div class="col-sm-5 parts_side" >
<!-- ####################################### starts here #################################### -->
<?php get_template_part('common/form','common'); ?>
<!-- ####################################### ends here #################################### -->
</div> <!-- col-sm-5 -->
</div> <!-- pack container -->

  <script type="text/javascript">
  var package_tour_name = "MACTAN ISLAND HOPPING";
  var pkprice_per_head = [6100,4150,3500,3300,3200,3100,3000,2900,2800,2700,2600,2500,2400];
  var pk_person_variance = [];
  pk_person_variance[0] = [1,1];
  pk_person_variance[1] = [2,2];
  pk_person_variance[2] = [3,3];
  pk_person_variance[3] = [4,4];
  pk_person_variance[4] = [5,5];
  pk_person_variance[5] = [6,6];
  pk_person_variance[6] = [7,7];
  pk_person_variance[7] = [8,8];
  pk_person_variance[8] = [9,9];
  pk_person_variance[9] = [10,10];
  pk_person_variance[10] = [11,11];
  pk_person_variance[11] = [12,12];
  pk_person_variance[12] = [13,13];

  var max_persons = 0;
  var number_of_persons = 0;
  var number_of_children = 0;
  var add_on_per_head_total = 0;
  var add_on_for_total =0;
  var discount_on_per_head_total =0;
  var discount_on_total =0;
  var d_price_head =0;

  //initialize discount for children if any
  var discount = 20;
  var percent = true;

  //initialize PACKAGE PROMO if any
  var promo_discount = <?php echo $discount ?>;
  var promo_percent = <?php echo $discount_percent ?>;

  $(document).ready( function(){
    
    setInterval('swapImages("tour_Gallery")', 5000);
    //initialize tour name
    $('#tour_name').val(package_tour_name);

    //initialize children discount
    $('#children_discount').val(discount);

    //initialize
    for(i=1;i<=99;i++) {
      $('#no_of_persons').append('<option val="'+ i + '">' + i + '</option>');
    }
    for(i=0;i<=0;i++) {
      $('#no_of_children').append('<option val="'+ i + '">' + i + '</option>');
    }
    if(discount == 0) {
      $('.children_row').hide();
    }

    //initialize other details on load
    set_other_payment_details();

    number_of_persons = $('select[name=no_of_persons]').val();
    number_of_children = $('select[name=no_of_children]').val();

    // alert("the array length : " + pk_person_variance.length);
    max_persons = pk_person_variance[pk_person_variance.length -1][1];

    // alert("the maximum person : " + max_persons);

    d_price_head = get_price_index(pkprice_per_head,pk_person_variance, number_of_persons, max_persons);
    //alert( "The price per head : " + d_price_head );

    add_on_per_head_total = compute_addon_discount_total('add_on_head');
    add_on_total = compute_addon_discount_total('add_on_total');
    discount_on_per_head_total = compute_addon_discount_total('discount_head');
    discount_total = compute_addon_discount_total('discount_total');
    compute_total(d_price_head,add_on_per_head_total,discount_on_per_head_total,number_of_persons, number_of_children,percent,discount,add_on_total,discount_total);

  monitor_control_changes();
  submit_package_forms();

});

</script>