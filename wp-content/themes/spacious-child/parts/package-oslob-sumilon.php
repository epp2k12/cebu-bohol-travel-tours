<?php
$discount = get_post_meta( get_the_ID(), 'discount', true );
$discount_percent = get_post_meta( get_the_ID(), 'is_discount_percent', true );
?>

<div class="pack_container">
<div class="col-sm-7 parts_main">

    <?php
    if($discount>0) {
    echo "<div id='promo_box1'>PROMO FOR THIS MONTH";
    echo "<h4 style='margin:0px;padding:0px'>PER HEAD IS LESS ";
    echo $discount;
	  echo ($discount_percent=='true')?'%':'';
    echo "!</h4>";
    echo "</div>";
    }
    ?>

<div class="mid_blue_title_bars" >OSLOB WHALE, TUMALOG FALLS &amp; SUMILON ISLAND</div>
  <div id="tour_Gallery" class="clearfix">
    <img src="<?php bloginfo('stylesheet_directory')?>/img/oslob_sumilon/oslob_sumilon01.jpg" class="active" alt="Oslob Whale Tumalog Sumilon Island"/>
    <img src="<?php bloginfo('stylesheet_directory')?>/img/oslob_sumilon/oslob_sumilon02.jpg" alt="Oslob Whale Tumalog Sumilon Island"/>
    <img src="<?php bloginfo('stylesheet_directory')?>/img/oslob_sumilon/oslob_sumilon03.jpg" alt="Oslob Whale Tumalog Sumilon Island"/>
    <img src="<?php bloginfo('stylesheet_directory')?>/img/oslob_sumilon/oslob_sumilon04.jpg" alt="Oslob Whale Tumalog Sumilon Island"/>
    <img src="<?php bloginfo('stylesheet_directory')?>/img/oslob_sumilon/oslob_sumilon05.jpg" alt="Oslob Whale Tumalog Sumilon Island"/>
    <img src="<?php bloginfo('stylesheet_directory')?>/img/oslob_sumilon/oslob_sumilon06.jpg" alt="Oslob Whale Tumalog Sumilon Island"/>
    <img src="<?php bloginfo('stylesheet_directory')?>/img/oslob_sumilon/oslob_sumilon07.jpg" alt="Oslob Whale Tumalog Sumilon Island"/>
  </div>
<br/>

<div class="panel-group clearfix" id="accordion" >
        <div class="panel panel-default" >
              <div class="panel-heading">
                  <h4 class="panel-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">RATE CHART</a>
                  </h4>
              </div>
              <div id="collapseOne" class="panel-collapse collapse in">
                <div class="panel-body tour_details">
                  <table class="table table-condensed">
                  <tr><td style="width:25%">No. of Person(s)</td><td style="width:75%">Price per Head</td></tr>
                  <tr><td>1</td><td>7,900/head</td></tr>
                  <tr><td>2</td><td>4,800/head </td></tr>
                  <tr><td>3</td><td>3,800/head</td></tr>
                  <tr><td>4</td><td>3,400/head</td></tr>
                  <tr><td>5</td><td>3,200/head </td></tr>
                  <tr><td>6</td><td>3,000/head</td></tr>
                  <tr><td>7</td><td>2,800/head</td></tr>
                  <tr><td>8</td><td>2,600/head </td></tr>
                  <tr><td>9</td><td>2,500/head</td></tr>
                  <tr><td>10</td><td>2,400/head</td></tr>
                  <tr><td>11</td><td>2,300/head</td></tr>
                  <tr><td>12</td><td>2,200/head</td></tr>
                  <tr><td>13</td><td>2,100/head</td></tr>
                  <tr><td>14 and Above</td><td>Contact us for the price quotation</td></tr>
                  <tr><td colspan="2">
                    <strong>For Snorkeling with whale sharks:</strong>
                    <ul>
                    <li>Add on of Php 500/head for Foreign Guest</li>
                    </ul>
                    <strong>Note:</strong><br>
                     The add-on of Php500 for foreign guests is a part of Municipal and Tourist ordinance in Oslob, Cebu.<br><br>
                    <strong>For  Sumilon day Tour</strong>
                    <ul>
                    <li>Add on of Php 500/head if day tour falls on weekends or holidays</li>
                    </ul>
                  </td></tr>
                  </table>
                </div>
              </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">PACKAGE INCLUSIONS</a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse">
                <div class="panel-body tour_details">
                  <table class="table table-condensed">
                  <tr><td colspan="2">
                  Whale watching and Tumalog Falls entrance <br>
                  Boat ride and life vest during whale watching <br>
                  Whale shark watching<br>
                  Sumilon Island Tour <br>
                  Tumalog Falls<br>
                  Boat transfer from Oslob to Sumilon island and back <br>
                  Fully air-conditioned Car or Van Service <br>
                  </td></tr>
                  </table>
                </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">PACKAGE EXCLUSIONS</a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse">
                <div class="panel-body tour_details">
                  <table class="table table-condensed">
                  <tr><td colspan="2">
                    MEALS/SNACKS<br>
                    Bluewater Resort<br>
                    Accommodations<br/>
                  Air Fare<br/>
                  </td></tr>
                  </table>
                </div>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">ITINERARY</a>
                </h4>
            </div>
            <div id="collapseFour" class="panel-collapse collapse">
                <div class="panel-body tour_details">
                  <table class="table table-condensed">
                  <tr><td colspan="2">Recommended pick up time from Cebu City or Lapu-lapu City is between 3:00AM to 4:00AM</td></tr>
                  <tr><td style="width:35%">4:00 AM </td><td style="width:65%"> Pick up and departure time from Hotel </td>
                  <tr><td>6:45 AM </td><td> Arrival at Oslob then Breakfast </td>
                  <tr><td>7:15 AM </td><td> Whale Shark Watching </td>
                  <tr><td>8:15 AM </td><td> Boat ride to Sumilon Island </td>
                  <tr><td>8:45 AM </td><td> Sumilon Sandbar </td>
                  <tr><td>11:30AM </td><td> Depart from Sumilon Island </td>
                  <tr><td>12:00NN </td><td> Lunch time </td>
                  <tr><td>1:00 PM </td><td> Tumalog Falls</td></tr>
                  <tr><td>3:00 PM </td><td> Depart from Oslob</td></tr>
                  <tr><td>7:00 PM </td><td> Estimated Arrival at Metro Cebu Tour duration is maximum for 15 hrs. </td></tr>
                  <tr><td colspan="2">In case you want to have side trip (upon arrival in Cebu City), we charge P 300.00/hr. </td></tr>
                  </table>
                </div>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive">DAY TOUR SERVICE</a>
                </h4>
            </div>
            <div id="collapseFive" class="panel-collapse collapse">
                <div class="panel-body tour_details">
                  <table class="table table-condensed">
                  <tr><td colspan="2">1 Day private tour (15 hours duration)</td></tr>
                  <tr><td colspan="2">Tour Facilitator</td></tr>
                  <tr><td colspan="2">Fully air-conditioned vehicle service</td></tr>
                   <tr><td colspan="2">
                   <ul>
                   <li>1 to 3 persons &#45; sedan type vehicle</li>
                   <li>4 to 6 persons &#45; minivan type vehicle</li>
                   <li>7 to 13 persons &#45; van type vehicle</li>
                   <li>14 and above &#45; 2 vehicles or it depends on the number of heads</li>
                   </ul>
                   </td></tr>
                  </table>
                </div>
            </div>
          </div>

</div> <!-- accordion -->


</div> <!-- col-sm-7 -->
<div class="col-sm-5 parts_side" >
<!-- ####################################### starts here #################################### -->
<?php get_template_part('common/form','common'); ?>
<!-- ####################################### ends here #################################### -->
</div> <!-- col-sm-5 -->
</div> <!-- pack container -->


<script type="text/javascript">

  var package_tour_name = "OSLOB WHALE / TUMALOG FALLS / SUMILON ISLAND";
  var pkprice_per_head = [7900,4800,3800,3400,3200,3000,2800,2600,2500,2400,2300,2200,2100];
  var pk_person_variance = [];
  pk_person_variance[0] = [1,1];
  pk_person_variance[1] = [2,2];
  pk_person_variance[2] = [3,3];
  pk_person_variance[3] = [4,4];
  pk_person_variance[4] = [5,5];
  pk_person_variance[5] = [6,6];
  pk_person_variance[6] = [7,7];
  pk_person_variance[7] = [8,8];
  pk_person_variance[8] = [9,9];
  pk_person_variance[9] = [10,10];
  pk_person_variance[10] = [11,11];
  pk_person_variance[11] = [12,12];
  pk_person_variance[12] = [13,13];

  var max_persons = 0;
  var number_of_persons = 0;
  var number_of_children = 0;
  var add_on_per_head_total = 0;
  var add_on_for_total =0;
  var discount_on_per_head_total =0;
  var discount_on_total =0;
  var d_price_head =0;

  //initialize discount for children if any
  var discount = 0;
  var percent = false;

  //initialize PACKAGE PROMO if any
  var promo_discount = <?php echo $discount ?>;
  var promo_percent = <?php echo $discount_percent ?>;

  $(document).ready( function(){

    setInterval('swapImages("tour_Gallery")', 5000);

    //initialize tour name
    $('#tour_name').val(package_tour_name);

    //initialize children discount
    $('#children_discount').val(discount);

    //initialize
    for(i=1;i<=99;i++) {
      $('#no_of_persons').append('<option val="'+ i + '">' + i + '</option>');
    }
    for(i=0;i<=0;i++) {
      $('#no_of_children').append('<option val="'+ i + '">' + i + '</option>');
    }
    if(discount == 0) {
      $('.children_row').hide();
    }

    //initialize other details on load
    set_other_payment_details();

    number_of_persons = $('select[name=no_of_persons]').val();
    number_of_children = $('select[name=no_of_children]').val();

    // alert("the array length : " + pk_person_variance.length);
    max_persons = pk_person_variance[pk_person_variance.length -1][1];

    // alert("the maximum person : " + max_persons);

    d_price_head = get_price_index(pkprice_per_head,pk_person_variance, number_of_persons, max_persons);
    //alert( "The price per head : " + d_price_head );

    add_on_per_head_total = compute_addon_discount_total('add_on_head');
    add_on_total = compute_addon_discount_total('add_on_total');
    discount_on_per_head_total = compute_addon_discount_total('discount_head');
    discount_total = compute_addon_discount_total('discount_total');
    compute_total(d_price_head,add_on_per_head_total,discount_on_per_head_total,number_of_persons, number_of_children,percent,discount,add_on_total,discount_total);

  monitor_control_changes();
  submit_package_forms();

});

</script>
