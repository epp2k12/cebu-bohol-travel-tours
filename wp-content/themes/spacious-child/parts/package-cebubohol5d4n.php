<?php
$discount = get_post_meta( get_the_ID(), 'discount', true );
$discount_percent = get_post_meta( get_the_ID(), 'is_discount_percent', true );
?>

<div class="pack_container">
<div class="col-sm-7 parts_main">

    <?php
    if($discount>0) {
    echo "<div id='promo_box1'>PROMO FOR THIS MONTH";
    echo "<h4 style='margin:0px;padding:0px'>PER HEAD IS LESS ";
    echo $discount;
	  echo ($discount_percent=='true')?'%':'';
    echo "!</h4>";
    echo "</div>";
    }
    ?>

<div class="mid_blue_title_bars" >5 DAYS &amp; 4 NIGHTS CEBU &amp; BOHOL TOUR</div>
  <div id="tour_Gallery" class="clearfix">
    <img src="<?php bloginfo('stylesheet_directory')?>/img/cebu4d3n/cebu4d3n07.png" class="active" alt="Cebu Tours and Bohol Tours"/>
    <img src="<?php bloginfo('stylesheet_directory')?>/img/cebu4d3n/cebu4d3n02.png" alt="Cebu Tours and Bohol Tours"/>
    <img src="<?php bloginfo('stylesheet_directory')?>/img/cebu4d3n/cebu4d3n03.png" alt="Cebu Tours and Bohol Tours"/>
    <img src="<?php bloginfo('stylesheet_directory')?>/img/cebu4d3n/cebu4d3n04.png" alt="Cebu Tours and Bohol Tours"/>
    <img src="<?php bloginfo('stylesheet_directory')?>/img/cebu4d3n/cebu4d3n05.png" alt="Cebu Tours and Bohol Tours"/>
    <img src="<?php bloginfo('stylesheet_directory')?>/img/cebu4d3n/cebu4d3n06.png" alt="Cebu Tours and Bohol Tours"/>
    <img src="<?php bloginfo('stylesheet_directory')?>/img/cebu4d3n/cebu4d3n01.png" alt="Cebu Tours and Bohol Tours"/>
  </div>

<br/>

<!--
  <a href="#demo" class="btn btn-info" data-toggle="collapse">Simple collapsible</a>
  <div id="demo" class="collapse">
    Lorem ipsum dolor sit amet, consectetur adipisicing elit,
    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
  </div>
-->

<div class="panel-group clearfix" id="accordion" >
        <div class="panel panel-default" >
              <div class="panel-heading">
                  <h4 class="panel-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">RATE CHART</a>
                  </h4>
              </div>
              <div id="collapseOne" class="panel-collapse collapse in">
                <div class="panel-body tour_details">
                  <table class="table table-condensed">
                  <tr><td style="width:25%">No. of Person(s)</td><td style="width:75%">Price per Head</td></tr>
                  <tr><td>1</td><td>29,850 / head</td></tr>
                  <tr><td>2</td><td>18,500 / head</td></tr>
                  <tr><td>3</td><td>14,717 / head</td></tr>
                  <tr><td>4</td><td>12,175 / head</td></tr>
                  <tr><td>5</td><td>11,110 / head</td></tr>
                  <tr><td>6</td><td>10,583 / head</td></tr>
                  <tr><td>7</td><td>10,379 / head</td></tr>
                  <tr><td>8</td><td>9,638 / head</td></tr>
                  <tr><td>9</td><td>9,094 / head</td></tr>
                  <tr><td>10</td><td>8,700 / head</td></tr>
                  <tr><td>11</td><td>8,332 / head</td></tr>
                  <tr><td>12</td><td>8,025 / head</td></tr>
                  <tr><td>13</td><td>7,758 / head</td></tr>
                  <tr><td>14</td><td>7,550 / head</td></tr>
                  <tr><td>15 and Above</td><td>Contact us for the price quotation</td></tr>
                  <tr><td colspan="2">
                    <strong>For Snorkeling with whale sharks:</strong>
                    <ul>
                    <li>•  For foreign guest, it has an add-on fee of P500/head in whale shark and not included in this package.</li>
                    <li>•  Children below 12yo are not allowed at Canyoneering. Children with parents or guardians will go directly to Kawasan Falls</li>
                    <li>•  Side trip is subject to surcharge</li>
                    </ul>
                </td></tr>
                </table>
                </div>
              </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">PACKAGE INCLUSIONS</a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse">
                <div class="panel-body tour_details">
                <table class="table table-condensed">
                <tr><td colspan="2">
                  <ul>
                  <li>Accommodation<ul>
                  <li>2Nights stay at Lagnason's Place Resort in Oslob</li>
                  <li>2Nights stay at Robe’s Pension in Cebu City</li>
                  </ul>
                  </li>
                  <li>Osmena Peak</li>
                  <li>Whale Watching/swimming/snorkeling
                  <ul>
                  <li>Entrance fee</li>
                  <li>boat service</li>
                  <li>life vest</li>
                  <li>snorkeling gear</li>
                  </ul>
                  </li>
                  <li>Tumalog Falls
                  <ul>
                  <li>Entrance fee</li>
                  <li>Motorbike service</li>
                  </ul>
                  </li>
                  <li>Simala Shrine</li>
                  <li>Pescador island hopping
                  <ul>
                  <li>entrance fee</li>
                  <li>lifevest, snorkeling gear</li>
                  <li>pumpboat service</li>
                  <li>boatman as guide</li>
                  </ul>
                  </li>
                  <li>Bohol Day Tour
                  <ul>
                  <li>Pickup service from Hotel to Wharf</li>
                  <li>Round Trip Fast Craft Ticket (Oceanjet)</li>
                  <li>Buffet Lunch at Floating Restaurant</li>
                  <li>Venue Entrance Fees</li>
                  <li>Fully Air-conditioned Van for Bohol Transport</li>
                  <li>Pick-up Service from Cebu Wharf to Cebu Hotel</li>
                  </ul>
                  </li>
                  <li>Kawasan Canyoneering
                  <ul>
                  <li>entrance fee</li>
                  <li>motorbike service to jump off point</li>
                  <li>local canyoneering tour guide</li>
                  <li>life vest</li>
                  <li>rubber shoes</li>
                  <li>safety helmet</li>
                  </ul>
                  </li>
                  <li>City Tour
                  <ul>
                  <li>Venue Entrance Fees</li>
                  </ul>
                  </li>
                  <li>Private Fully air-condition vehicle service for the whole tour
                  <ul>
                  <li>1-6 pax Toyota innova</li>
                  <li>7-14pax Toyota Hiace</li>
                  </ul>
                  </li>
                  </ul>
                </td></tr>
                </table>
                </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">PACKAGE EXCLUSIONS</a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse">
                <div class="panel-body tour_details">
                <table class="table table-condensed">
                <tr><td colspan="2">
                <ul>
                <li>Meals</li>
                <li>Airfare</li>
                <li>underwater cam</li>
                <li>ATV and Zip ride at Bohol</li>
                <li>Others not stated in the inclusions above</li>
                </ul>
                </td></tr>
                </table>
                </div>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">ITINERARY</a>
                </h4>
            </div>
            <div id="collapseFour" class="panel-collapse collapse">
                <div class="panel-body tour_details">
                  <table class="table table-condensed">
                  <tr><td colspan="2"><strong>Day 1</strong></td></tr>
                  <tr><td>05:00am</td><td>Pickup at mactan airport</td></tr>
                  <tr><td>06:00am</td><td>Breakfast at City</td></tr>
                  <tr><td>07:30am</td><td>Depart to Simala</td></tr>
                  <tr><td>09:00am</td><td>Simala Shrine</td></tr>
                  <tr><td>11:30am</td><td>Lunch</td></tr>
                  <tr><td>01:00pm</td><td>Depart to Osmena peak</td></tr>
                  <tr><td>02:30pm</td><td>Osmena Peak</td></tr>
                  <tr><td>04:30pm</td><td>Depart to Oslob</td></tr>
                  <tr><td>05:30pm</td><td>Check-in at Lagnason's Place Resort in Oslob</td></tr>

                  <tr><td colspan="2"><strong>Day 2</strong></td></tr>
                  <tr><td>05:30am</td><td>Breakfast at resort</td></tr>
                  <tr><td>05:50am</td><td>Depart to Whale watching area</td></tr>
                  <tr><td>06:10am</td><td>Whale shark watching/swimming</td></tr>
                  <tr><td>08:00am</td><td>Tumalog Falls</td></tr>
                  <tr><td>09:00am</td><td>Depart to Moalboal</td></tr>
                  <tr><td>11:00am</td><td>Arrival at Moalboal then lunch</td></tr>
                  <tr><td>11:30am</td><td>Start Pescador island hopping</td></tr>
                  <tr><td colspan="2">
                  <ul>
                  <li>• turtle watching</li>
                  <li>• sardine run</li>
                  <li>• fish feeding</li>
                  <li>• swimming/snorkeling</li>
                  </ul>
                  <strong>Note:</strong> turtle watching and sardine run are not guaranteed it will depend on the weather and timing
                  </td></tr>
                  <tr><td>04:00pm</td><td>   Back to Oslob</td></tr>
                  <tr><td>05:30pm</td><td>  Arrival at resort</td></tr>
                   <tr><td colspan="2"><strong>Day 3</strong></td></tr>
                  <tr><td>05:30am</td><td>   Breakfast at resort</td></tr>
                  <tr><td>06:00am</td><td>   Check-out and Depart to Badian</td></tr>
                  <tr><td>08:00am</td><td>   Start Canyoneering</td></tr>
                  <tr><td>12:00nn</td><td>   Lunch at Kawasan</td></tr>
                  <tr><td>01:00pm</td><td>   Cool off at Kawasan Falls</td></tr>
                  <tr><td>04:00pm</td><td>   Depart to City</td></tr>
                  <tr><td>07:00pm</td><td>   Arrival and Check-in at Robe's Pension</td></tr>
                  <tr><td colspan="2"><strong>Day 4</strong></td></tr>
                  <tr><td>06:00am</td><td>   Pickup at Hotel</td></tr>
                  <tr><td>06:30am</td><td>   Breakfast at fastfood</td></tr>
                  <tr><td>08:00am</td><td>   Depart to Bohol via Oceanjet</td></tr>
                  <tr><td>10:00am</td><td>   Arrival at Bohol then start tour</td></tr>
                  <tr><td colspan="2">
                  <ul>
                  <li>• Blood Compact Site</li>
                  <li>• Baclayon Church</li>
                  <li>• St. Peter's Ruins Church</li>
                  <li>• Bohol Biggiest Snake in Captivity</li>
                  <li>• River Cruise Floating Resturant w/ Buffet Lunch</li>
                  <li>• Bilar Man-made Forest</li>
                  <li>• Tarsier Visit</li>
                  <li>• Chocolate Hills</li>
                  <li>• Butterfly Garden</li>
                  </ul>
                  </td></tr>
                  <tr><td>04:00pm</td><td>   End of Tour Proceed to Sea Port</td></tr>
                  <tr><td>05:30pm</td><td>   Depart to Cebu City via Oceanjet</td></tr>
                  <tr><td>07:30pm</td><td>   Pickup from Cebu Sea port/wharf then drop off to hotel</td></tr>
                  <tr><td colspan="2"><strong>Day 5</strong></td></tr>
                  <tr><td>09:00am</td><td>   Check out and start city tour</td></tr>
                  <tr><td>09:30am</td><td>   Fort San Pedro, Magellan’s Cross, Santo Nino Church, Cebu Cathedral, Cebu Heritage monument, Yap Sandiego House</td></tr>
                  <tr><td>12:00nn</td><td>   Lunch</td></tr>
                  <tr><td>01:00pm</td><td>   Pasalubong (souvenir) shop, Taoist temple, Mactan Shrine</td></tr>
                  <tr><td>05:00pm</td><td>   Arrival at Mactan Airport</td></tr>
                  <tr><td colspan="2"><strong>End of tour and service</strong></td></tr>
                  </table>
                </div>
            </div>
          </div>
</div> <!-- accordion -->


</div> <!-- col-sm-7 -->
<div class="col-sm-5 parts_side" >
<!-- ####################################### starts here #################################### -->
<?php get_template_part('common/form','common'); ?>
<!-- ####################################### ends here #################################### -->
</div> <!-- col-sm-5 -->
</div> <!-- pack container -->


<script type="text/javascript">
  setInterval('swapImages("tour_Gallery")', 5000);

  var package_tour_name = "5 DAYS AND 4 NIGHTS CEBU and BOHOL TOUR PACKAGE";
  var pkprice_per_head = [29850,18500,14717,12175,11110,10583,10379,9638,9094,8700,8332,8025,7758,7550];
  var pk_person_variance = [];
  pk_person_variance[0] = [1,1];
  pk_person_variance[1] = [2,2];
  pk_person_variance[2] = [3,3];
  pk_person_variance[3] = [4,4];
  pk_person_variance[4] = [5,5];
  pk_person_variance[5] = [6,6];
  pk_person_variance[6] = [7,7];
  pk_person_variance[7] = [8,8];
  pk_person_variance[8] = [9,9];
  pk_person_variance[9] = [10,10];
  pk_person_variance[10] = [11,11];
  pk_person_variance[11] = [12,12];
  pk_person_variance[12] = [13,13];
  pk_person_variance[13] = [14,14];

  var max_persons = 0;
  var number_of_persons = 0;
  var number_of_children = 0;
  var add_on_per_head_total = 0;
  var add_on_for_total =0;
  var discount_on_per_head_total =0;
  var discount_on_total =0;
  var d_price_head =0;

  //initialize discount for children if any
  var discount = 0;
  var percent = false;

  //initialize PACKAGE PROMO if any
  var promo_discount = <?php echo $discount ?>;
  var promo_percent = <?php echo $discount_percent ?>;

  $(document).ready( function(){

    //initialize tour name
    $('#tour_name').val(package_tour_name);

    //initialize children discount
    $('#children_discount').val(discount);

    //initialize
    for(i=1;i<=99;i++) {
      $('#no_of_persons').append('<option val="'+ i + '">' + i + '</option>');
    }
    for(i=0;i<=0;i++) {
      $('#no_of_children').append('<option val="'+ i + '">' + i + '</option>');
    }
    if(discount == 0) {
      $('.children_row').hide();
    }

    //initialize other details on load
    set_other_payment_details();

    number_of_persons = $('select[name=no_of_persons]').val();
    number_of_children = $('select[name=no_of_children]').val();

    // alert("the array length : " + pk_person_variance.length);
    max_persons = pk_person_variance[pk_person_variance.length -1][1];

    // alert("the maximum person : " + max_persons);

    d_price_head = get_price_index(pkprice_per_head,pk_person_variance, number_of_persons, max_persons);
    //alert( "The price per head : " + d_price_head );

    add_on_per_head_total = compute_addon_discount_total('add_on_head');
    add_on_total = compute_addon_discount_total('add_on_total');
    discount_on_per_head_total = compute_addon_discount_total('discount_head');
    discount_total = compute_addon_discount_total('discount_total');
    compute_total(d_price_head,add_on_per_head_total,discount_on_per_head_total,number_of_persons, number_of_children,percent,discount,add_on_total,discount_total);

  monitor_control_changes();
  submit_package_forms();

});

</script>