<?php
$discount = get_post_meta( get_the_ID(), 'discount', true );
$discount_percent = get_post_meta( get_the_ID(), 'is_discount_percent', true );
?>

<div class="pack_container">
<div class="col-sm-7 parts_main">

    <?php
    if($discount>0) {
    echo "<div id='promo_box1'>PROMO FOR THIS MONTH";
    echo "<h4 style='margin:0px;padding:0px'>PER HEAD IS LESS ";
    echo $discount;
	  echo ($discount_percent=='true')?'%':'';
    echo "!</h4>";
    echo "</div>";
    }
    ?>

<div class="mid_blue_title_bars" >OSLOB WHALE, TUMALOG FALLS &amp; SIMALA</div>

  <div id="tour_Gallery" class="clearfix">
    <img src="<?php bloginfo('stylesheet_directory')?>/img/oslob_tumalog/oslob_tumalog01.jpg" class="active" alt="Oslob Whale Tumalog Simala"/>
    <img src="<?php bloginfo('stylesheet_directory')?>/img/oslob_tumalog/oslob_tumalog02.jpg" alt="Oslob Whale Tumalog Simala"/>
    <img src="<?php bloginfo('stylesheet_directory')?>/img/oslob_tumalog/oslob_tumalog03.jpg" alt="Oslob Whale Tumalog Simala"/>
    <img src="<?php bloginfo('stylesheet_directory')?>/img/oslob_tumalog/oslob_tumalog04.jpg" alt="Oslob Whale Tumalog Simala"/>
    <img src="<?php bloginfo('stylesheet_directory')?>/img/oslob_tumalog/oslob_tumalog05.jpg" alt="Oslob Whale Tumalog Simala"/>
    <img src="<?php bloginfo('stylesheet_directory')?>/img/oslob_tumalog/oslob_tumalog06.jpg" alt="Oslob Whale Tumalog Simala"/>
    <img src="<?php bloginfo('stylesheet_directory')?>/img/oslob_tumalog/oslob_tumalog07.jpg" alt="Oslob Whale Tumalog Simala"/>
  </div>
<br/>

<div class="panel-group clearfix" id="accordion" >
        <div class="panel panel-default" >
              <div class="panel-heading">
                  <h4 class="panel-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">RATE CHART</a>
                  </h4>
              </div>
              <div id="collapseOne" class="panel-collapse collapse in">
                <div class="panel-body tour_details">
                  <table class="table table-condensed">
                  <tr><td style="width:25%">No. of Person(s)</td><td style="width:75%">Price per Head</td></tr>
                  <tr><td>1</td><td>5,450head</td></tr>
                  <tr><td>2</td><td>3,350/head </td></tr>
                  <tr><td>3</td><td>2,800/head</td></tr>
                  <tr><td>4</td><td>2,400/head</td></tr>
                  <tr><td>5</td><td>2,200/head </td></tr>
                  <tr><td>6</td><td>1,950/head</td></tr>
                  <tr><td>7</td><td>1,850/head</td></tr>
                  <tr><td>8</td><td>1,800/head </td></tr>
                  <tr><td>9</td><td>1,750/head</td></tr>
                  <tr><td>10</td><td>1,700/head</td></tr>
                  <tr><td>11</td><td>1,650/head</td></tr>
                  <tr><td>12</td><td>1,600/head</td></tr>
                  <tr><td>13</td><td>1,550/head</td></tr>
                  <tr><td>14 and Above</td><td>Contact us for the price quotation</td></tr>
                  <tr><td colspan="2">
                    <strong>For Snorkeling with whale sharks:</strong>
                    <ul>
                    <li>Add on of Php 500/head for Foreign Guest</li>
                    </ul>
                    <strong>Note:</strong><br>
                     The add-on of Php500 for foreign guests is a part of Municipal and Tourist ordinance in Oslob, Cebu.<br><br>
                  </td></tr>
                  </table>
                </div>
              </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">PACKAGE INCLUSIONS</a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse">
                <div class="panel-body tour_details">
                  <table class="table table-condensed">
                  <tr><td colspan="2">
                  Whale watching and Tumalog falls entrance<br>
                  Boat ride and life vest<br>
                  Whale shark watching<br>
                  Tumalog Falls<br>
                  Fully air-conditioned Car or Van Service<br>
                  </td></tr>
                  </table>
                </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">PACKAGE EXCLUSIONS</a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse">
                <div class="panel-body tour_details">
                  <table class="table table-condensed">
                  <tr><td colspan="2">
                  MEALS/Snacks<br>
                  Underwater Camera<br>
                  Air Fare<br/>
                  Accomodations<br/>
                  </td></tr>
                  </table>
                </div>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">ITINERARY</a>
                </h4>
            </div>
            <div id="collapseFour" class="panel-collapse collapse">
                <div class="panel-body tour_details">
                  <table class="table table-condensed">
                  <tr><td colspan="2">Recommended pick up time from Cebu City or Lapu-lapu City is between 4:00AM to 5:30AM</td></tr>
                  <tr><td style="width:35%">05:00 AM </td><td style="width:65%"> Pick up and departure time from Hotel </td></tr>
                  <tr><td>08:00 AM </td><td> Arrival at Oslob then Breakfast </td></tr>
                  <tr><td>08:30 AM </td><td> Whale Shark Watching </td></tr>
                  <tr><td>09:30 AM </td><td> Tumalog Falls </td></tr>
                  <tr><td>11:30 AM </td><td> Lunch </td></tr>
                  <tr><td>12:30 PM </td><td> Depart from Oslob for Simala</td></tr>
                  <tr><td>02:30 PM </td><td> Arrival Simala Shrine</td></tr>
                  <tr><td>03:30 PM </td><td> Depart to Cebu City</td></tr>
                  <tr><td>06:00 PM </td><td> Arrival Cebu City </td></tr>
                  <tr><td colspan="2">In case you want to have side trip (upon arrival in Cebu City), we charge P300.00/hr. </td></tr>
                  </table>
                </div>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive">DAY TOUR SERVICE</a>
                </h4>
            </div>
            <div id="collapseFive" class="panel-collapse collapse">
                <div class="panel-body tour_details">
                  <table class="table table-condensed">
                  <tr><td colspan="2">1 Day private tour (15 hours duration)</td></tr>
                  <tr><td colspan="2">Tour Facilitator</td></tr>
                  <tr><td colspan="2">Fully air-conditioned vehicle service</td></tr>
                   <tr><td colspan="2">
                   <ul>
                   <li>1 to 3 persons &#45; sedan type vehicle</li>
                   <li>4 to 6 persons &#45; minivan type vehicle</li>
                   <li>7 to 13 persons &#45; van type vehicle</li>
                   <li>14 and above &#45; 2 vehicles or it depends on the number of heads</li>
                   </ul>
                   </td></tr>
                  </table>
                </div>
            </div>
          </div>

</div> <!-- accordion -->

<div class="package-caption">
<h1>Oslob whale shark watching, Tumalog Falls and Simala</h1>
<p>
An exciting day tour package for your group (friends and family). We kick off as we pick your group early 5:00AM with a private fully air-conditioned vehicle (van or car depends on number of persons). Pick-up is free within Cebu City area or Mactan Area (Airport or Hotel). From Cebu we travel about 3 hours to reach Oslob, Tan-awan town. All entrance fees, motorcycle ride and boat rides are included in the package. For the oslob whale shark watching you will be provided with life vest. You will then swim with the whale sharks ( there are rules not to touch the whales ) for about 30 minutes. You can take pictures with the whale sharks but you must provide your own underwater camera. If you don't have one you can easily rent a camera on sight for about P400-500. No worries, if you don't know how to swim the life vest will keep you afloat and the boat men in charge will help you and keep watch of you and your group. You will surely cherish this great experience of Oslob whale shark watching and swimming with the giants of the seas.
</p>
<p>
Then we head off to Tumalog falls around 10:00 and there you will see one of the most beautiful water falls in Oslob. It is like a great wall with water falling like rain drops. You will enjoy taking pictures or even taking a quick dip in its cool waters.
</p>
<p>
Last off will be a tour to the most holy shrine, the Simala Shrine of Sibonga. It is a very huge castle like shrine for the catholic devotees. Many believers come here in Simala shrine to pray and make their petitions.
</p>
<p>
This day tour package is very wholesome and very affordable. So hurry book this tour now. Oslob <a href="https://whalesharkoslob.com">whale shark watching</a>, Tumalog falls and Simala Shrine tour package.
</p>
</div> <!-- package caption -->

</div> <!-- col-sm-7 -->
<div class="col-sm-5 parts_side" >
<!-- ####################################### starts here #################################### -->
<?php get_template_part('common/form','common'); ?>
<!-- ####################################### ends here #################################### -->
</div> <!-- col-sm-5 -->
</div> <!-- pack container -->


<script type="text/javascript">

  var package_tour_name = "OSLOB WHALE / TUMALOG FALLS / SIMALA";
  var pkprice_per_head = [5450,3350,2800,2400,2200,1950,1850,1800,1750,1700,1650,1600,1550];
  var pk_person_variance = [];
  pk_person_variance[0] = [1,1];
  pk_person_variance[1] = [2,2];
  pk_person_variance[2] = [3,3];
  pk_person_variance[3] = [4,4];
  pk_person_variance[4] = [5,5];
  pk_person_variance[5] = [6,6];
  pk_person_variance[6] = [7,7];
  pk_person_variance[7] = [8,8];
  pk_person_variance[8] = [9,9];
  pk_person_variance[9] = [10,10];
  pk_person_variance[10] = [11,11];
  pk_person_variance[11] = [12,12];
  pk_person_variance[12] = [13,13];

  var max_persons = 0;
  var number_of_persons = 0;
  var number_of_children = 0;
  var add_on_per_head_total = 0;
  var add_on_for_total =0;
  var discount_on_per_head_total =0;
  var discount_on_total =0;
  var d_price_head =0;

  //initialize discount for children if any
  var discount = 0;
  var percent = false;

  //initialize PACKAGE PROMO if any
  var promo_discount = <?php echo $discount ?>;
  var promo_percent = <?php echo $discount_percent ?>;

  $(document).ready( function(){

    setInterval('swapImages("tour_Gallery")', 5000);
    
    //initialize tour name
    $('#tour_name').val(package_tour_name);

    //initialize children discount
    $('#children_discount').val(discount);

    //initialize
    for(i=1;i<=99;i++) {
      $('#no_of_persons').append('<option val="'+ i + '">' + i + '</option>');
    }
    for(i=0;i<=0;i++) {
      $('#no_of_children').append('<option val="'+ i + '">' + i + '</option>');
    }
    if(discount == 0) {
      $('.children_row').hide();
    }

    //initialize other details on load
    set_other_payment_details();

    number_of_persons = $('select[name=no_of_persons]').val();
    number_of_children = $('select[name=no_of_children]').val();

    // alert("the array length : " + pk_person_variance.length);
    max_persons = pk_person_variance[pk_person_variance.length -1][1];

    // alert("the maximum person : " + max_persons);

    d_price_head = get_price_index(pkprice_per_head,pk_person_variance, number_of_persons, max_persons);
    //alert( "The price per head : " + d_price_head );

    add_on_per_head_total = compute_addon_discount_total('add_on_head');
    add_on_total = compute_addon_discount_total('add_on_total');
    discount_on_per_head_total = compute_addon_discount_total('discount_head');
    discount_total = compute_addon_discount_total('discount_total');
    compute_total(d_price_head,add_on_per_head_total,discount_on_per_head_total,number_of_persons, number_of_children,percent,discount,add_on_total,discount_total);

  monitor_control_changes();
  submit_package_forms();

});

</script>