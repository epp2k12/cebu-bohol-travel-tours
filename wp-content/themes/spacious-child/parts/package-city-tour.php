<?php
$discount = get_post_meta( get_the_ID(), 'discount', true );
$discount_percent = get_post_meta( get_the_ID(), 'is_discount_percent', true );
?>

<div class="pack_container">
<div class="col-sm-7 parts_main">

    <?php
    if($discount>0) {
    echo "<div id='promo_box1'>PROMO FOR THIS MONTH";
    echo "<h4 style='margin:0px;padding:0px'>PER HEAD IS LESS ";
    echo $discount;
	  echo ($discount_percent=='true')?'%':'';
    echo "!</h4>";
    echo "</div>";
    }
    ?>

<div class="mid_blue_title_bars" >CEBU CITY DAY TOUR</div>
  <div id="tour_Gallery" class="clearfix">
    <img src="<?php bloginfo('stylesheet_directory')?>/img/city_tours/citytours01.png" class="active" />
    <img src="<?php bloginfo('stylesheet_directory')?>/img/city_tours/citytours02.png" />
    <img src="<?php bloginfo('stylesheet_directory')?>/img/city_tours/citytours03.png" />
    <img src="<?php bloginfo('stylesheet_directory')?>/img/city_tours/citytours04.png" />
    <img src="<?php bloginfo('stylesheet_directory')?>/img/city_tours/citytours05.png" />
    <img src="<?php bloginfo('stylesheet_directory')?>/img/city_tours/citytours06.png" />
    <img src="<?php bloginfo('stylesheet_directory')?>/img/city_tours/citytours07.png" />
    <img src="<?php bloginfo('stylesheet_directory')?>/img/city_tours/citytours08.png" />
    <img src="<?php bloginfo('stylesheet_directory')?>/img/city_tours/citytours09.png" />
    <img src="<?php bloginfo('stylesheet_directory')?>/img/city_tours/citytours10.png" />
    <img src="<?php bloginfo('stylesheet_directory')?>/img/city_tours/citytours11.png" />
  </div>
<br/>

<div class="panel-group clearfix" id="accordion" >
        <div class="panel panel-default" >
              <div class="panel-heading">
                  <h4 class="panel-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">RATE CHART</a>
                  </h4>
              </div>
              <div id="collapseOne" class="panel-collapse collapse in">
                <div class="panel-body tour_details">
                  <table class="table table-condensed">
                  <tr><td style="width:30%">No. of Person(s)</td><td style="width:70%">Price per Head ()</td></tr>
                  <tr><td>1</td><td>2,900/head</td></tr>
                  <tr><td>2</td><td>1,600/head </td></tr>
                  <tr><td>3</td><td>1,200/head</td></tr>
                  <tr><td>4</td><td>1,050/head</td></tr>
                  <tr><td>5</td><td>900/head </td></tr>
                  <tr><td>6</td><td>800/head</td></tr>
                  <tr><td>7</td><td>750/head</td></tr>
                  <tr><td>8</td><td>700/head </td></tr>
                  <tr><td>9</td><td>650/head</td></tr>
                  <tr><td>10</td><td>600/head</td></tr>
                  <tr><td>11</td><td>550/head</td></tr>
                  <tr><td>12</td><td>500/head</td></tr>
                  <tr><td>13</td><td>500/head</td></tr>
                  <tr><td>14 and Above</td><td>Contact us for the price quotation</td>
                  </tr>
                  <tr><td colspan="3">
                    Children / Kids Pricing
                    <ul>
                    <li>Child below 2 years old - FREE/Not included in headcount</li>
                    <li>Child 3 to 7 years old - Less Php300</li>
                    <li>Child above 8 years old - Full Charge</li>
                    </ul>
                  </td></tr>
                  </table>
                </div>
              </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">PACKAGE INCLUSIONS</a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse">
                <div class="panel-body tour_details">
                  <table class="table table-condensed">
                  <tr><td colspan="2">
                  Entrance fees<br>
                  Fully air-conditioned Car or Van Service<br>
                  </td></tr>
                  </table>
                </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">PACKAGE EXCLUSIONS</a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse">
                <div class="panel-body tour_details">
                  <table class="table table-condensed">
                  <tr><td colspan="2">
                    MEALS/SNACKS<br>
                    Accommodations<br/>
                  Air Fare<br/>
                  </td></tr>
                  </table>
                </div>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">ITINERARY</a>
                </h4>
            </div>
            <div id="collapseFour" class="panel-collapse collapse">
                <div class="panel-body tour_details">
                  <table class="table table-condensed">
                  <tr><td width="30%">9:00 AM </td><td width="70%"> Pick up and departure time from Hotel </td></tr>
                  <tr><td>9:45 AM &#45; 11:30 AM </td><td> Fort San Pedro, Magellan&#39;s Cross, Santo Nino, Cebu Heritage Monument, Yap-Sandiego Ancestral House  </td></tr>
                  <tr><td>1200 NN &#45; 1:30 PM </td><td> Lunch Break</td></tr>
                  <tr><td>1:30 PM &#45; 4:00 PM </td><td> Museo Sugbo, Cebu Taoist Temple, Mactan Shrine </td></tr>
                  <tr><td>5:00 PM </td><td>Back at hotel  </td></tr>
                  <tr><td colspan="2">Tour duration is maximum for 8 hrs. In case you want to have side trip, we will charge P 300.00/hr.</td></tr>
                  </table>
                  <table class="table table-condensed">
                  <tr><td colspan ="2"> WITH TEMPLE OF LEA &amp; TOPS </td></tr>
                  <tr><td width="30%">08:00 AM </td><td width="70%"> Pick up at hotel or airport</td></tr>
                  <tr><td>08:30 AM &#45; 11:30 AM </td><td> Mactan Shrine, Fort San Pedro, Magellan&#39;s Cross, Santo Nino, Cebu Heritage Monument, Yap-Sandiego Ancestral House  </td></tr>
                  <tr><td>12:00 NN </td><td> Lunch Break (Guest Own Expense)</td></tr>
                  <tr><td>01:00 PM &#45; 5:00 PM </td><td> Museo Sugbo, Cebu Taoist Temple, Pasalubong Shop, Temple of Leah, Cebu Tops </td></tr>
                  <tr><td>06:00 PM </td><td>Back at hotel  </td></tr>
                  <tr><td colspan="2">Tour duration is maximum for 10 hrs. In case you want to have side trip, we will charge P 300.00/hr.</td></tr>
                  </table>
                </div>
            </div>
          </div>
</div> <!-- accordion -->

<div class="package-caption">
<h1>CEBU TOUR</h1>
<p>
The Cebu tour will start as we will pick you up at the Hotel (within the City) or Mactan Airport around 9:00AM. Then the tour proceeds to Fort San Pedro, a very nice park. Then to the historical Magellan's cross and the holy Basillica del Santo Nino Church, one of the oldest church in the city. The we visit Heritage monument and some very old ancestral house of Yap-Sandiego. 
</p>
After the lunch break, we proceed to Museo Sugbo, then to the famous CebuTaoist Temple and off to Mactan Shrine. You can also add a few places if you want to extend your Cebu Tour for an add-on price of only P400. You will be taken to the little greece in the Philippines "Temple of Leah" and you will see the very scenic view of the entire Cebu City at the mountain TOPs of Cebu. 
</p>
<p>
A very comfortable private ride and the best tour drivers for an affordable rate you will get to know the best of Cebu City.
</p>
</div> <!-- package caption -->

</div> <!-- col-sm-7 -->
<div class="col-sm-5 parts_side" >
<!-- ####################################### starts here #################################### -->
<?php get_template_part('common/form','common-city'); ?>
<!-- ####################################### ends here #################################### -->
</div> <!-- col-sm-5 -->
</div> <!-- pack container -->


<script type="text/javascript">
  setInterval('swapImages("tour_Gallery")', 5000);

  var package_tour_name = "CEBU CITY DAY TOUR";
  var pkprice_per_head = [2900,1600,1200,1050,900,800,750,700,650,600,550,500,500];
  var pk_person_variance = [];
  pk_person_variance[0] = [1,1];
  pk_person_variance[1] = [2,2];
  pk_person_variance[2] = [3,3];
  pk_person_variance[3] = [4,4];
  pk_person_variance[4] = [5,5];
  pk_person_variance[5] = [6,6];
  pk_person_variance[6] = [7,7];
  pk_person_variance[7] = [8,8];
  pk_person_variance[8] = [9,9];
  pk_person_variance[9] = [10,10];
  pk_person_variance[10] = [11,11];
  pk_person_variance[11] = [12,12];
  pk_person_variance[12] = [13,13];

  var max_persons = 0;
  var number_of_persons = 0;
  var number_of_children = 0;
  var add_on_per_head_total = 0;
  var add_on_for_total =0;
  var discount_on_per_head_total =0;
  var discount_on_total =0;
  var d_price_head =0;

  //initialize discount for children if any
  var discount = 300;
  var percent = false;

  //initialize PACKAGE PROMO if any
  var promo_discount = <?php echo $discount ?>;
  var promo_percent = <?php echo $discount_percent ?>;

  $(document).ready( function(){

    //initialize tour name
    $('#tour_name').val(package_tour_name);

    //initialize children discount
    $('#children_discount').val(discount);

    //initialize
    for(i=1;i<=99;i++) {
      $('#no_of_persons').append('<option val="'+ i + '">' + i + '</option>');
    }
    for(i=0;i<=0;i++) {
      $('#no_of_children').append('<option val="'+ i + '">' + i + '</option>');
    }
    if(discount == 0) {
      $('.children_row').hide();
    }

    //initialize other details on load
    set_other_payment_details();

    number_of_persons = $('select[name=no_of_persons]').val();
    number_of_children = $('select[name=no_of_children]').val();

    max_persons = pk_person_variance[pk_person_variance.length -1][1];

    d_price_head = get_price_index(pkprice_per_head,pk_person_variance, number_of_persons, max_persons);
    //alert( "The price per head : " + d_price_head );

    add_on_per_head_total = compute_addon_discount_total('add_on_head');
    add_on_total = compute_addon_discount_total('add_on_total');
    discount_on_per_head_total = compute_addon_discount_total('discount_head');
    discount_total = compute_addon_discount_total('discount_total');
    compute_total(d_price_head,add_on_per_head_total,discount_on_per_head_total,number_of_persons, number_of_children,percent,discount,add_on_total,discount_total);

  monitor_control_changes();
  submit_package_forms();

});

</script>