<style type="text/css">
* {
    box-sizing: border-box;
}
</style>
<?php
$discount = get_post_meta( get_the_ID(), 'discount', true );
$discount_percent = get_post_meta( get_the_ID(), 'is_discount_percent', true );
?>

<div class="pack_container">
<div class="col-sm-7 parts_main">

    <?php
    if($discount>0) {
    echo "<div id='promo_box1'>PROMO FOR THIS MONTH";
    echo "<h4 style='margin:0px;padding:0px'>PER HEAD IS LESS ";
    echo $discount;
	  echo ($discount_percent=='true')?'%':'';
    echo "!</h4>";
    echo "</div>";
    }
    ?>

<div class="mid_blue_title_bars" >CEBU BOHOL 3 DAYS &amp; 2 NIGHTS PACKAGE</div>
  <div id="tour_Gallery" class="clearfix">
    <img src="<?php bloginfo('stylesheet_directory')?>/img/cebu_bohol_3d2n/cebubohol3d2n01.jpg" class="active" />
    <img src="<?php bloginfo('stylesheet_directory')?>/img/cebu_bohol_3d2n/cebubohol3d2n02.jpg" />
    <img src="<?php bloginfo('stylesheet_directory')?>/img/cebu_bohol_3d2n/cebubohol3d2n03.jpg" />
    <img src="<?php bloginfo('stylesheet_directory')?>/img/cebu_bohol_3d2n/cebubohol3d2n04.jpg" />
    <img src="<?php bloginfo('stylesheet_directory')?>/img/cebu_bohol_3d2n/cebubohol3d2n05.jpg" />
    <img src="<?php bloginfo('stylesheet_directory')?>/img/cebu_bohol_3d2n/cebubohol3d2n06.jpg" />
    <img src="<?php bloginfo('stylesheet_directory')?>/img/cebu_bohol_3d2n/cebubohol3d2n07.jpg" />
  </div>

<br/>

<!--
  <a href="#demo" class="btn btn-info" data-toggle="collapse">Simple collapsible</a>
  <div id="demo" class="collapse">
    Lorem ipsum dolor sit amet, consectetur adipisicing elit,
    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
  </div>
-->

<div class="panel-group clearfix" id="accordion" >
        <div class="panel panel-default" >
              <div class="panel-heading">
                  <h4 class="panel-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">RATE CHART</a>
                  </h4>
              </div>
              <div id="collapseOne" class="panel-collapse collapse in">
                <div class="panel-body tour_details">
                  <table class="table table-condensed">
                  <tr><td style="width:25%">No. of Person(s)</td><td style="width:75%">Price per Head</td></tr>
                  <tr><td>1</td><td>16,150 / head</td></tr>
                  <tr><td>2</td><td>11,650 / head</td></tr>
                  <tr><td>3</td><td>9,950 / head</td></tr>
                  <tr><td>4</td><td>9,250 / head</td></tr>
                  <tr><td>5</td><td>8,650 / head</td></tr>
                  <tr><td>6</td><td>8,150 / head</td></tr>
                  <tr><td>7</td><td>7,650 / head</td></tr>
                  <tr><td>8</td><td>7,150 / head</td></tr>
                  <tr><td>9</td><td>6,650 / head</td></tr>
                  <tr><td>10</td><td>6,250 / head</td></tr>
                  <tr><td>11</td><td>5,850 / head</td></tr>
                  <tr><td>12</td><td>5,450/ head</td></tr>
                  <tr><td>13</td><td>5,150 / head</td></tr>
                  <tr><td>14 and Above</td><td>Contact us for the price quotation</td></tr>
                  <tr><td colspan="2">
                    <strong>For Snorkeling with whale sharks:</strong>
                    <ul>
                    <li>Add on of Php 500/head for Foreign Guest</li>
                    <li>Whale watching viewing time is from 6AM to 11AM only</li>
                    <li>Whale watching time limit is within 30 minutes only</li>
                    </ul>
                  </td></tr>
                  </table>
                </div>
              </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">PACKAGE INCLUSIONS</a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse">
                <div class="panel-body tour_details">

              <table class="table table-condensed">
              <tr><td colspan="2">
                <ul>
                <li>Accommodation</li>
                <ul><li>2 Nights stay at Robeâ€™s Pension in Cebu City</li></ul>
                <li>Whale Watching/swimming/snorkeling</li>
                <ul>
                <li>Entrance fee</li>
                <li>boat service</li>
                <li>life vest</li>
                <li>snorkeling gear</li>
                </ul>
                <li>Sumilon Island sandbar</li>
                <ul>
                <li>Entrance fee</li>
                <li>Roundtrip pumpboat service</li>
                </ul>
                <li>Cebu/Mactan City tour</li>
                <ul><li>Venue entrance fee</li></ul>
                <li>Bohol Countryside Tour</li>
                <ul>
                <li>Venue Entrance Fees</li>
                <li>Roundtrip Oceanjet Ferry Ticket</li>
                <li>Lunch at Loboc river cruise</li>
                </ul>
                <li>Private Fully air-condition vehicle service for the whole tour</li>
                </ul>
              </td></tr>
              </table>

                </div>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">PACKAGE EXCLUSIONS</a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse">
                <div class="panel-body tour_details">
                <table class="table table-condensed">
                <tr><td colspan="2">
                <ul>
                <li>Meals (Breakfast, Lunch, Dinner)</li>
                <li>Airfare</li>
                <li>Underwater Cam<li/>
                <li>ATV and Zip ride fee at Bohol Tour<li/>
                <li>Others not stated in the inclusion above</li>
                </ul>
                </td></tr>
                </table>
                </div>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">ITINERARY</a>
                </h4>
            </div>
            <div id="collapseFour" class="panel-collapse collapse">
                <div class="panel-body tour_details">
                  <table class="table table-condensed">
                  <tr><td colspan="2">Day 1 - Whale Watching, Sumilon, Tumalog Falls</td></tr>
                  <tr><td style="width:35%">04:00 AM</td><td style="width:65%">Pick up at Hotel/Airport</td></tr>
                  <tr><td>07:00am</td><td>Breakfast at Oslob</td></tr>
                  <tr><td>07:30am</td><td>Whale shark watching/swimming</td></tr>
                  <tr><td>08:00am</td><td>Depart to Sumilon island sandbar</td></tr>
                  <tr><td>08:30am</td><td>Sumilon sandbar</td></tr>
                  <tr><td>11:30am</td><td>Depart back to Mainland</td></tr>
                  <tr><td>12:00nn</td><td>Lunch</td></tr>
                  <tr><td>01:30pm</td><td>Depart to Tumalog Falls</td></tr>
                  <tr><td>02:30pm</td><td>Depart to City</td></tr>
                  <tr><td>05:30pm</td><td>Arrival and check-in at Robeâ€™s</td></tr>
                  <tr><td colspan="2">Day 2 - Bohol Countryside Tour</td></tr>
                  <tr><td>07:00am</td><td>Pickup at Robeâ€™s</td></tr>
                  <tr><td>08:00am</td><td>Depart to Tagbilaran via Oceanjet</td></tr>
                  <tr><td>10:00am</td><td>Arrival at Bohol then Pickup and start of the tour</td></tr>
                  <tr><td colspan="2">
                  <ul>
                  <li>Blood Compact Site</li>
                  <li>Baclayon Church</li>
                  <li>St. Peter Ruins Church</li>
                  <li>River Cruise Floating Restaurant with Buffet Lunch</li>
                  <li>Bilar Man-made Forest</li>
                  <li>Tarsier Visit</li>
                  <li>Chocolate Hills</li>
                  <li>Butterfly Garden</li>
                  </ul>
                  </td></tr>
                  <tr><td>05:40pm</td><td>Depart to Cebu via Oceanjet</td></tr>
                  <tr><td>07:40pm</td><td>Arrival and pickup at Pier1</td></tr>
                  <tr><td>08:00pm</td><td>Arrival at Robeâ€™s</td></tr>
                  <tr><td colspan="2">Day 3 - Cebu/Mactan City Tour</td></tr>
                  <tr><td>09:00am</td><td>Check-out and Pickup at Robe's</td></tr>
                  <tr><td>09:30am</td><td>Fort San Pedro, Magellanâ€™s Cross, Santo Nino Church</td></tr>
                  <tr><td>10:30am</td><td>Cebu Cathedral Church, Cebu Heritage Monument, Sugbo Museo</td></tr>
                  <tr><td>12:00nn</td><td>Lunch</td></tr>
                  <tr><td>01:00pm</td><td>Pasalubong Shop, Taoist temple, Mactan Shrine</td></tr>
                  <tr><td>05:00pm</td><td>Arrival at Mactan Airport</td></tr>
                  </table>
                </div>
            </div>
          </div>
</div> <!-- accordion -->


</div> <!-- col-sm-7 -->
<div class="col-sm-5 parts_side" >
<!-- ####################################### starts here #################################### -->
<?php get_template_part('common/form','common'); ?>
<!-- ####################################### ends here #################################### -->
</div> <!-- col-sm-5 -->
</div> <!-- pack container -->


<script type="text/javascript">
  setInterval('swapImages("tour_Gallery")', 5000);

  var package_tour_name = "CEBU BOHOL 3 DAYS & 2 NIGHTS PACKAGE";

  var pkprice_per_head = [16150,11650,9950,9250,8650,8150,7650,7150,6650,6250,5850,5450,5150];
  var pk_person_variance = [];
  pk_person_variance[0] = [1,1];
  pk_person_variance[1] = [2,2];
  pk_person_variance[2] = [3,3];
  pk_person_variance[3] = [4,4];
  pk_person_variance[4] = [5,5];
  pk_person_variance[5] = [6,6];
  pk_person_variance[6] = [7,7];
  pk_person_variance[7] = [8,8];
  pk_person_variance[8] = [9,9];
  pk_person_variance[9] = [10,10];
  pk_person_variance[10] = [11,11];
  pk_person_variance[11] = [12,12];
  pk_person_variance[12] = [13,13];

  var max_persons = 0;
  var number_of_persons = 0;
  var number_of_children = 0;
  var add_on_per_head_total = 0;
  var add_on_for_total =0;
  var discount_on_per_head_total =0;
  var discount_on_total =0;
  var d_price_head =0;

  //initialize discount for children if any
  var discount = 0;
  var percent = false;

  //initialize PACKAGE PROMO if any
  var promo_discount = <?php echo $discount ?>;
  var promo_percent = <?php echo $discount_percent ?>;

  $(document).ready( function(){

    //initialize tour name
    $('#tour_name').val(package_tour_name);

    //initialize children discount
    $('#children_discount').val(discount);

    //initialize
    for(i=1;i<=99;i++) {
      $('#no_of_persons').append('<option val="'+ i + '">' + i + '</option>');
    }
    for(i=0;i<=0;i++) {
      $('#no_of_children').append('<option val="'+ i + '">' + i + '</option>');
    }
    if(discount == 0) {
      $('.children_row').hide();
    }

    //initialize other details on load
    set_other_payment_details();

    number_of_persons = $('select[name=no_of_persons]').val();
    number_of_children = $('select[name=no_of_children]').val();

    // alert("the array length : " + pk_person_variance.length);
    max_persons = pk_person_variance[pk_person_variance.length -1][1];

    // alert("the maximum person : " + max_persons);

    d_price_head = get_price_index(pkprice_per_head,pk_person_variance, number_of_persons, max_persons);
    //alert( "The price per head : " + d_price_head );

    add_on_per_head_total = compute_addon_discount_total('add_on_head');
    add_on_total = compute_addon_discount_total('add_on_total');
    discount_on_per_head_total = compute_addon_discount_total('discount_head');
    discount_total = compute_addon_discount_total('discount_total');
    compute_total(d_price_head,add_on_per_head_total,discount_on_per_head_total,number_of_persons, number_of_children,percent,discount,add_on_total,discount_total);

  monitor_control_changes();
  submit_package_forms();

});

</script>