<?php
$discount = get_post_meta( get_the_ID(), 'discount', true );
$discount_percent = get_post_meta( get_the_ID(), 'is_discount_percent', true );
?>

<div class="pack_container">
<div class="col-sm-7 parts_main">

    <?php
    if($discount>0) {
    echo "<div id='promo_box1'>PROMO FOR THIS MONTH";
    echo "<h4 style='margin:0px;padding:0px'>PER HEAD IS LESS ";
    echo $discount;
	  echo ($discount_percent=='true')?'%':'';
    echo "!</h4>";
    echo "</div>";
    }
    ?>

<div class="mid_blue_title_bars">KAWASAN FALLS / CANYONEERING</div>
  <div id="tour_Gallery" class="clearfix">
    <img src="<?php bloginfo('stylesheet_directory')?>/img/kawasan_page/kawasan01.jpg" class="active" />
    <img src="<?php bloginfo('stylesheet_directory')?>/img/kawasan_page/kawasan02.jpg" />
    <img src="<?php bloginfo('stylesheet_directory')?>/img/kawasan_page/kawasan03.jpg" />
    <img src="<?php bloginfo('stylesheet_directory')?>/img/kawasan_page/kawasan04.jpg" />
    <img src="<?php bloginfo('stylesheet_directory')?>/img/kawasan_page/kawasan05.jpg" />
    <img src="<?php bloginfo('stylesheet_directory')?>/img/kawasan_page/kawasan06.jpg" />
  </div>
<br/>

<div class="panel-group clearfix" id="accordion" >
        <div class="panel panel-default" >
              <div class="panel-heading">
                  <h4 class="panel-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">RATE CHART</a>
                  </h4>
              </div>
              <div id="collapseOne" class="panel-collapse collapse in">
                <div class="panel-body tour_details">
                  <table class="table table-condensed">
                  <tr><td style="width:25%">No. of Person(s)</td><td style="width:75%">Price per Head</td></tr>
                  <tr><td>1</td><td>6,000/head</td></tr>
                  <tr><td>2</td><td>3,700/head </td></tr>
                  <tr><td>3</td><td>3,100/head</td></tr>
                  <tr><td>4</td><td>2,800/head</td></tr>
                  <tr><td>5</td><td>2,600/head </td></tr>
                  <tr><td>6</td><td>2,400/head</td></tr>
                  <tr><td>7</td><td>2,300/head</td></tr>
                  <tr><td>8</td><td>2,100/head </td></tr>
                  <tr><td>9</td><td>2,000/head</td></tr>
                  <tr><td>10</td><td>1,900/head</td></tr>
                  <tr><td>11</td><td>1,800/head</td></tr>
                  <tr><td>12</td><td>1,750/head</td></tr>
                  <tr><td>13</td><td>1,700/head</td></tr>
                  <tr><td>14 and Above</td><td>Contact us for the price quotation</td></tr>
                  <tr><td colspan="2">
                  <p>
                  <strong>Add-on Fee:</strong><br/>
                  <ul>
                  <li>If you want to use bamboo raft = P600</li>
                  <li>if you want to use bamboo shed = P300</li>
                  <li>if you want to use bamboo cottage = P1,800</li>
                  </ul>
                  </p>
                  </td></tr>
                  </table>
                </div>
              </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">PACKAGE INCLUSIONS</a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse">
                <div class="panel-body tour_details">
                  <table class="table table-condensed">
                  <tr><td colspan="2">
                  Entrance fees<br>
                  Life Jacket<br>
                  Motorbike ride to jump off point<br>
                  Rubber shoes<br>
                  Safety helmet<br>
                  Local Canyoneering Tour Guide<br>
                  Fully air-conditioned Car or Van Service<br>
                  </td></tr>
                  </table>
                </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">PACKAGE EXCLUSIONS</a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse">
                <div class="panel-body tour_details">
                  <table class="table table-condensed">
                  <tr><td colspan="2">
                    Underwater Camera<br>
                    MEALS/SNACKS<br>
                    Accommodations<br/>
                    Air Fare<br/>
                  </td></tr>
                  </table>
                </div>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">ITINERARY</a>
                </h4>
            </div>
            <div id="collapseFour" class="panel-collapse collapse">
                <div class="panel-body tour_details">
                  <table class="table table-condensed">
                  <tr><td colspan="2">Recommended pick up time from Cebu City or Lapu-lapu City between 4:00AM to 6:00AM</td></tr>
                  <tr><td style="width:35%">5:00 AM</td><td style="width:65%">Pick up and departure time from Hotel </td></tr>
                  <tr><td>7:30 AM</td><td>Arrival at Badian then Breakfast </td></tr>
                  <tr><td>8:15 AM</td><td>Motorbike to jump off point</td></tr>
                  <tr><td>12:00 NN</td><td>End of canyoneering at Kawasan falls</td></tr>
                  <tr><td>1200 NN &#45; 1:30 PM</td><td>Lunch and cool off </td></tr>
                  <tr><td>1:45 PM</td><td>Depart from Badian</td></tr>
                  <tr><td>4:30 PM</td><td>Arrival in Metro Cebu</td></tr>
                  <tr><td colspan="2">Tour duration is maximum for 10 hrs. In case you want to have side trip (upon arrival in Cebu City), we charge P 300.00/hr.</td></tr>
                  </table>
                </div>
            </div>
          </div>
</div> <!-- accordion -->


</div> <!-- col-sm-7 -->
<div class="col-sm-5 parts_side" >
<!-- ####################################### starts here #################################### -->
<?php get_template_part('common/form','common'); ?>
<!-- ####################################### ends here #################################### -->
</div> <!-- col-sm-5 -->
</div> <!-- pack container -->

<script type="text/javascript">

  var package_tour_name = "KAWASAN FALLS / CANYONEERING";
  var pkprice_per_head = [6000,3700,3100,2800,2600,2400,2300,2100,2000,1900,1800,1750,1700];
  var pk_person_variance = [];
  pk_person_variance[0] = [1,1];
  pk_person_variance[1] = [2,2];
  pk_person_variance[2] = [3,3];
  pk_person_variance[3] = [4,4];
  pk_person_variance[4] = [5,5];
  pk_person_variance[5] = [6,6];
  pk_person_variance[6] = [7,7];
  pk_person_variance[7] = [8,8];
  pk_person_variance[8] = [9,9];
  pk_person_variance[9] = [10,10];
  pk_person_variance[10] = [11,11];
  pk_person_variance[11] = [12,12];
  pk_person_variance[12] = [13,13];

  var max_persons = 0;
  var number_of_persons = 0;
  var number_of_children = 0;
  var add_on_per_head_total = 0;
  var add_on_for_total =0;
  var discount_on_per_head_total =0;
  var discount_on_total =0;
  var d_price_head =0;

  //initialize discount for children if any
  var discount = 0;
  var percent = false;

  //initialize PACKAGE PROMO if any
  var promo_discount = <?php echo $discount ?>;
  var promo_percent = <?php echo $discount_percent ?>;


  $(document).ready( function(){

    setInterval('swapImages("tour_Gallery")', 5000);

    //initialize tour name
    $('#tour_name').val(package_tour_name);

    //initialize children discount
    $('#children_discount').val(discount);

    //initialize
    for(i=1;i<=99;i++) {
      $('#no_of_persons').append('<option val="'+ i + '">' + i + '</option>');
    }
    for(i=0;i<=0;i++) {
      $('#no_of_children').append('<option val="'+ i + '">' + i + '</option>');
    }
    if(discount == 0) {
      $('.children_row').hide();
    }

    //initialize other details on load
    set_other_payment_details();

    number_of_persons = $('select[name=no_of_persons]').val();
    number_of_children = $('select[name=no_of_children]').val();

    // alert("the array length : " + pk_person_variance.length);
    max_persons = pk_person_variance[pk_person_variance.length -1][1];

    // alert("the maximum person : " + max_persons);

    d_price_head = get_price_index(pkprice_per_head,pk_person_variance, number_of_persons, max_persons);
    //alert( "The price per head : " + d_price_head );

    add_on_per_head_total = compute_addon_discount_total('add_on_head');
    add_on_total = compute_addon_discount_total('add_on_total');
    discount_on_per_head_total = compute_addon_discount_total('discount_head');
    discount_total = compute_addon_discount_total('discount_total');
    compute_total(d_price_head,add_on_per_head_total,discount_on_per_head_total,number_of_persons, number_of_children,percent,discount,add_on_total,discount_total);

  monitor_control_changes();
  submit_package_forms();

});

</script>