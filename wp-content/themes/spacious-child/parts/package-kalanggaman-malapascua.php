<?php
$discount = get_post_meta( get_the_ID(), 'discount', true );
$discount_percent = get_post_meta( get_the_ID(), 'is_discount_percent', true );
?>

<div class="pack_container">
<div class="col-sm-7 parts_main">

    <?php
    if($discount>0) {
    echo "<div id='promo_box1'>PROMO FOR THIS MONTH";
    echo "<h4 style='margin:0px;padding:0px'>PER HEAD IS LESS ";
    echo $discount;
	  echo ($discount_percent=='true')?'%':'';
    echo "!</h4>";
    echo "</div>";
    }
    ?>

<div class="mid_blue_title_bars">MALAPASCUA+KALANGGAMAN ISLAND OVERNIGHT PACKAGE
<br/>
<span style="font-size:.8em">This is the best overnight package for your family.
</span>
</div>
<div style="line-height:1.2em;padding:10px;text-align:center;">
&bull; Malapascua Island Tour
&bull; Kalanggaman Island Tour
&bull; Accommodation
&bull; Roundtrip service land and sea
</div>
  <div id="tour_Gallery" class="clearfix">
    <img src="<?php bloginfo('stylesheet_directory')?>/img/kalanggaman/kalanggaman01.jpg" class="active" />
    <img src="<?php bloginfo('stylesheet_directory')?>/img/kalanggaman/kalanggaman02.jpg"/>
    <img src="<?php bloginfo('stylesheet_directory')?>/img/kalanggaman/kalanggaman03.jpg"/>
    <img src="<?php bloginfo('stylesheet_directory')?>/img/kalanggaman/kalanggaman04.jpg"/>
    <img src="<?php bloginfo('stylesheet_directory')?>/img/malapascua/malapascua01.jpg"/>
    <img src="<?php bloginfo('stylesheet_directory')?>/img/malapascua/malapascua02.jpg"/>
  </div>
<br/>

<div class="panel-group clearfix" id="accordion" >
        <div class="panel panel-default" >
              <div class="panel-heading">
                  <h4 class="panel-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">RATE CHART</a>
                  </h4>
              </div>
              <div id="collapseOne" class="panel-collapse collapse in">
                <div class="panel-body tour_details">
                  <table class="table table-condensed">
                  <tr><td style="width:25%">No. of Person(s)</td><td style="width:75%">Price per Head</td></tr>
                  <tr><td>2</td><td>14,800 / head</td></tr>
                  <tr><td>3</td><td>11,200 / head</td></tr>
                  <tr><td>4</td><td>9,200 / head</td></tr>
                  <tr><td>5</td><td>8,400 / head</td></tr>
                  <tr><td>6</td><td>7,800 / head</td></tr>
                  <tr><td>7</td><td>6,900 / head</td></tr>
                  <tr><td>8</td><td>6,600 / head</td></tr>
                  <tr><td>9</td><td>6,400 / head</td></tr>
                  <tr><td>10</td><td>6,300 / head</td></tr>
                  <tr><td>11</td><td>6,200 / head</td></tr>
                  <tr><td>12</td><td>6,100 / head</td></tr>
                  <tr><td>13</td><td>6,000 / head</td></tr>
                  <tr><td>14 and above</td><td>Contact us for the price quotation</td></tr>
                  </table>
                </div>
              </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">PACKAGE INCLUSIONS</a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse">
                <div class="panel-body tour_details">
                <table class="table table-condensed">
                <tr><td colspan="2">
                <ul>
                  <li>Private Fully Air-conditioned car or van service â€“ Roundtrip transfer from City-Maya Port</li>
                  <li>Room Accommodation 1Night at Malapascua resort (Fan room type)</li>
                  <li>Roundtrip Pumpboat Service to/from Maya port to Malapascua/Kalanggaman</li>
                  <li>Life Jacket</li>
                  <li>Meals: 2 Breakfasts, 2 Lunches, 1 Dinner</li>
                  <li>Tour Guide Fee</li>
                  <li>Islands Entrance fee</li>
                  <li>Private charter boat for island tours</li>
                  <li>Free videoke (for 9heads and above)</li>
                </ul>
                </td></tr>
                </table>
                </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">PACKAGE EXCLUSIONS</a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse">
                <div class="panel-body tour_details">
                  <table class="table table-condensed">
                  <tr><td colspan="2">
                  <ul>
                    <li>Airfare</li>
                    <li>Underwater Camera</li>
                    <li>Snorkeling gear</li>
                  </ul>
                  </td></tr>
                  </table>
                </div>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">ITINERARY</a>
                </h4>
            </div>
            <div id="collapseFour" class="panel-collapse collapse">
                <div class="panel-body tour_details">
                  <table class="table table-condensed">
                  <tr><td colspan="2" style="background-color:#faffaf">DAY 1</td></tr>
                  <tr><td style="width:35%">04:00am</td><td>Pickup at Hotel/Airport</td></tr>
                  <tr><td>07:00am</td><td>   Arrival at Maya Port</td></tr>
                  <tr><td>07:15am</td><td>   Depart to Malapascua island</td></tr>
                  <tr><td>08:00am</td><td>   Arrival at Malapascua resort</td></tr>
                  <tr><td>08:30am</td><td>   Breakfast</td></tr>
                  <tr><td>09:00am</td><td>   Depart to Kalanggaman island</td></tr>
                  <tr><td>11:00am</td><td>   Arrival Kalanggaman island, swimming/snorkeling</td></tr>
                  <tr><td>12:00nn</td><td>   Lunch</td></tr>
                  <tr><td>03:00pm</td><td>   Depart back to Malapascua</td></tr>
                  <tr><td>05:00pm</td><td>   Arrival at Malapascua resort</td></tr>
                  <tr><td>06:30pm</td><td>   Dinner
                  <tr><td colspan="2"  style="background-color:#faffaf">DAY 2</td></tr>
                  <tr><td style="width:35%">07:30am</td><td>Breakfast</td></tr>
                  <tr><td>08:30am</td><td>    Malapascua Tour</td></tr>
                  <tr><td>11:00am</td><td>   Back at resort</td></tr>
                  <tr><td>12:00nn</td><td>   Lunch Time</td></tr>
                  <tr><td>01:00pm</td><td>   Depart to Maya Port</td></tr>
                  <tr><td>02:00pm</td><td>   Arrival Maya Port</td></tr>
                  <tr><td>02:15pm</td><td>   Depart to Cebu City</td></tr>
                  <tr><td>05:30pm</td><td>   Arrival in Cebu City</td></tr>
                  </table>
                </div>
            </div>
          </div>
</div> <!-- accordion -->


</div> <!-- col-sm-7 -->
<div class="col-sm-5 parts_side" >
<!-- ####################################### starts here #################################### -->
<?php get_template_part('common/form','common'); ?>
<!-- ####################################### ends here #################################### -->
</div> <!-- col-sm-5 -->
</div> <!-- pack container -->


<script type="text/javascript">
  setInterval('swapImages("tour_Gallery")', 5000);

  var package_tour_name = "OVERNIGHT MALAPASCUA KALANGGAMAN ISLAND";
  var pkprice_per_head = [14800,11200,9200,8400,7800,6900,6600,6400,6300,6200,6100,6000];
  var pk_person_variance = [];
  pk_person_variance[0] = [2,2];
  pk_person_variance[1] = [3,3];
  pk_person_variance[2] = [4,4];
  pk_person_variance[3] = [5,5];
  pk_person_variance[4] = [6,6];
  pk_person_variance[5] = [7,7];
  pk_person_variance[6] = [8,8];
  pk_person_variance[7] = [9,9];
  pk_person_variance[8] = [10,10];
  pk_person_variance[9] = [11,11];
  pk_person_variance[10] = [12,12];
  pk_person_variance[11] = [13,13];

  var max_persons = 0;
  var number_of_persons = 0;
  var number_of_children = 0;
  var add_on_per_head_total = 0;
  var add_on_for_total =0;
  var discount_on_per_head_total =0;
  var discount_on_total =0;
  var d_price_head =0;

  //initialize discount for children if any
  var discount = 0;
  var percent = false;

  //initialize PACKAGE PROMO if any
  var promo_discount = <?php echo $discount ?>;
  var promo_percent = <?php echo $discount_percent ?>;

  $(document).ready( function(){

    //initialize tour name
    $('#tour_name').val(package_tour_name);

    //initialize children discount
    $('#children_discount').val(discount);

    //initialize
    for(i=2;i<=99;i++) {
      $('#no_of_persons').append('<option val="'+ i + '">' + i + '</option>');
    }
    for(i=0;i<=0;i++) {
      $('#no_of_children').append('<option val="'+ i + '">' + i + '</option>');
    }
    if(discount == 0) {
      $('.children_row').hide();
    }

    //initialize other details on load
    set_other_payment_details();

    number_of_persons = $('select[name=no_of_persons]').val();
    number_of_children = $('select[name=no_of_children]').val();

    // alert("the array length : " + pk_person_variance.length);
    max_persons = pk_person_variance[pk_person_variance.length -1][1];

    // alert("the maximum person : " + max_persons);

    d_price_head = get_price_index(pkprice_per_head,pk_person_variance, number_of_persons, max_persons);
    //alert( "The price per head : " + d_price_head );

    add_on_per_head_total = compute_addon_discount_total('add_on_head');
    add_on_total = compute_addon_discount_total('add_on_total');
    discount_on_per_head_total = compute_addon_discount_total('discount_head');
    discount_total = compute_addon_discount_total('discount_total');
    compute_total(d_price_head,add_on_per_head_total,discount_on_per_head_total,number_of_persons, number_of_children,percent,discount,add_on_total,discount_total);

  monitor_control_changes();
  submit_package_forms();

});

</script>
