<?php
$discount = get_post_meta( get_the_ID(), 'discount', true );
$discount_percent = get_post_meta( get_the_ID(), 'is_discount_percent', true );
?>

<div class="pack_container">
<div class="col-sm-7 parts_main">

    <?php
    if($discount>0) {
    echo "<div id='promo_box1'>PROMO FOR THIS MONTH";
    echo "<h4 style='margin:0px;padding:0px'>PER HEAD IS LESS ";
    echo $discount;
	  echo ($discount_percent=='true')?'%':'';
    echo "!</h4>";
    echo "</div>";
    }
    ?>

<div class="mid_blue_title_bars" >OSLOB OVERNIGHT (2D/1N) BUDGET PACKAGE
<br/><span style="font-size:.8em">Enjoy an overnight adventure just right for your budget!</span></div>
  <div id="tour_Gallery" class="clearfix">
    <img src="<?php bloginfo('stylesheet_directory')?>/img/oslob_overnight_budget/oslob_overnight_budget01.jpg" class="active" />
    <img src="<?php bloginfo('stylesheet_directory')?>/img/oslob_overnight_budget/oslob_overnight_budget02.jpg" />
    <img src="<?php bloginfo('stylesheet_directory')?>/img/oslob_overnight_budget/oslob_overnight_budget03.jpg" />
    <img src="<?php bloginfo('stylesheet_directory')?>/img/oslob_overnight_budget/oslob_overnight_budget04.jpg" />
    <img src="<?php bloginfo('stylesheet_directory')?>/img/oslob_overnight_budget/oslob_overnight_budget05.jpg" />
  </div>
<br/>

<div class="panel-group clearfix" id="accordion" >
        <div class="panel panel-default" >
              <div class="panel-heading">
                  <h4 class="panel-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">RATE CHART</a>
                  </h4>
              </div>
              <div id="collapseOne" class="panel-collapse collapse in">
                <div class="panel-body tour_details">
                  <table class="table table-condensed">
                  <tr><td style="width:25%">No. of Person(s)</td><td style="width:75%">Price per Head</td></tr>
                  <tr><td>1</td><td>13,000 / head</td></tr>
                  <tr><td>2</td><td>7,900 / head</td></tr>
                  <tr><td>3</td><td>6,500 / head</td></tr>
                  <tr><td>4</td><td>5,800 / head</td></tr>
                  <tr><td>5</td><td>5,000 / head</td></tr>
                  <tr><td>6</td><td>4,600 / head</td></tr>
                  <tr><td>7</td><td>4,500 / head</td></tr>
                  <tr><td>8</td><td>4,300 / head</td></tr>
                  <tr><td>9</td><td>4,100 / head</td></tr>
                  <tr><td>10</td><td>3,900 / head</td></tr>
                  <tr><td>11</td><td>3,600 / head</td></tr>
                  <tr><td>12</td><td>3,300 / head</td></tr>
                  <tr><td>13 and Above</td><td>Contact us for the price quotation</td></tr>
                  <tr><td colspan="2">
                  <p>
                  <strong>Note:</strong> For foreign guest â€“ add-on of 500/head for whale watching and not included in this package
                  </p>
                  <p style="background-color:#000000;padding:5px">
                  <strong style="color:#ffffff">NOTE : </strong><span style="color:#F5FFB3">Please check "INCLUDE MEALS (Add-on)" checkbox if you want MEALS (Breakfast, Lunch &amp; Dinner) to be included. Rate per Head will increase (Additional Fee).</span>
                  </p>
                  </td></tr>
                  </table>
                </div>
              </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">PACKAGE INCLUSIONS</a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse">
                <div class="panel-body tour_details">
                  <table class="table table-condensed">
                  <tr><td colspan="2">
                  <ul>
                    <li>Whale Watching Fee</li>
                    <li>Boat ride for whale watching</li>
                    <li>Life vest and snorkeling gear during whale watching</li>
                    <li>Fully Air-conditioned car or van service</li>
                    <li>Room Accommodation 1Night at Lagnasonâ€™s Place resort</li>
                    <li>Boat transfer service to Sumilon and back</li>
                    <li>Tumalog Falls fee and motorbike service</li>
                    <li>Osmena Peak fee</li>
                  </ul>
                  </td></tr>
                  </table>
                </div>
            </div>
          </div>
          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">PACKAGE EXCLUSIONS</a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse">
                <div class="panel-body tour_details">
                  <table class="table table-condensed">
                  <tr><td colspan="2">
                  <ul>
                    <li>Airfare</li>
                    <li>Underwater Camera (You can rent from the locals)</li>
                    <li>Meals (Breakfast, Lunch &amp; Dinner)</li>
                    <li>Snorkeling gear in sumilon</li>
                    <li>Bluewater resort in sumilon</li>
                  </ul>
                  </td></tr>
                  </table>
                </div>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">ITINERARY</a>
                </h4>
            </div>
            <div id="collapseFour" class="panel-collapse collapse">
                <div class="panel-body tour_details">
                  <table class="table table-condensed">
                  <tr><td colspan="2" style="background-color:#faffaf">DAY 1</td></tr>
                  <tr><td style="width:35%">07:00 AM</td><td>Pickup at any point in Metro Cebu (Hotels/Airport)</td></tr>
                  <tr><td>09:30 AM</td><td>Arrival at Osmena Peak</td></tr>
                  <tr><td>11:00 AM</td><td>Depart for Oslob</td></tr>
                  <tr><td>12:00 NN</td><td>Arrival at Oslob Resort / Check-in / Lunch</td></tr>
                  <tr><td>01:00 PM</td><td>Depart for Tumalog Falls</td></tr>
                  <tr><td>04:30 PM</td><td>Depart back to Resort</td></tr>
                  <tr><td>06:00 PM</td><td>Dinner at Resort</td></tr>
                  <tr><td>07:00 PM</td><td>Relax or Night Swimming at Resort</td></tr>
                  <tr><td colspan="2"  style="background-color:#faffaf">DAY 2</td></tr>
                  <tr><td style="width:35%">06:00 AM</td><td>Breakfast </td></tr>
                  <tr><td>06:30 AM</td><td>Depart to Oslob Whale Watching</td></tr>
                  <tr><td>08:00 AM</td><td>Depart to Sumilon Island</td></tr>
                  <tr><td>11:00 AM</td><td>Depart to Oslob</td></tr>
                  <tr><td>11:30 AM</td><td>Check out resort</td></tr>
                  <tr><td>12:00 NN</td><td>Lunch Time</td></tr>
                  <tr><td>01:00 PM</td><td>Depart to Simala Shrine</td></tr>
                  <tr><td>02:30 PM</td><td>Arrival Simala Shrine</td></tr>
                  <tr><td>04:00 PM</td><td>Depart to Cebu City</td></tr>
                  <tr><td>06:30 PM</td><td>Arrival in Cebu City</td></tr>
                  </table>
                </div>
            </div>
          </div>

</div> <!-- accordion -->


</div> <!-- col-sm-7 -->
<div class="col-sm-5 parts_side" >
<!-- ####################################### starts here #################################### -->
<?php get_template_part('common/form','common'); ?>
<!-- ####################################### ends here #################################### -->
</div> <!-- col-sm-5 -->
</div> <!-- pack container -->


  <script type="text/javascript">
  var package_tour_name = "OSLOB OVERNIGHT (2D/1N) BUDGET PACKAGE";
  var pkprice_per_head = [13000,7900,6500,5800,5000,4600,4500,4300,4100,3900,3600,3300];
  var pk_person_variance = [];
  pk_person_variance[0] = [1,1];
  pk_person_variance[1] = [2,2];
  pk_person_variance[2] = [3,3];
  pk_person_variance[3] = [4,4];
  pk_person_variance[4] = [5,5];
  pk_person_variance[5] = [6,6];
  pk_person_variance[6] = [7,7];
  pk_person_variance[7] = [8,8];
  pk_person_variance[8] = [9,9];
  pk_person_variance[9] = [10,10];
  pk_person_variance[10] = [11,11];
  pk_person_variance[11] = [12,12];

  var max_persons = 0;
  var number_of_persons = 0;
  var number_of_children = 0;
  var add_on_per_head_total = 0;
  var add_on_for_total =0;
  var discount_on_per_head_total =0;
  var discount_on_total =0;
  var d_price_head =0;

  //initialize discount for children if any
  var discount = 0;
  var percent = false;

  //initialize PACKAGE PROMO if any
  var promo_discount = <?php echo $discount ?>;
  var promo_percent = <?php echo $discount_percent ?>;

  $(document).ready( function(){

    setInterval('swapImages("tour_Gallery")', 5000);

    //initialize tour name
    $('#tour_name').val(package_tour_name);

    //initialize children discount
    $('#children_discount').val(discount);

    //initialize
    for(i=1;i<=99;i++) {
      $('#no_of_persons').append('<option val="'+ i + '">' + i + '</option>');
    }
    for(i=0;i<=0;i++) {
      $('#no_of_children').append('<option val="'+ i + '">' + i + '</option>');
    }
    if(discount == 0) {
      $('.children_row').hide();
    }

    //initialize other details on load
    set_other_payment_details();

    number_of_persons = $('select[name=no_of_persons]').val();
    number_of_children = $('select[name=no_of_children]').val();

    // alert("the array length : " + pk_person_variance.length);
    max_persons = pk_person_variance[pk_person_variance.length -1][1];

    // alert("the maximum person : " + max_persons);

    d_price_head = get_price_index(pkprice_per_head,pk_person_variance, number_of_persons, max_persons);
    //alert( "The price per head : " + d_price_head );

    add_on_per_head_total = compute_addon_discount_total('add_on_head');
    add_on_total = compute_addon_discount_total('add_on_total');
    discount_on_per_head_total = compute_addon_discount_total('discount_head');
    discount_total = compute_addon_discount_total('discount_total');
    compute_total(d_price_head,add_on_per_head_total,discount_on_per_head_total,number_of_persons, number_of_children,percent,discount,add_on_total,discount_total);

  monitor_control_changes();
  submit_package_forms();

});

</script>
