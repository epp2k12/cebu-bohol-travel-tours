<style type="text/css">
* {
    box-sizing: border-box;
}
</style>
<?php
$discount = get_post_meta( get_the_ID(), 'discount', true );
$discount_percent = get_post_meta( get_the_ID(), 'is_discount_percent', true );
?>

<div class="pack_container">
<div class="col-sm-7 parts_main">

    <?php
    if($discount>0) {
    echo "<div id='promo_box1'>PROMO FOR THIS MONTH";
    echo "<h4 style='margin:0px;padding:0px'>PER HEAD IS LESS ";
    echo $discount;
	  echo ($discount_percent=='true')?'%':'';
    echo "!</h4>";
    echo "</div>";
    }
    ?>

<div class="mid_blue_title_bars" >BOHOL COUNTRYSIDE TOURs</div>


  <div id="tour_Gallery" class="clearfix">
    <img src="<?php bloginfo('stylesheet_directory')?>/img/bohol_tours/boholtours01.jpg" class="active" />
    <img src="<?php bloginfo('stylesheet_directory')?>/img/bohol_tours/boholtours02.jpg" />
    <img src="<?php bloginfo('stylesheet_directory')?>/img/bohol_tours/boholtours03.jpg" />
    <img src="<?php bloginfo('stylesheet_directory')?>/img/bohol_tours/boholtours04.jpg" />
    <img src="<?php bloginfo('stylesheet_directory')?>/img/bohol_tours/boholtours05.jpg" />
    <img src="<?php bloginfo('stylesheet_directory')?>/img/bohol_tours/boholtours06.jpg" />
    <img src="<?php bloginfo('stylesheet_directory')?>/img/bohol_tours/boholtours07.jpg" />
    <img src="<?php bloginfo('stylesheet_directory')?>/img/bohol_tours/boholtours08.jpg" />
  </div>

<br/>

<!--
  <a href="#demo" class="btn btn-info" data-toggle="collapse">Simple collapsible</a>
  <div id="demo" class="collapse">
    Lorem ipsum dolor sit amet, consectetur adipisicing elit,
    sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
    quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
  </div>
-->

<div class="panel-group clearfix" id="accordion" >
        <div class="panel panel-default" >
              <div class="panel-heading">
                  <h4 class="panel-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">RATE CHART</a>
                  </h4>
              </div>
              <div id="collapseOne" class="panel-collapse collapse in">

                  <div class="panel-body tour_details">
					  <table class="table table-condensed">
					  <tr><td style="width:25%">No. of Person(s)</td><td style="width:75%">Price per Head</td></tr>
					  <tr><td>1</td><td>8,010 /head</td></tr>
					  <tr><td>2</td><td>5,510 /head </td></tr>
					  <tr><td>3</td><td>4,677 /head</td></tr>
					  <tr><td>4</td><td>4,210 /head</td></tr>
					  <tr><td>5</td><td>3,970 /head </td></tr>
					  <tr><td>6</td><td>3,953 /head</td></tr>
					  <tr><td>7</td><td>3,927 /head</td></tr>
					  <tr><td>8</td><td>3,773 /head </td></tr>
					  <tr><td>9</td><td>3,743 /head</td></tr>
					  <tr><td>10</td><td>3,660 /head</td></tr>
					  <tr><td>11</td><td>3,555/head</td></tr>
					  <tr><td>12</td><td>3,468/head</td></tr>
					  <tr><td>13</td><td>3,395/head</td></tr>
					  <tr><td>14 and Above</td><td>Contact us for the price quotation</td></tr>
					  </table>
                  </div>
              </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">PACKAGE INCLUSIONS</a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse">
                <div class="panel-body tour_details">

				   <table class="table table-condensed">
				   <tr><td colspan="2">
					 Pickup service from Hotel to Wharf<br/>
					 Round Trip Fast Craft Ticket<br/>
					 Buffet Lunch at Floating Restaurant<br/>
					 Venue Entrance Fees<br/>
					 Fully Air-conditioned Van for Bohol Transport<br/>
					 Pick-up Service from Cebu Wharf to Cebu Hotel<br/>
				   </td></tr>
				   </table>

                </div>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">PACKAGE EXCLUSIONS</a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse">
                <div class="panel-body tour_details">
				   <table class="table table-condensed">
				   <tr><td colspan="2">
				     BikeZip, ATV ride<br/>
					 Breakfast and Dinner<br/>
					 Accommodations<br/>
					 Air Fare<br/>
				   </td></tr>
				  </table>
                </div>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">ITINERARY</a>
                </h4>
            </div>
            <div id="collapseFour" class="panel-collapse collapse">
                <div class="panel-body tour_details">

					<table class="table table-condensed">
						<tr><td style="width:35%">6:00 AM </td><td style="width:65%"> Pickup at hotel then drop off to pier 1 (fast Craft)</td></tr>
						<tr><td>8:00 AM </td><td>Departure for Bohol</td></tr>
						<tr><td>10:00 AM </td><td>Arrival at Bohol then Pickup and start of the tour</td></tr>
						<tr><td colspan="2">
						<ul>
						   <li>Blood Compact Site</li>
						   <li>Baclayon Church</li>
						   <li>St. Peter's Ruins Church</li>
						   <li>Bohol Biggest Snake in Captivity</li>
						   <li>River Cruise Floating Restaurant with Buffet Lunch</li>
						   <li>Bilar Man-made Forest</li>
						   <li>Tarsier Visit</li>
						   <li>Chocolate Hills</li>
						   <li>Butterfly Garden</li>
						</ul>
						</td></tr>
						<tr><td>4:00 PM </td><td>End of Tour Proceed to Sea Port</td></tr>
						<tr><td>5:30 PM </td><td>Departure for Cebu City</td></tr>
						<tr><td>7:30 PM </td><td>Pickup from Cebu SeaPort/Wharf then Drop off to Hotel</td></tr>
					</table>

                </div>
            </div>
          </div>


</div> <!-- accordion -->


</div> <!-- col7 -->
<div class="col-sm-5 parts_side" >

<!-- ####################################### starts here #################################### -->
<?php get_template_part('common/form','common'); ?>


<!-- ####################################### ends here #################################### -->

</div> <!-- col3 -->
</div> <!-- container -->


  <script type="text/javascript">

  var package_tour_name = "BOHOL COUNTRYSIDE TOUR";

  var pkprice_per_head = [8010,5510,4677,4210,3970,3953,3927,3773,3743,3660,3555,3468,3395];
  var pk_person_variance = [];
  pk_person_variance[0] = [1,1];
  pk_person_variance[1] = [2,2];
  pk_person_variance[2] = [3,3];
  pk_person_variance[3] = [4,4];
  pk_person_variance[4] = [5,5];
  pk_person_variance[5] = [6,6];
  pk_person_variance[6] = [7,7];
  pk_person_variance[7] = [8,8];
  pk_person_variance[8] = [9,9];
  pk_person_variance[9] = [10,10];
  pk_person_variance[10] = [11,11];
  pk_person_variance[11] = [12,12];
  pk_person_variance[12] = [13,13];

  var max_persons = 0;
  var number_of_persons = 0;
  var number_of_children = 0;
  var add_on_per_head_total = 0;
  var add_on_for_total =0;
  var discount_on_per_head_total =0;
  var discount_on_total =0;
  var d_price_head =0;

  //initialize discount for children if any
  var discount = 0;
  var percent = false;

  //initialize PACKAGE PROMO if any
  var promo_discount = <?php echo $discount ?>;
  var promo_percent = <?php echo $discount_percent ?>;

  $(document).ready( function(){

    setInterval('swapImages("tour_Gallery")', 5000);

    //initialize tour name
    $('#tour_name').val(package_tour_name);

    //initialize children discount
    $('#children_discount').val(discount);

    //initialize
    for(i=1;i<=99;i++) {
      $('#no_of_persons').append('<option val="'+ i + '">' + i + '</option>');
    }
    for(i=0;i<=0;i++) {
      $('#no_of_children').append('<option val="'+ i + '">' + i + '</option>');
    }
    if(discount == 0) {
      $('.children_row').hide();
    }

    //initialize other details on load
    set_other_payment_details();

    number_of_persons = $('select[name=no_of_persons]').val();
    number_of_children = $('select[name=no_of_children]').val();

    max_persons = pk_person_variance[pk_person_variance.length -1][1];

    d_price_head = get_price_index(pkprice_per_head,pk_person_variance, number_of_persons, max_persons);
    //alert( "The price per head : " + d_price_head );

    add_on_per_head_total = compute_addon_discount_total('add_on_head');
    add_on_total = compute_addon_discount_total('add_on_total');
    discount_on_per_head_total = compute_addon_discount_total('discount_head');
    discount_total = compute_addon_discount_total('discount_total');
    compute_total(d_price_head,add_on_per_head_total,discount_on_per_head_total,number_of_persons, number_of_children,percent,discount,add_on_total,discount_total);

	monitor_control_changes();
	submit_package_forms();

});

</script>
