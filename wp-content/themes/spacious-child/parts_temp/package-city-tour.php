<style type="text/css">
* {
    box-sizing: border-box;
}
</style>
<?php
$discount = get_post_meta( get_the_ID(), 'discount', true );
$discount_percent = get_post_meta( get_the_ID(), 'is_discount_percent', true );
?>
<div class="pack_container">
<div class="col7">
<div class="jumbotron">

    <?php
    if($discount>0) {
    echo "<div id='promo_box1'>PROMO FOR THIS MONTH";
    echo "<h4 style='margin:0px;padding:0px'>RATE/HEAD IS LESS ";
    echo $discount;
	echo ($discount_percent=='true')?'%':'';
    echo "!</h4>";
    echo "</div>";
    }
    ?>

<div class="mid_blue_title_bars" >CEBU CITY DAY TOUR</div>
  <div id="tour_Gallery" style="margin:0 auto;">
    <img src="<?php bloginfo('stylesheet_directory')?>/img/city_tours/citytours01.png" class="active" />
    <img src="<?php bloginfo('stylesheet_directory')?>/img/city_tours/citytours02.png" />
    <img src="<?php bloginfo('stylesheet_directory')?>/img/city_tours/citytours03.png" />
    <img src="<?php bloginfo('stylesheet_directory')?>/img/city_tours/citytours04.png" />
    <img src="<?php bloginfo('stylesheet_directory')?>/img/city_tours/citytours05.png" />
    <img src="<?php bloginfo('stylesheet_directory')?>/img/city_tours/citytours06.png" />
    <img src="<?php bloginfo('stylesheet_directory')?>/img/city_tours/citytours07.png" />
    <img src="<?php bloginfo('stylesheet_directory')?>/img/city_tours/citytours08.png" />
    <img src="<?php bloginfo('stylesheet_directory')?>/img/city_tours/citytours09.png" />
    <img src="<?php bloginfo('stylesheet_directory')?>/img/city_tours/citytours10.png" />
    <img src="<?php bloginfo('stylesheet_directory')?>/img/city_tours/citytours11.png" />
  </div>
</div>
<br/>
<div class="panel-group" id="accordion" >
        <div class="panel panel-default" >
              <div class="panel-heading">
                  <h4 class="panel-title">
                      <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">RATE CHART</a>
                  </h4>
              </div>
              <div id="collapseOne" class="panel-collapse collapse in">

                  <div class="panel-body tour_details">
					  <table class="table table-condensed">
					  <tr><td style="width:30%">No. of Person(s)</td><td style="width:70%">Price per Head ()</td></tr>
					  <tr><td>1</td><td>2,900/head</td></tr>
					  <tr><td>2</td><td>1,600/head </td></tr>
					  <tr><td>3</td><td>1,200/head</td></tr>
					  <tr><td>4</td><td>1,050/head</td></tr>
					  <tr><td>5</td><td>900/head </td></tr>
					  <tr><td>6</td><td>800/head</td></tr>
					  <tr><td>7</td><td>750/head</td></tr>
					  <tr><td>8</td><td>700/head </td></tr>
					  <tr><td>9</td><td>650/head</td></tr>
					  <tr><td>10</td><td>600/head</td></tr>
					  <tr><td>11</td><td>550/head</td></tr>
					  <tr><td>12</td><td>500/head</td></tr>
					  <tr><td>13</td><td>500/head</td></tr>
					  <tr><td>14 and Above</td><td>Contact us for the price quotation</td>
					  </tr>
					  <tr><td colspan="3">
							Children / Kids Pricing
							<ul>
							<li>Child below 2 years old - FREE/Not included in headcount</li>
							<li>Child 3 to 7 years old - Less Php300</li>
							<li>Child above 8 years old - Full Charge</li>
							</ul>
					  </td></tr>
					  </table>
                  </div>
              </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">PACKAGE INCLUSIONS</a>
                </h4>
            </div>
            <div id="collapseTwo" class="panel-collapse collapse">
                <div class="panel-body tour_details">

				  <table class="table table-condensed">
				  <tr><td colspan="2">
				  Entrance fees<br>
				  Fully air-conditioned Car or Van Service<br>
				  </td></tr>
				  </table>

                </div>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFive">PACKAGE EXCLUSIONS</a>
                </h4>
            </div>
            <div id="collapseFive" class="panel-collapse collapse">
                <div class="panel-body tour_details">

				  <table class="table table-condensed">
				  <tr><td colspan="2">
				  	MEALS/SNACKS<br>
				  	Accommodations<br/>
					Air Fare<br/>
				  </td></tr>
				  </table>

                </div>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">ITINERARY</a>
                </h4>
            </div>
            <div id="collapseThree" class="panel-collapse collapse">
                <div class="panel-body tour_details">

				  <table class="table table-condensed">
				  <tr><td width="30%">9:00 AM </td><td width="70%"> Pick up and departure time from Hotel </td></tr>
				  <tr><td>9:45 AM &#45; 11:30 AM </td><td> Fort San Pedro, Magellan&#39;s Cross, Santo Nino, Cebu Heritage Monument, Yap-Sandiego Ancestral House  </td></tr>
				  <tr><td>1200 NN &#45; 1:30 PM </td><td> Lunch Break</td></tr>
				  <tr><td>1:30 PM &#45; 4:00 PM </td><td> Museo Sugbo, Cebu Taoist Temple, Mactan Shrine </td></tr>
				  <tr><td>5:00 PM </td><td>Back at hotel  </td></tr>
				  <tr><td colspan="2">Tour duration is maximum for 8 hrs. In case you want to have side trip, we will charge P 300.00/hr.</td></tr>
				  </table>
				  <table class="table table-condensed">
				  <tr><td colspan ="2"> WITH TEMPLE OF LEA &amp; TOPS </td></tr>
				  <tr><td width="30%">08:00 AM </td><td width="70%"> Pick up at hotel or airport</td></tr>
				  <tr><td>08:30 AM &#45; 11:30 AM </td><td> Mactan Shrine, Fort San Pedro, Magellan&#39;s Cross, Santo Nino, Cebu Heritage Monument, Yap-Sandiego Ancestral House  </td></tr>
				  <tr><td>12:00 NN </td><td> Lunch Break (Guest Own Expense)</td></tr>
				  <tr><td>01:00 PM &#45; 5:00 PM </td><td> Museo Sugbo, Cebu Taoist Temple, Pasalubong Shop, Temple of Leah, Cebu Tops </td></tr>
				  <tr><td>06:00 PM </td><td>Back at hotel  </td></tr>
				  <tr><td colspan="2">Tour duration is maximum for 10 hrs. In case you want to have side trip, we will charge P 300.00/hr.</td></tr>
				  </table>




                </div>
            </div>
          </div>

          <div class="panel panel-default">
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseFour">DAY TOUR SERVICE</a>
                </h4>
            </div>
            <div id="collapseFour" class="panel-collapse collapse">
                <div class="panel-body tour_details">

				  <table class="table table-condensed">
				  <tr><td colspan="2">1 Day private tour (8 hours duration)</td></tr>
				  <tr><td colspan="2">Driver as guide</td></tr>
				  <tr><td colspan="2">Fully air-conditioned vehicle service</td></tr>
				   <tr><td colspan="2">
					 <ul>
					 <li>1 to 3 persons &#45; sedan type vehicle</li>
					 <li>4 to 6 persons &#45; minivan type vehicle</li>
					 <li>7 to 13 persons &#45; van type vehicle</li>
					 <li>14 and above &#45; 2 vehicles or it depends on the number of heads</li>
					 </ul>
				   </td></tr>
				  </table>

                </div>
            </div>
          </div>

</div> <!-- accordion -->



</div> <!-- col7 -->
<div class="col3">

<iframe width="560" height="315" src="https://www.youtube.com/embed/4dnsNWerCl8?autoplay=1" frameborder="0" allowfullscreen></iframe>

<!-- ####################################### starts here #################################### -->


  <form id="form_compute">
  <input type="hidden" name="submitted" value="true">
  <input type="hidden" name="reservation_mode" id="reservation_mode" value="">
  <input type="hidden" name="other_payment_details" id="other_payment_details" value="">
  <input type="hidden" name="children_discount" id="children_discount" value="">

  <div id="form_compute_status"></div>
  <table class="table table-condensed table_compute" id="table_compute" >

  <tr><td colspan="4" style="background-color:#000000;color:#ffffff" >
  <input type="text" class="hilite_value" name="tour_name" id="tour_name" readonly style="background-color:#000000;color:#ffffff;border:none;">
  </td></tr>

        <tr><td colspan="4" style="background-color:gray; color:white;">Personal Information (Fill-up required details)</td></tr>
        <tr><td colspan="2">
        Full Name <span class="required">*</span>
        </td>
        <td colspan="2">
        <input type="text" class="form-control compute_form_required_control" name="first_name" id="first_name" placeholder="Enter First Name">
        </td>
        </tr>
        <tr><td colspan="2">
        Contact: <span class="required">*</span>
        </td><td colspan="2">
        <input type="text" class="form-control compute_form_required_control" name="contact" id="contact" placeholder="Enter Contact Number">
        </td></tr>
        <tr><td colspan="2">
        Email: <span class="required">*</span>
        </td><td colspan="2">
        <input type="email" class="form-control compute_form_required_control" name="email" id="email" placeholder="Enter Best Email" required>
        </td></tr>
        <tr><td colspan="2">
        Pick Up Location: <span class="required">*</span>
        </td><td colspan="2">
        <input type="text" class="form-control compute_form_required_control" name="pick_location" id="pick_location" placeholder="Airport/Hotel">
        </td></tr>
        <tr><td colspan="2">
        Pick Up Date: <span class="required">*</span>
        </td><td colspan="2">
        <input type="text" class="form-control compute_form_required_control" name="pick_date" id="pick_date" placeholder="mm/dd/yyyy">
        </td></tr>
        <tr><td colspan="4">
        Additional Information
        <textarea class="form-control noresize" name="other_info" id="other_info"  rows="3" placeholder="Additional Info"></textarea>
        </td></tr>

  <tr><td colspan="2"><span class="impt_text" style="font-style:strong;color:yellow">NO. OF PERSONS</td>
  <td colspan="2">
    <select name="no_of_persons" id="no_of_persons" style="width:100%">
    </select>
  </td>
  </tr>
  <tr class="children_row"><td colspan="2">CHILDREN(3-7 YO)</td>
  <td colspan="2">
    <select name="no_of_children" id="no_of_children" style="width:100%">
    </select>
  </td>
  </tr>
  <tr><td colspan="4" style="padding:14px">
    <p style="text-align:center;padding:0px;margins:0px;border:1px solid orange">ADD-ONS AND DISCOUNTS</p>

    <input type="checkbox" name="add_on_head" id="tops_temple_of_lea" description="Add-on of 400/px (TEMPLE OF LEA/TOPS)" value="400">
    <label for="add_on_head" style="display:inline" >Add-on of 400/px (TEMPLE OF LEA/TOPS) </label><br/>




    <!--
     <input type="checkbox" name="add_on_head" id="tops_temple_of_lea" description="Add-on 1000/px (Canyoneering)" value="1000" >
    <label for="add_on_head" style="display:inline">Add-on 1000/px (Canyoneering)</label><br/>
    <input type="checkbox" name="add_on_total" id="tops_temple_of_lea" description="Add-on 500 (Bamboo Shade)" value="500">
    <label for="add_on_total" style="display:inline">Add-on 500 (Bamboo Shade)</label><br/>
      <input type="checkbox" name="add_on_head" id="tops_temple_of_lea" value="1">
    <label for="tops_temple_of_lea" style="display:inline">add 1000 head</label><br/>
      <input type="checkbox" name="add_on_total" id="tops_temple_of_lea" value="2">
    <label for="tops_temple_of_lea" style="display:inline">add 400 total</label><br/>
      <input type="checkbox" name="add_on_total" id="tops_temple_of_lea" value="3">
    <label for="tops_temple_of_lea" style="display:inline">add 1000 total</label><br/>
      <input type="checkbox" name="discount_head" id="tops_temple_of_lea" value="4">
    <label for="tops_temple_of_lea" style="display:inline">less 400 head</label><br/>
      <input type="checkbox" name="discount_head" id="tops_temple_of_lea" value="5">
    <label for="tops_temple_of_lea" style="display:inline">less 1000 head</label><br/>
      <input type="checkbox" name="discount_total" id="tops_temple_of_lea" value="6">
    <label for="tops_temple_of_lea" style="display:inline">less 400 total</label><br/>
        <input type="checkbox" name="discount_total" id="tops_temple_of_lea" value="7">
    <label for="tops_temple_of_lea" style="display:inline">less 1000 total</label><br/>

    -->
  </td></tr>
  <tr><td colspan="4">
    <?php
    if($discount>0) {
    echo "<div id='promo_box1'>PROMO FOR THIS MONTH";
    echo "<h4 style='margin:0px;padding:0px'>RATE/HEAD IS LESS ";
    echo $discount;
	echo ($discount_percent=='true')?'%':'';
    echo "!</h4>";
    echo "</div>";
    }
    ?>
	  <table style="background:#000000" id="sample_calculate_table">
	  <tr><td colspan="4" style="text-align:center"><span style="font-size:1.1em;color:#fdffc9">SAMPLE RATE CALCULATIONS (PHP)</span></td></tr>
	  <tr><td>&nbsp;</td><td>RATE/HEAD
    <?php
    if($discount>0) {
    echo "<span style='color:yellow'>(";
    echo $discount;
    echo ($discount_percent=='true')?'%':'';
    echo  " OFF)";
    echo  "</span>";
    }
    ?>
	  </td><td>NO.</td><td>TOTAL</td></tr>
	  <tr>
	  <td>ADULT</td>
	  <td><input type="text" class="hilite_value" name="price_head_adult" id="price_head_adult" readonly></td>
	  <td><input type="text" class="hilite_value" name="no_of_adults" id="no_of_adults" readonly></td>
	  <td><input type="text" class="hilite_value" name="total_price_adult" id="total_price_adult" readonly></td>
	  </tr>
	  <tr class="children_row">
	  <td>CHILDREN</td>
	  <td><input type="text" class="hilite_value" name="price_head_children" id="price_head_children" readonly></td>
	  <td><input type="text" class="hilite_value" name="no_of_3to7" id="no_of_3to7" readonly></td>
	  <td><input type="text" class="hilite_value" name="total_price_children" id="total_price_children" readonly></td>
	  </tr>
	  <tr class="children_row">
	  <td colspan="3">SUB TOTAL</td>
	  <td><input type="text" class="hilite_value" name="sub_total" id="sub_total" readonly></td>
	  </tr>
	  <tr>
	  <td colspan="3">Total Add-On</td>
	  <td><input type="text" class="hilite_value" name="total_addon" id="total_addon" readonly></td>
	  </tr>
	  <tr>
	  <td colspan="3">Total Discount</td>
	  <td><input type="text" class="hilite_value" name="total_less" id="total_less" readonly></td>
	  </tr>
	  <tr><td colspan="2">TOTAL PRICE</td><td colspan="2"><input type="text" class="hilite_value" name="computed_total_price" id="computed_total_price" readonly style="font-size:1.2em;color:#ffffff"></td></tr>
	  </table>

  </td></tr>

  <tr><td colspan="4" style="text-align:center">
  <input  type="submit" value="RESERVE ONLY NOW" id="reserve_14"><br/>
  </td></tr>

  </table>
  </form>

<!-- ------------- dialog box ------------------- -->


      <div id="dialog-4" title="BOOK YOUR TOUR NOW!">
        <p style="font-size:.8em;line-height:1em">
        FOR YOUR BOOKING WE OFFER A SPECIAL RATE. PLEASE COMPLETE THE FORM BELOW. THANKS.
        </p>
      <form name="form14" id="form14">
      <table>
      <tr><td style="padding:0px">
      FULL NAME
      </td></tr>
      <tr><td>
      <input type="text" id="dialog_name" name="dialog_name" class="dialog_max" required style="padding:0px;margin:0px">
      </td></tr>
      <tr><td>
      EMAIL ADDRESS
      </td></tr>
      <tr><td>
      <input type="text" id="dialog_email" name="dialog_email" class="dialog_max" required style="padding:0px;margin:0px">
      </td></tr>
      <tr><td>
      TOUR</td></tr>
      <tr><td>
      <input type="text" id="dialog_package_tour" name="dialog_package_tour" style="width:100%" class="dialog_max" required>
      </td></tr>
      <tr><td>
      NO. OF PERSONS
      </td></tr>
      <tr><td>
      <input type="text" id="dialog_no_of_persons" name="dialog_no_of_persons" class="dialog_max" required>
      </td></tr>
      <tr><td>
      PICK-UP DATE
      </td></tr>
      <tr><td>
      <input type="text" id="dialog_pick_up_date" name="dialog_pick_up_date" class="dialog_max" required>
      </td></tr>
      </table>
      </form>

      <div id="dialog_status" style="font-size:.8em;color:#001085;line-height:1em"></div>
      </div>



<!-- ####################################### ends here #################################### -->

</div> <!-- col3 -->
</div> <!-- container -->

  <script type="text/javascript">
  setInterval('swapImages("tour_Gallery")', 5000);
  var package_tour_name = "CEBU CITY DAY TOUR";

  var pkprice_per_head = [2900,1600,1200,1050,900,800,750,700,650,600,550,500,500];
  var pk_person_variance = [];
  pk_person_variance[0] = [1,1];
  pk_person_variance[1] = [2,2];
  pk_person_variance[2] = [3,3];
  pk_person_variance[3] = [4,4];
  pk_person_variance[4] = [5,5];
  pk_person_variance[5] = [6,6];
  pk_person_variance[6] = [7,7];
  pk_person_variance[7] = [8,8];
  pk_person_variance[8] = [9,9];
  pk_person_variance[9] = [10,10];
  pk_person_variance[10] = [11,11];
  pk_person_variance[11] = [12,12];
  pk_person_variance[12] = [13,13];

  var max_persons = 0;
  var number_of_persons = 0;
  var number_of_children = 0;
  var add_on_per_head_total = 0;
  var add_on_for_total =0;
  var discount_on_per_head_total =0;
  var discount_on_total =0;
  var d_price_head =0;

  //initialize discount for children if any
  var discount = 300;
  var percent = false;

  //initialize PACKAGE PROMO if any
  var promo_discount = <?php echo $discount ?>;
  var promo_percent = <?php echo $discount_percent ?>;

  $(document).ready( function(){

    //initialize tour name
    $('#tour_name').val(package_tour_name);

    //initialize children discount
    $('#children_discount').val(discount);

    //initialize
    for(i=1;i<=99;i++) {
      $('#no_of_persons').append('<option val="'+ i + '">' + i + '</option>');
    }
    for(i=0;i<=0;i++) {
      $('#no_of_children').append('<option val="'+ i + '">' + i + '</option>');
    }
    if(discount == 0) {
      $('.children_row').hide();
    }

    //initialize other details on load
    set_other_payment_details();

    number_of_persons = $('select[name=no_of_persons]').val();
    number_of_children = $('select[name=no_of_children]').val();

    max_persons = pk_person_variance[pk_person_variance.length -1][1];

    d_price_head = get_price_index(pkprice_per_head,pk_person_variance, number_of_persons, max_persons);
    //alert( "The price per head : " + d_price_head );

    add_on_per_head_total = compute_addon_discount_total('add_on_head');
    add_on_total = compute_addon_discount_total('add_on_total');
    discount_on_per_head_total = compute_addon_discount_total('discount_head');
    discount_total = compute_addon_discount_total('discount_total');
    compute_total(d_price_head,add_on_per_head_total,discount_on_per_head_total,number_of_persons, number_of_children,percent,discount,add_on_total,discount_total);

	monitor_control_changes();
	submit_package_forms();

});

</script>