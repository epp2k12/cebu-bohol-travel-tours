<?php
/*
Template Name: cebu-home page
*/
get_header();
?>
<div id="packgroup" class="col-sm-8 row">

<div class="mid_blue_title_bars" >CEBU TOURS &amp; TRAVELS</div>
	<?php
		$args=array(
						'cat' => 16,
						'order' => ASC
					);
	$packagePosts = new WP_Query( $args );
	if( $packagePosts->have_posts() ) {
	//loop through related posts based on the tag
		while ( $packagePosts->have_posts() ) :
			$packagePosts->the_post();
	?>

		<div class="dpackposts col-sm-4 col-xs-6">
			<?php
					$discount = get_post_meta( get_the_ID(), 'discount', true );
					$discount_percent = get_post_meta( get_the_ID(), 'is_discount_percent', true );
					if($discount > 0) {
						if($discount_percent == "true") {
							$promo_text = "<strong style='color:yellow;'>LESS ". $discount . "% </strong>";
						}else{
							$promo_text = "<strong style='color:yellow;'>LESS - P" . $discount . "</strong>";
						}
					} else {
						$promo_text = "";
					}
			if ($discount > 0) {
			?>
				<div class="small_promo">
				<span class="label" style="background-color:red;font-size:1.1em;"><?php echo $promo_text; ?></span>
				<!--
				<img src="<?php bloginfo('stylesheet_directory')?>/img/small_captions/small_featured.jpg" alt="featured promo">
				-->
				</div>

			<?php }	?>
			<div class="tbnail">
			<a href="<?php the_permalink(); ?>">
			<?php the_post_thumbnail(); ?>
			</a>
			</div>
			<div class="dtitle">
			<a href="<?php the_permalink(); ?>">
			<?php the_title(); ?>
			<!-- <button type="button" class="btn btn-primary" style="float:right">Warning</button> -->
			</a>
			</div>
		<a href="<?php the_permalink(); ?>" class="btn btn-info" role="button" style="width:100%">BOOK NOW</a>

		</div>

	<?php endwhile; ?>
	<?php wp_reset_postdata(); ?>
	<?php } ?>


</div> <!-- packgroup -->



<div id="packside" class="col-sm-4">

<?php
include 'common/side-bar-pages.php';
?>

  <!--
  <div id="myContent" style="margin:0 auto">
  <ul>
    <li class="active"><h1>HTML 01</h1></li>
        <li><h1>HTML 02</h1></li>
            <li><h1>HTML 03</h1></li>
                <li><h1>HTML 04</h1></li>
                    <li><h1>HTML 05</h1></li>
  </ul>
  </div>
  -->
</div>

<div style="clear:both"></div>

<div class="packinclusions">
<h1 class="mid_blue_title_bars">Why you should choose us!</h1>

	<div class="dbox">
	<img src="<?php bloginfo('stylesheet_directory')?>/img/cebu_home/home-part01.jpg">

		<div class="white_box">
		<!-- <img src="<?php bloginfo('stylesheet_directory')?>/img/cebu_home/home-icon01.png"> -->
		<br/>
		<p>
		<span class="white_box_title"><strong>SAFE &amp; RELAXING RIDES.</strong></span><br/>
		We have a line of well maintained &amp; fully air-conditioned Van &amp; Car service.
		</p>
		<div class="dbutton">
		<a href="http://earentacarcebu.com/fleet/" class="btn btn-info" role="button">View Fleets</a>
		</div>
		</div>

	</div>
	<div class="dbox">
	<img src="<?php bloginfo('stylesheet_directory')?>/img/cebu_home/home-part02.jpg">
		<div class="white_box">

		<!-- <img src="<?php bloginfo('stylesheet_directory')?>/img/cebu_home/home-icon02.png"> -->
		<br/>
		<p>
		<span class="white_box_title"><strong>RESERVATION ANYTIME</strong></span><br/>
		Call, text or email us for your inquiries and expect a quick response from us.
		</p>
		<div class="dbutton">
		<a href="http://cebuboholtraveltours.com/contact-us/" class="btn btn-info" role="button">Contact Us Now!</a>
		</div>
		</div>

	</div>
	<div class="dbox">
		<img src="<?php bloginfo('stylesheet_directory')?>/img/cebu_home/home-part04.jpg">
		<div class="white_box">
		<!-- <img src="<?php bloginfo('stylesheet_directory')?>/img/cebu_home/home-icon03.png"> -->
		<br/>
		<p>
		<span  class="white_box_title"><strong>LOWEST RATES EVER</strong></span><br/>
		Enjoy safe, exciting and adventurous tours at the lowest cost ever.
		</p>
		<div class="dbutton">
		<a href="http://cebuboholtraveltours.com/blogs/" class="btn btn-info" role="button">View Rates</a>
		</div>
		</div>

	</div>
</div>



<script type="text/javascript">
$(document).ready(function(){

	setInterval('swapImages("myGallery")', 5000);
	set_white_box();

});

</script>

<?php // spacious_sidebar_select(); ?>
<?php get_footer(); ?>
