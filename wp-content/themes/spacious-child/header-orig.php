<?php
/**
 * Theme Header Section for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="main" class="clearfix"> <div class="inner-wrap">
 *
 * @package ThemeGrill
 * @subpackage Spacious
 * @since Spacious 1.0
 */
?>
<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<?php
/**
 * This hook is important for wordpress plugins and other many things
 */
wp_head();
?>
</head>

<body <?php body_class(); ?>>
<?php	do_action( 'before' ); ?>
<div id="page" class="hfeed site">
	<?php do_action( 'spacious_before_header' ); ?>
	<header id="masthead" class="site-header clearfix">



<?php if(is_front_page()): ?>
<div id="header-logo-image">
	<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><img src="<?php echo of_get_option( 'spacious_header_logo_image', '' ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>"></a>
</div><!-- #header-logo-image -->							



<style type="text/css">

	#myCarousel {
		margin:0px;
		padding:0px;
	}

@media only screen and (min-width: 768px) {

	.carousel .item {
		max-height:350px;
		height:350px;
	}

	.carousel-caption {
		display:none;
	}
}
@media only screen and (max-width: 768px) {

	#myCarousel {
		display:none;
	}

	#header-logo-image {
	  position:static;
	}

}

</style>

<div id="myCarousel" class="carousel slide" data-interval="3000" data-ride="carousel"><!-- class of slide for animation -->
<ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
            <li data-target="#myCarousel" data-slide-to="2"></li>
            <li data-target="#myCarousel" data-slide-to="3"></li>
            <li data-target="#myCarousel" data-slide-to="4"></li>

        </ol>   
 <div class="carousel-inner" role="listbox">
    <div class="item active"><!-- class of active since it's the first item -->
      <img src="http://localhost/alvin_wp01/wp-content/uploads/2015/10/001.jpg" alt="" />
      <div class="carousel-caption">
      	<p><button type="button" class="btn btn-success" style="margin-right:10px">Warning</button>Caption text here</p>
      </div>
    </div>
    <div class="item">
      <img src="http://localhost/alvin_wp01/wp-content/uploads/2015/10/slider-10-1600x6001.jpg" alt="" />
      <div class="carousel-caption">
        <p>Caption text here</p>
      </div>
    </div>
    <div class="item">
      <img src="http://localhost/alvin_wp01/wp-content/uploads/2015/10/slider-8-1600x600.jpg" alt="" />
      <div class="carousel-caption">
        <p>Caption text here</p>
      </div>
    </div>
    <div class="item">
      <img src="http://localhost/alvin_wp01/wp-content/uploads/2015/10/slider-3-1600x600.jpg" alt="" />
      <div class="carousel-caption">
        <p>Caption text here</p>
      </div>
    </div>
    <div class="item">
      <img src="http://localhost/alvin_wp01/wp-content/uploads/2015/10/slider-1-1600x600.jpg" alt="" />
      <div class="carousel-caption">
        <p>Caption text here</p>
      </div>
    </div>

   </div><!-- /.carousel-inner -->
  <!--  Next and Previous controls below
        href values must reference the id for this carousel -->

  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
    <img src="http://localhost/alvin_wp01/wp-content/uploads/2015/10/success-previous-button-small.png">
  </a>
  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
  <img src="http://localhost/alvin_wp01/wp-content/uploads/2015/10/success-next-button-small.png">
  </a>

</div><!-- /.carousel -->
					
						<nav id="site-navigation" class="main-navigation" role="navigation">
							<h3 class="menu-toggle"><?php _e( 'Menu', 'spacious' ); ?></h3>
							<?php
								if ( has_nav_menu( 'primary' ) ) {
									wp_nav_menu( array( 'theme_location' => 'primary' ) );
								}
								else {
									wp_page_menu();
								}
							?>
						</nav>

<?php else:	?>
<div id="header-logo-image">
	<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home"><img src="<?php echo of_get_option( 'spacious_header_logo_image', '' ); ?>" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" style="width:150px;"></a>
</div><!-- #header-logo-image -->




						<nav id="site-navigation" class="main-navigation" role="navigation">
						<h3 class="menu-toggle"><?php _e( 'Menu', 'spacious' ); ?></h3>
						<?php
							if ( has_nav_menu( 'primary' ) ) {
								wp_nav_menu( array( 'theme_location' => 'primary' ) );
							}
							else {
								wp_page_menu();
							}
						?>
						</nav>				

<?php endif; ?>

<script type="text/javascript">
	$(document).ready(function() {


		// alert("im inside the maze of life!");
		//var ditem = $(this).children('img').attr('src');
		//alert("the source is :" + ditem);
		$('.item').hover(function(){
			$(this).children('.carousel-caption').fadeIn(500);
		},function(){
			$(this).children('.carousel-caption').fadeOut(500);
		});
			

		$('.carousel').carousel({
      		interval: 3000
    	});



	});
</script>

</header>



	<?php do_action( 'spacious_after_header' ); ?>
	<?php do_action( 'spacious_before_main' ); ?>
	<div id="main" class="clearfix">
		<div class="inner-wrap">