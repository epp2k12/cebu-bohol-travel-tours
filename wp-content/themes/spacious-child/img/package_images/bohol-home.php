<?php
/*
Template Name: bohol-home page
*/
get_header();
?>


<style type="text/css">
* {
    box-sizing: border-box;
}
@media only screen and (min-width: 768px) {

	#packgroup {
	 float:left;
	 width:73%;
	}
	#packside {
	 float:left;
	 width:27%;
	}
	.dpackpost {
		max-width:215px;
		min-height:480px;
		margin:10px;
		float:left;
	}
	.dpackpost .dtitle {
		text-align: center;
		min-height: 60px;
	}
	.dpackpost img {
		border:1px solid gray;
		max-width:208px;
	}
}
@media only screen and (max-width: 768px) {

	#packgroup {
	 width:100%;
	}
	#packside {
	 width:100%;
	}
	.dpackpost {
	 width:100%;
	 height:100%;
	 margin:3px
	}
	.dpackpost img {
		width:100%;
		height:100%;
		margin:3px;
	}
}

</style>




<div id="packgroup">

<h1 class="mid_blue_title_bars" >Cebu Tours &amp; Travels</h1>
<?php
$args=array(
'cat' => 4,
'order' => ASC
);
$packagePosts = new WP_Query( $args );

	if( $packagePosts->have_posts() ) {
	//loop through related posts based on the tag
		while ( $packagePosts->have_posts() ) :
			$packagePosts->the_post();
?>

		<div class="dpackpost">
			<a href="<?php the_permalink(); ?>">

			<?php
			$categories = get_the_category();
			$cat_array = array();

			foreach ( $categories as $category ) {
			    $cat_array[] = $category->name;
			}
			if (in_array("promo", $cat_array)) {
			?>
				<div class="small_promo">
				<img src="<?php bloginfo('stylesheet_directory')?>/img/small_captions/small_featured.jpg" alt="featured promo">
				</div>

			<?php }	?>

			<div class="dtitle">


			<?php the_title(); ?>
		<!-- <button type="button" class="btn btn-primary" style="float:right">Warning</button> -->
			</div>

			<div class="tbnail">
			<?php the_post_thumbnail(); ?>
			</div>
			</a>


			<?php the_excerpt(); ?>


		</div>

		<?php endwhile; ?>
		<?php wp_reset_postdata(); ?>

	<?php } ?>

</div> <!-- packgroup -->

<div id="packside">

<div class="contact_numbers">
<table class="table table-condensed">
<tr><th colspan="2">Contact Us</th></tr>
<tr><td colpsan="2"><h3>Need help booking?</h3></td></tr>
<tr><td colpsan="2"><h3>Call Us at <img style="margin:0;padding:0;" src="<?php bloginfo('stylesheet_directory')?>/img/contact_icons/mobile01.png"></h3></td></tr>
<tr><td colspan=2>
+63 917 587 6314</td></tr>
<tr><td colspan=2>
+63 923 2344 818</td></tr>
<tr><td colspan=2>
+63 928 3626 036</td></tr>
<tr><td colspan=2>
<p>E-mail us at : support@cebuboholtraveltours.com</p></td></tr>
<tr><td colspan="2" style:"text-align:center;">
			<div class="contact_us_button">
			<button type="button" onclick="window.location='http://cebuboholtraveltours.com/?p=19'">Contact Us!</button>
			</div>
  </td>
  </tr>
</table>
</div>

  <div id="rent_a_car" style="overflow:auto;">
  <table class="table table-condensed" style="font-size:.8em">
  <tr><th colspan="2" style="color:#ffffff;background-color:#000">FOR CAR RENTAL - <span style="color:#ff0000">BEST DEALS</span></th></tr>
  <tr><td>
  <div style="text-align:center">
	<a href="http://earentacarcebu.com/">
	<img src="<?php bloginfo('stylesheet_directory')?>/img/home_images/logo7.png">
	</a>
	</div>
  <div id="myGallery" style="margin:0 auto">
    <img src="<?php bloginfo('stylesheet_directory')?>/img/cars/car01.jpg" class="active" />
    <img src="<?php bloginfo('stylesheet_directory')?>/img/cars/car02.jpg" />
    <img src="<?php bloginfo('stylesheet_directory')?>/img/cars/car03.jpg" />
    <img src="<?php bloginfo('stylesheet_directory')?>/img/cars/car04.jpg" />
  </div>
  </td></tr>
  <tr><td style="font-size:.9em;line-height:1.2;padding:10px;text-align:center">
    CEBU's MOST AFFORDABLE VAN/CAR RENTAL
  All Van and Car Rentals start at Php 1,200 for the first 3 hours.
  </td></tr>
  <tr><td style="text-align:center;"><a href="http://earentacarcebu.com/" class="btn btn-info" role="button">BOOK NOW</td></tr>
  </table>


  </div>





  <!--
  <div id="myContent" style="margin:0 auto">
  <ul>
    <li class="active"><h1>HTML 01</h1></li>
        <li><h1>HTML 02</h1></li>
            <li><h1>HTML 03</h1></li>
                <li><h1>HTML 04</h1></li>
                    <li><h1>HTML 05</h1></li>
  </ul>
  </div>
  -->

</div>

<div style="clear:both"></div>

<div class="packinclusions">
<h1 class="mid_blue_title_bars">Why you should choose us!</h1>

	<div class="dbox">
	<img src="<?php bloginfo('stylesheet_directory')?>/img/cebu_home/home-part01.jpg">

		<div class="white_box">
		<img src="<?php bloginfo('stylesheet_directory')?>/img/cebu_home/home-icon01.png">
		<p>
		<span class="white_box_title"><strong>SAFE &amp; RELAXING RIDES.</strong></span><br/>
		We have a line of well maintained &amp; fully air-conditioned Van &amp; Car service.
		</p>
		<div class="dbutton">
		<a href="http://earentacarcebu.com/fleet/" class="btn btn-info" role="button">View Fleets</a>
		</div>
		</div>

	</div>
	<div class="dbox">
	<img src="<?php bloginfo('stylesheet_directory')?>/img/cebu_home/home-part02.jpg">
		<div class="white_box">

		<img src="<?php bloginfo('stylesheet_directory')?>/img/cebu_home/home-icon02.png">
		<p>
		<span class="white_box_title"><strong>RESERVATION ANYTIME</strong></span><br/>
		Call, text or email us for your inquiries and expect a quick response from us.
		</p>
		<div class="dbutton">
		<a href="http://localhost/alvin_wp01/?p=19" class="btn btn-info" role="button">Contact Us Now!</a>
		</div>
		</div>

	</div>
	<div class="dbox">
		<img src="<?php bloginfo('stylesheet_directory')?>/img/cebu_home/home-part04.jpg">
		<div class="white_box">
		<img src="<?php bloginfo('stylesheet_directory')?>/img/cebu_home/home-icon03.png">
		<p>
		<span  class="white_box_title"><strong>LOWEST RATES EVER</strong></span><br/>
		Enjoy safe, exciting and adventurous tours at the lowest cost ever.
		</p>
		<div class="dbutton">
		<a href="http://localhost/alvin_wp01/?p=114" class="btn btn-info" role="button">View Rates</a>
		</div>
		</div>

	</div>
</div>

<script type="text/javascript">
$(document).ready(function(){

	setInterval('swapImages("myGallery")', 5000);

	$('.white_box').hover(function(){
		$(this).css(
		{
			'background-color': 'rgba(0, 0, 255, 0.7)'
		}

		);
		$(this).find('p').css (
		{
			'color':'#ffffff'
		}
		);
	},function(){
		$(this).css(
		{
			'background-color': 'rgba(255, 255, 255, 0.3)'
		}

		);
		$(this).find('p').css (
		{
			'color':'#000000'
		}
		);
	});

});

</script>

<?php // spacious_sidebar_select(); ?>
<?php get_footer(); ?>
