<?php
/**
 * @package Cebu Bohol Travel Tours Company
 * @subpackage CBTT
 * @since CBTT 1.0
 */
?>
<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="google-site-verification" content="Rs2kcKk7K_Hspjx_ldGhwE86boDRDgx6xryJPcA2jXw" />
<meta name="google-site-verification" content="YMPEAyzc_GJCu4fjfIEG84JUA6VsOtV_2lki406LWxk" />
<meta name="msvalidate.01" content="94E032D67074B89854BC8FE811FAB4F7" />
<meta name="p:domain_verify" content="376708b384fe4097ba1890e2d4441c53"/>
<meta name="description" content="We offer affordable Cebu and Bohol tours (Day tours and Overnight tours)-Whale shark cebu tour, canyoneering cebu, sumilon Island sandbar tour, mactan island hopping day tour, malapascua island, kalanggaman island, pescador island hopping, Cebu City tour, Bohol countryside tour and car van rentals." />
<meta name="keywords" content="cebu tours, whale shark oslob, canyoneering kawasan, oslob whale shark, canyoneering cebu, cebu city  tours, whale shark cebu, oslob whale shark tours, whale shark tours, cebu travel, cheap oslob tour package, affordable tour of cebu, whale shark cebu tours, affordable cebu tours, oslob shark tours">
<meta property="og:image" content="https://cebuboholtraveltours.com/wp-content/uploads/2018/06/alvinlogo03.png"/>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Playfair+Display' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Roboto+Condensed' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Quicksand' rel='stylesheet' type='text/css'>
<link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
<?php
/**
 * This hook is important for wordpress plugins and other many things
 */
wp_head();
?>
<!-- Bootstrap Date-Picker Plugin -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/js/bootstrap-datepicker.min.js"></script>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.4.1/css/bootstrap-datepicker3.css"/>
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
<script>
  (adsbygoogle = window.adsbygoogle || []).push({
    google_ad_client: "ca-pub-6097410298968540",
    enable_page_level_ads: true
  });
</script>
</head>

<body <?php body_class(); ?>>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-76084323-2', 'auto');
  ga('send', 'pageview');

</script>

<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/56ea4aa073d8348e247e30ec/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->

<?php	do_action( 'before' ); ?>
<div id="page" class="hfeed site">
	<?php do_action( 'spacious_before_header' ); ?>
	<header id="masthead" class="site-header clearfix">

<div id="header_logo_container">
<a href="<?php echo home_url(); ?>">
<img src="<?php bloginfo('stylesheet_directory')?>/img/logo/alvinlogo05.png" alt="cebu bohol tours"/ class="site_logo_class">
</a>
</div>
<?php if(is_front_page()): ?>



						<nav id="site-navigation" class="main-navigation" role="navigation">
							<h3 class="menu-toggle"><?php _e( 'Menu', 'spacious' ); ?></h3>
							<?php
								if ( has_nav_menu( 'primary' ) ) {
									wp_nav_menu( array( 'theme_location' => 'primary' ) );
								}
								else {
									wp_page_menu();
								}
							?>
						</nav>

<?php else:	?>

						<nav id="site-navigation" class="main-navigation" role="navigation">
						<h3 class="menu-toggle"><?php _e( 'Menu', 'spacious' ); ?></h3>
						<?php
							if ( has_nav_menu( 'primary' ) ) {
								wp_nav_menu( array( 'theme_location' => 'primary' ) );
							}
							else {
								wp_page_menu();
							}
						?>
						</nav>

<?php endif; ?>
</header>



	<?php do_action( 'spacious_after_header' ); ?>
	<?php do_action( 'spacious_before_main' ); ?>
	<div id="main" class="clearfix">
		<div class="inner-wrap">