function monitor_control_changes() {

    //monitor changes
    $('#no_of_persons').change(function(){

      //set select controls
      number_of_persons = $('select[name=no_of_persons]').val();
      $('#no_of_children').empty();
      for(i=0;i<number_of_persons;i++) {
        $('#no_of_children').append('<option val="'+ i + '">' + i + '</option>');
      }
      number_of_children = 0;
      d_price_head = get_price_index(pkprice_per_head,pk_person_variance, number_of_persons, max_persons);
      compute_total(d_price_head,add_on_per_head_total,discount_on_per_head_total,number_of_persons, number_of_children,percent,discount,add_on_total,discount_total);

      if(number_of_persons > max_persons) {
        $( "#dialog-4" ).dialog( "open" );
      }
    });

    //monitor changes
    $('#no_of_children').change(function() {
      number_of_children = $('select[name=no_of_children]').val();
      compute_total(d_price_head,add_on_per_head_total,discount_on_per_head_total,number_of_persons, number_of_children,percent,discount,add_on_total,discount_total);
    });

    var other_details_text="";
    $( "[type=checkbox]" ).change( function(){
      //alert("hello the world!");
      add_on_per_head_total = compute_addon_discount_total('add_on_head');
      add_on_total = compute_addon_discount_total('add_on_total');
      discount_on_per_head_total = compute_addon_discount_total('discount_head');
      discount_total = compute_addon_discount_total('discount_total');
      compute_total(d_price_head,add_on_per_head_total,discount_on_per_head_total,number_of_persons, number_of_children,percent,discount,add_on_total,discount_total);
      other_details_text +=

      // alert("the value is : " + $(this).attr('value'));
      set_other_payment_details();

    });

}


function submit_package_forms() {

  // ------------------------------------------ dialog box --------------------------------------
              $( "#dialog-4" ).dialog({
                  autoOpen: false,
                  modal: true,
                  hide: "fade",
                  show :"fade",
                  width:350,
                  buttons: {
                    "BOOK NOW": function() {
                      var dvalidate = validate_form('dialog_max');
                      //alert("the great value is : "  + dvalidate);
                      if(dvalidate) {
                        //alert("hello again!");
                        var pathname ='/alvin_wp01/js_ajax/book_exceed_max.php';
                        var sdata= $('#form14').serializeArray();
                        //alert("inside the great labyrinth!");
                        $.ajax({
                          "type":"POST",
                          "url":pathname,
                          "data":sdata,
                          //"data":"var1=Avocado&var2=Banana&var3=Orange",
                          "success":function(data) {
                          // alert("the data : " + data);
                          // alert("We have received your reservation. We will keep in touch with you the soonest! Thanks.")
                          $('#dialog_status').html("We have received your reservation. We will keep in touch with you the soonest! Thanks.");
                          }
                        });
                      }else{
                        // alert("something is missing!");
                        $('#dialog_status').html("<span style='color:#ff0000'>You need to complete the form to process your booking! Thanks.</span>");
                      }
                    }
                },
                 open: function(event, ui) {

                    $('#dialog_status').html("");
                    //alert("the number of persons : " + number_of_persons);
                    $('#dialog_name').val($('#first_name').val());
                    $('#dialog_no_of_persons').val(number_of_persons);
                    $('#dialog_package_tour').val($('#tour_name').val());
                    $('#dialog_pick_up_date').val($('#pick_date').val());
                    $('#dialog_email').val($('#email').val());
                 },
                 close: function(event, ui) {
                    $('#no_of_persons').val(pkprice_per_head[0]);
                    // ---------------------------------------- set select controls
                    number_of_persons=pk_person_variance[0][0];
                    $('#no_of_children').empty();
              for(i=0;i<number_of_persons;i++) {
                $('#no_of_children').append('<option val="'+ i + '">' + i + '</option>');
              }
            number_of_children = 0;
                    d_price_head = get_price_index(pkprice_per_head,pk_person_variance, number_of_persons, max_persons);
                    compute_total(d_price_head,add_on_per_head_total,discount_on_per_head_total,number_of_persons, number_of_children,percent,discount,add_on_total,discount_total);

                 }
              });

      $('#form14').submit( function(event) {
        // alert("inside the great madness!");
        // --------------------------------------------
            var pathname ='/alvin_wp01/js_ajax/test_ajax.php';
            var sdata= $(this).serializeArray();
            alert("inside the great labyrinth!");
            $.ajax({
              "type":"POST",
              "url":pathname,
              "data":sdata,
              //"data":"var1=Avocado&var2=Banana&var3=Orange",
              "success":function(data) {
              alert("the data : " + data);
              alert("We have received your reservation. We will keep in touch with you the soonest! Thanks.")
              }
            });

        // --------------------------------------------
        event.preventDefault();
      });


      $('#form_compute').submit(function(event){

        var dvalidate = validate_form('compute_form_required_control');
        // alert("the validate is : " + dvalidate);
        if(dvalidate) {

          // var btn = $(this).find("input[type=submit]:focus" );
          // button_type = btn.val();
          // alert("the button type is : " + button_type);

          var sdata= $('#form_compute').serializeArray();

          // alert("inside the maze of submit :" + sdata);
          // var pathname = document.location.origin + '/js_ajax/ajax-handler-payment.php';
          var pathname ='/js_ajax/ajax-handler-payment.php';
          // alert("inside the great labyrinth! " + document.location.origin+'/js_ajax/test_for_ajax.php');
          $.ajax({
            "type":"POST",
            "url":pathname,
            "data":sdata,
            //"data":"var1=Avocado&var2=Banana&var3=Orange",
            "success":function(data) {
            alert("Your booking request was sent successfully!, Thanks. ");     
	    document.getElementById("form_compute").reset();
            $("#first_name").focus();
            $("#tour_name").val(data);
            $('#form_compute_status').html("We have received your reservation. We will keep in touch with you the soonest! Thanks.");
            }
          });
        }else{
          $('#form_compute_status').html("Please fill out the required fields. Thanks.");
        }
      event.preventDefault();
      });

}



function validate_form(d_form_name) {

// this function will validate a form input by class name

  var d_return_value;
  var d_form_name = d_form_name;
  $("."+d_form_name).each(function() {
    var dvalue =  $(this).val();
    if(dvalue.trim()=="") {
      d_return_value = false;
      $(this).css({"background-color": "#fdffc1"}).focus();
      return false;
    }else{
      $(this).css({"background-color": "#ffffff"}).focus();
    }
    d_return_value = true;
  });
  return d_return_value;
}

function set_other_payment_details() {

  var other_payment_details="";
  $("form input[type='checkbox']").each(function() {

    if($(this).prop('checked')) {
      var description = $(this).attr('description');
      other_payment_details += "<li>";
      other_payment_details += description;
      other_payment_details += "</li>";
    }
  });

  //This will store the discount value and if it is in percent or not inside
  // a form control so that it can be included in the email information

  if(promo_discount > 0) {
      var percent_text;
      if(promo_percent) {
        percent_text = "percent";
      }else{
        percent_text = "";
      }
      var promo_text = "Promo Discount = " + promo_discount + " " + percent_text;
      other_payment_details += "<li>";
      other_payment_details += promo_text;
      other_payment_details += "</li>";
  }

  $('#other_payment_details').val(other_payment_details);
}

function get_selected_value(select_name) {
    var d_selected_value;
    $('#'+select_name).change(function(){
          // var selected_number_of_persons = $('#package_name option:selected')
      // alert('the selected value : ' + $('select[name='+select_name+']').val());
      d_selected_value = $('select[name='+select_name+']').val();
      //alert('the selected value : ' + d_selected_value);
      return d_selected_value;
    });
}

function compute_total(d_price_head,add_on_per_head_total,discount_on_per_head_total,number_of_persons,number_of_children,percent,discount,add_on_total,discount_total) {


  //alert("the computed add_on_per_head_total : " + add_on_per_head_total);
  var partial_price_head_wo_promo = (d_price_head + add_on_per_head_total) - discount_on_per_head_total;
    //compute for promo discount from patial price per head
  if(promo_percent) {
    promo_discount_head =  partial_price_head_wo_promo * (promo_discount/100);
  }else {
    promo_discount_head = promo_discount;
  }
  actual_price_head_per_adult = partial_price_head_wo_promo - promo_discount_head;
  // alert("actual price per head adult : " + actual_price_head_per_adult);

  
  actual_number_of_adults = number_of_persons - number_of_children;
  total_price_adult = actual_price_head_per_adult * actual_number_of_adults;

  //compute for the children rate
  if(percent) {
    child_discount =  actual_price_head_per_adult * (discount/100);
  }else {
    child_discount = discount;
  }
  child_rate_head = actual_price_head_per_adult - child_discount;
  total_price_child = child_rate_head * number_of_children;

  sub_total_price = total_price_adult + total_price_child;
  computed_total_price = ( sub_total_price + add_on_total ) - discount_total;


  //display
  $('#price_head_adult').val(actual_price_head_per_adult);
  $('#no_of_adults').val(actual_number_of_adults);
  $('#total_price_adult').val(total_price_adult);

  $('#price_head_children').val(child_rate_head);
  $('#no_of_3to7').val(number_of_children);
  $('#total_price_children').val(total_price_child);

  $('#sub_total').val(sub_total_price);

  $('#total_addon').val(add_on_total);
  $('#total_less').val(discount_total);
  $('#computed_total_price').val('PHP '+computed_total_price);

}


function compute_addon_discount_total(control_name) {
    $add_on_total = $('input[name='+control_name+']:checked');
    $total = 0;
    $add_on_total.each(function(){
      var dvalue = parseInt(this.value);
      $total += dvalue;
    });
    // alert('the total '+control_name+' is :' + $total);
    return $total;
}

function get_price_index(price_head,person_variance,num_persons,max_persons) {

  var p_head_length = price_head.length;
  // alert( "the maximum number of persons : " + max_persons );
  // alert( "the number of persons is : " + num_persons);

  if( num_persons > max_persons ) {
    return 0;
  } else {
    for(i=0;i<p_head_length;i++) {
        //alert( "the number is :" + i);
        if(num_persons >= person_variance[i][0] && num_persons <= person_variance[i][person_variance[i].length -1]) {
          // alert( "the index is : " + price_head[i] );
          return price_head[i];
        }
    }
  }
}

//---------------------------------- for swapping images ------------------------------
	function swapImages(img_group){
        var dactive = '#' + img_group + ' .active';
        var d_img_first = '#' + img_group + ' img:first';
        // alert( dactive);
        var $active = $(dactive);
        var $next = ($(dactive).next().length > 0) ? $(dactive).next() : $(d_img_first);
        $active.fadeOut(function(){
        $active.removeClass('active');
        $next.fadeIn().addClass('active');
        });
      }

//-------------------------------- set white box ---------------------------------------
function set_white_box() {
    $('.white_box').hover(function(){
    $(this).css(
    {
      'background-color': 'rgba(0, 0, 0, 0.9)'
    }

    );
    $(this).find('p').css (
    {
      'color':'#ffffff'
    }
    );
  },function(){
    $(this).css(
    {
      'background-color': 'rgba(255, 255, 255, 0.3)'
    }

    );
    $(this).find('p').css (
    {
      'color':'#000000'
    }
    );
  });
}
