<?php
get_header();
?>


<style type="text/css"> 
* {
    box-sizing: border-box;
}

.pack_inclusions {
  border:1px solid black;
}

@media only screen and (max-width: 768px ) {
   .col7 {
   width:100%;
   }

   .col3 {
  width:100%;
   }

   #header-logo-image {
   position:static;
   }

  #myGallery{

      position:relative;
      width:300px;
      height:170px;
      padding:10px;
  }



}

@media only screen and (min-width: 768px) {
   .col7 {
   width:60%;  
   float:left;
   padding:10px;
   }
   .col3 {
   width:40%;
   float:left;
   padding:10px;
   }

    #myGallery{

        position:relative;
        width:520px;
        height:348px;
        padding:10px;
        
    }


}


/* standard form styling */
#form_compute {
border:1px solid #d7d7d7;
padding:20px;
background-color:#008aa4;
color:#ffffff  
}

#form_compute input[type=text], #form_compute input[type=email] {
height:30px;
margin:0px;
}

input[type=text], input[type=email], select {
height:30px;
margin:0px;
}

input[type=submit] {
  margin:0px;
}

#form_compute .hilite_value {
border:4px solid #990000;
}

#form_compute input[type="submit"] {
margin:0px;
}

#form_compute textarea {
padding:0;
margin:0;
resize:none;
}

#table_compute {
font-size:.9em;  
}

</style>



<div class="pack_container">
<div class="col7">

  <div id="tour_page_images" style="overflow:hidden;">
  <table class="table table-condensed">
  <tr><th colspan="2" style="color:#ffffff;background-color:#008aa4">Oslob Whale Watching &amp; Tumalog Falls Adventure</th></tr>  
  <tr><td>
  <div id="myGallery" style="margin:0 auto">
    <img src="<?php bloginfo('stylesheet_directory')?>/img/oslob_tumalog/oslob_tumalog01.jpg" class="active" />
    <img src="<?php bloginfo('stylesheet_directory')?>/img/oslob_tumalog/oslob_tumalog02.jpg" />
    <img src="<?php bloginfo('stylesheet_directory')?>/img/oslob_tumalog/oslob_tumalog03.jpg" />
    <img src="<?php bloginfo('stylesheet_directory')?>/img/oslob_tumalog/oslob_tumalog04.jpg" />
    <img src="<?php bloginfo('stylesheet_directory')?>/img/oslob_tumalog/oslob_tumalog05.jpg" />
    <img src="<?php bloginfo('stylesheet_directory')?>/img/oslob_tumalog/oslob_tumalog06.jpg" />
    <img src="<?php bloginfo('stylesheet_directory')?>/img/oslob_tumalog/oslob_tumalog07.jpg" />
  </div>
  </td></tr>
  </table>

  </div>

  <div id="tour_inclusions" style="overflow:hidden;">
  <table class="table table-condensed">
  <tr><th colspan="2" style="color:#ffffff;background-color:#008aa4">Package Inclusions</th></tr>  
  <tr><td colspan="2">
  <ul>  
<li>Breakfast (Menu list will be emailed for the choices)</li>
<li>Lunch with 1 drink (Menu list will be emailed for the choices)</li>
<li>Whale watching and Tumalog falls entrance</li>
<li>Boat ride and life vest</li>
<li>Whale shark watching on boat.</li>
<li>Tumalog Falls</li>
<li>Fully air-conditioned Car or Van Service</li>
  </ul>
  </td></tr>
  </table>
  </div>


  <div id="tour_itenerary" style="overflow:hidden;">
  <table class="table table-condensed">
  <tr><th colspan="2" style="color:#ffffff;background-color:#008aa4">Package Itenerary</th></tr>
  <tr><td colspan="2">Recommended pick up time from Cebu City or Lapu-lapu City is between 4:00AM to 5:30AM</td></tr>   
<tr><td style="width:25%">5:00 AM </td><td style="width:75%"> Pick up and departure time from Hotel </td></tr>
<tr><td>8:00 AM </td><td> Arrival at Oslob then Breakfast </td></tr>
<tr><td>9:00 AM </td><td> Whale Shark Watching </td></tr>
<tr><td>10:30 AM </td><td> Tumalog Falls </td></tr>
<tr><td>11:30 AM </td><td> Lunch </td></tr>
<tr><td>12:30 PM </td><td> Depart from Oslob </td></tr>
<tr><td>4:00 PM </td><td> Arrival Cebu City </td></tr>
  <tr><td colspan="2">In case you want to have side trip (upon arrival in Cebu City), we charge P 300.00/hr. </td></tr>   
  </table>
  </div>

  <div id="tour_day_tour" style="overflow:hidden;">
  <table class="table table-condensed">
  <tr><th colspan="2" style="color:#ffffff;background-color:#008aa4">Day Tour Service</th></tr>

  <tr><td colspan="2">1 Day private tour (11 hours duration)</td></tr>   
  <tr><td colspan="2">Tour Facilitator</td></tr>  
  <tr><td colspan="2">Fully air-conditioned vehicle service</td></tr>  
   <tr><td colspan="2">
     <ul>
     <li>1 to 3 persons – sedan type vehicle</li>
     <li>4 to 6 persons – minivan type vehicle</li>
     <li>7 to 13 persons – van type vehicle</li>
     <li>14 and above – 2 vehicles or it depends on the number of heads</li>
     </ul>
   </td></tr>
  </table>
  </div>


</div> <!-- col7 -->
<div class="col3">

<form id="form_compute" action="<?php bloginfo('stylesheet_directory')?>/ajax-handler-payment.php" method="post" style="padding:20px;background-color:#008aa4;color:#ffffff">
<input type="hidden" name="submitted" value="true">
<input type="hidden" name="tour_name" value="Oslob Whale Watching and Tumalog Falls Adventure">

<table class="table table-condensed table_compute" id="table_compute" >

<tr><td colspan="2" style="background-color:#000000;color:#ffffff" >Oslob Whale Watching &amp; Tumalog Falls Adventure</td></tr>

<tr><td colspan="2">Select Package</td></tr>
<tr><td colspan="2">
<select id="package_name" name="package_name">
</select>
</td></tr>

<tr><td colspan="2" style="background-color:gray; color:white;">
Number of Persons Details/Computation<br/>
<ul style="color:yellow">
</li>Children Below 3yrs old (Free but not included meal count)</li>
<li>All rates are subject to change without prior notice.</li>
</ul>
</td></tr>
<tr><td>No. of Adults</td>
<td>
    <select name="no_of_adults" id="no_of_adults" style="width:60%">
    <option val='1' selected>1</option>
    </select>
</td>
</tr>
<tr><td>Price/Head</td>
<td>
<input type="text" id="package_price_head" name="package_price_head" style="" readonly>
</td>
</tr>
<tr><td>Sub-total Adult</td>
<td>
<input type="text" id="total_price_head" name="total_price_head" style="" readonly>
</td>
</tr>
<tr><td>Children(3-5 yrs)</td>
<td>
    <select name="no_of_children" id="no_of_children" style="width:60%">
    <option val='0' selected>0</option>
    </select>
</td>
</tr>
<tr><td>Price Less Php400</td>
<td>
<input type="text" id="package_price_head_children" name="package_price_head_children" style="" readonly>
</td>
</tr>
<tr><td>Sub-total Children 3-5 yrs. old</td>
<td>
<input type="text" id="sub_total_children" name="sub_total_children" style="" readonly>
</td>
</tr>

<input type="hidden" name="other_payment_details" id="other_payment_details" value="">

<tr><td style="background-color:gray; color:white;">TOTAL NO. OF PERSONS</td><td><input type="text" id="no_of_persons" name="no_of_persons" style="" readonly> </td></tr>
<tr><td>
    Grand Total (Php)
</td><td>
   <input type="text" class="hilite_value" name="computed_total_price" id="computed_total_price" readonly>
</td></tr>

<tr style="background-color:gray"><td colspan="2" style="color:white;">
    Select Payment Mode Now:
</td></tr>
<tr><td colspan="2" style="padding:3px">
<input type="radio" name="payment_mode" value="20" checked> 20% Downpayment<br/>
<input type="radio" name="payment_mode" value="50"> 50% Downpayment<br/>
<input type="radio" name="payment_mode" value="full"> Full Payment<br/>
<input type="hidden" name="reservation_mode" id="reservation_mode" value="">
</td></tr>

<tr><td>
    Selected Payment Amount (Php)
</td><td>
<input type="text" class="hilite_value" name="downpayment" id="downpayment" readonly>
</td></tr>


        <tr><td colspan="2" style="background-color:gray; color:white;">Personal Information</td></tr>
        <tr><td style="width:30%">
        Firstname <span class="required">*</span>
        </td>
        <td style="width:70%">
        <input type="text" class="form-control" name="first_name" id="first_name" placeholder="Enter Firstname" required='required'>
        </td>
        </tr>
        <tr><td>
        Lastname: <span class="required">*</span>
        </td><td>
        <input type="text" class="form-control" name="last_name" id="last_name" placeholder="Enter Lastname" aria-required='true' required='required'>
        </td></tr>
        <tr><td>
        Contact: <span class="required">*</span>
        </td><td>
        <input type="text" class="form-control" name="contact" id="contact" placeholder="Enter Contact Number" aria-required='true' required='required'>
        </td></tr>
        <tr><td>
        Email: <span class="required">*</span>
        </td><td>
        <input type="email" class="form-control" name="email" id="email" placeholder="Enter Best Email" aria-required='true' required='required'>
        </td></tr>
        <tr><td>
        Pick Up Location: <span class="required">*</span>
        </td><td>
        <input type="text" class="form-control" name="pick_location" id="pick_location" placeholder="Airport/Hotel" aria-required='true' required='required'>
        </td></tr>
        <tr><td>
        Pick Up Date: <span class="required">*</span>
        </td><td>
        <input type="text" class="form-control" name="pick_date" id="pick_date" placeholder="mm/dd/yyyy" aria-required='true' required='required'>
        </td></tr>
        <tr><td>
        Pick Up Time: <span class="required">*</span>
        </td><td>
        <input type="text" class="form-control" name="pick_time" id="pick_time" placeholder="08:00 AM" aria-required='true' required='required'>
        </td></tr>
        <tr><td colspan="2">
        Additional Information
        <textarea class="form-control noresize" name="other_info" id="other_info"  rows="5" placeholder="Additional Info"></textarea>
        </td></tr>


<tr><td colspan="2" style="text-align:center">
<input type="image" name="submit" id="reserve_paypal" src="https://www.paypalobjects.com/webstatic/mktg/logo/bdg_now_accepting_pp_2line_w.png" border="0" alt="Submit" />
<!-- <input type="submit" value="Submit"> -->
 <span id="or"> or </span> 
<input  type="submit" value="RESERVE ONLY" id="reserve_14">
</td></tr>


</table>
<img src="https://www.paypalobjects.com/webstatic/mktg/logo/PP_AcceptanceMarkTray-NoDiscover_243x40.png" alt="Buy now with PayPal" />
</form>

<form action="https://www.paypal.com/cgi-bin/webscr" method="post" id="paypal_form">
<input type="hidden" name="cmd" value="_xclick">
<input type="hidden" name="business" value="epp2k12@gmail.com">
<input type="hidden" name="item_name" value="Kawasan Tours and Canyoneering">
<input type="hidden" name="item_number" value="123">
<input type="hidden" name="currency_code" value="PHP">
<input type="hidden" name="amount" id="payment_amount_for_paypal" value="">
<input type="hidden" name="first_name" value="John">
<input type="hidden" name="last_name" value="Doe">
<input type="hidden" name="address1" value="9 Elm Street">
<input type="hidden" name="address2" value="Apt 5">
<input type="hidden" name="city" value="Berwyn">
<input type="hidden" name="state" value="PA">
<input type="hidden" name="zip" value="19312">
<input type="hidden" name="night_phone_a" value="610">
<input type="hidden" name="night_phone_b" value="555">
<input type="hidden" name="night_phone_c" value="1234">
<input type="hidden" name="email" value="jdoe@zyzzyu.com">
<input type="image" id="paypal_submit" name="submit" border="0"
src="https://www.paypalobjects.com/en_US/i/btn/btn_buynow_LG.gif"
alt="PayPal - The safer, easier way to pay online" style="display:none">
</form>

</div> <!-- col3 -->

<div id="dialog_14" title="14 or more persons">
  <p>14 or more persons would require additional charges. Just fill the form and send your reservations. We will
  email you your actual billing.</p>
  <p>Note : Please indicate exact number of persons</p>
</div>



</div> <!-- container -->


<script type="text/javascript">
  var pkdescription = ['1 person','2 persons','3 persons','4-6 persons','7-9 persons','10-13 persons','14 and more persons'];
  var pkprice_per_head = [5700,3600,3000,2350,2100,1700,0];
  // var pkprice_per_head_pandanon = [6000,3800,3000,2600,2200,1950,0];
  

$(document).ready(function() {
  var pkval; //this is the package value that can be used inside this function
  var price_table=pkprice_per_head; // this is the look up table for computations


  //set up initial values during document load
  $('#form_compute').trigger('reset');
  $('#tour_name').val("Oslob Whale Watching and Tumalog Falls Adventure");
  for(i=0;i<pkdescription.length;i++) {
    $('#package_name').append('<option val="'+ i + '">' + pkdescription[i] + '</option>');
  }
  pkval = get_package_value();

  //set_num_select('#no_of_adults',1,100);
  //set_num_select('#no_of_children',0,50);

  compute_total(price_table,pkval);


  //set up the number of adults according to package selected
  $('#package_name').change(function(){
    $('#package_name option:selected').each(function(){ 
      // change global value of package value - pkval
      pkval = get_package_value();
      // alert( "the pkval is : " + pkval);
        $("#no_of_adults").empty(); // remove old options
        if(pkval==0) {
          set_num_select("#no_of_adults",1,1);
          set_num_select("#no_of_children",0,0);
        }else if (pkval==1) {
          set_num_select("#no_of_adults",2,2);
          set_num_select("#no_of_children",0,1);
        }else if (pkval==2) {
          set_num_select("#no_of_adults",3,3);
          set_num_select("#no_of_children",0,2);
        }else if(pkval==3) {
          set_num_select("#no_of_adults",4,6);
          set_num_select("#no_of_children",0,5);
        }else if(pkval==4)  {
          set_num_select("#no_of_adults",7,9);
          set_num_select("#no_of_children",0,8);
        }else if(pkval==5)  {
          set_num_select("#no_of_adults",10,13);
          set_num_select("#no_of_children",0,12);
        }else {
          set_num_select("#no_of_adults",14,100);
          set_num_select("#no_of_children",0,99);
          $( "#dialog_14" ).dialog( "open" );
        }
        if(pkval == 6) {
          $('#reserve_14').css({"display":"block"});
          $('#reserve_paypal').css({"display":"none"});
          $('#or').css({"display":"none"});
        }else{
          $('#reserve_14').css({"display":"initial"});
          $('#reserve_paypal').css({"display":"initial"}); 
           $('#or').css({"display":"initial"});             
        }

        //$('#package_price_head').val(pkprice_per_head[parseInt(pkval)])
    });
  compute_total(price_table,pkval);
  });

  $('#no_of_children').change(function() {
    compute_total(price_table,pkval);
    set_other_payment_details($('#no_of_children option:selected').val());
  });
  $('#no_of_adults').change(function() {
    compute_total(price_table,pkval);
  });

  $( 'input[name="payment_mode"]:radio' ).change(function() {
    //alert("inside the radio heads!");
        var pmode = this.value;
        set_payment_mode(pmode);
  });


  $('#reserve_paypal').click(function() {
    $('#reservation_mode').val("reserve with paypal payment");
  });

  $('#reserve_14').click(function() {
    $('#reservation_mode').val("reserve only");
  });

  $('#form_compute').submit(function(event){

      var sdata= $(this).serializeArray();

      //alert("inside the maze of submit :" + sdata);
      var pathname = document.location.origin + '/alvin_wp01/js_ajax/ajax-handler-payment.php';

      // alert("inside the great labyrinth!");
      // alert("the pkval : " + pkval);

      $.ajax({
        "type":"POST",
        "url":pathname,
        "data":sdata,
        //"data":"var1=Avocado&var2=Banana&var3=Orange",
        "success":function(data) {

          // alert("the data : " + data);
          if(pkval != 6) {
            $('#paypal_form').submit();
          }else{
          $('#form_compute').trigger('reset');
          $('#reserve_14').css({"display":"initial"});
          $('#reserve_paypal').css({"display":"initial"});
          }
        }
      });
      event.preventDefault();
  });


  //triggers the kawasan image gallery ------------------------------------------------ image gallery
  setInterval('swapImages()', 5000);


  //triggers pop up dialog box for more than 14 orders  ---------------------------------------------

    // for pop-up dialog box 
    $( "#dialog_14" ).dialog({
      autoOpen: false,
      modal : true,
      show: {
        effect: "blind",
        duration: 1000
      },
      hide: {
        effect: "explode",
        duration: 1000
      }
    });
    




});


//not shared functions
function compute_total(price_table,pkval) {
  var price_per_head_adult=0;
  var sub_price_adult=0;
  var price_per_head_children=0;
  var sub_price_children=0;
  var computed_total=0;

  if(pkval != 6) {

  //get price per hed for adult
  price_per_head_adult = price_table[parseInt(pkval)];
  $('#package_price_head').val(price_per_head_adult);
  sub_price_adult = $('#no_of_adults').val() * price_per_head_adult;
  $('#total_price_head').val(sub_price_adult);
  price_per_head_children = price_per_head_adult - 400;
  $('#package_price_head_children').val(price_per_head_children);
  sub_price_children = $('#no_of_children').val() * price_per_head_children;
  $('#sub_total_children').val(sub_price_children);
  // alert('the pkval inside compute total is : ' + pkval);

  $('#no_of_persons').val(parseInt($('#no_of_adults').val()) + parseInt($('#no_of_children').val()));

  computed_total = sub_price_adult + sub_price_children;
  //alert( additional_charge + "--" + computed_total);
    $('#computed_total_price').val(computed_total);
  set_payment_mode( $("input[name='payment_mode']:checked").val() );

  } else {
    $('#package_price_head').val(0);
    $('#total_price_head').val(0);
    $('#package_price_head_children').val(0);
    $('#sub_total_children').val(0);
    $('#computed_total_price').val(0);
    $('#payment_amount_for_paypal').val(0);
  }


}


function set_other_payment_details(num_children) {

  // alert("inside the great function!");
  var other_payment_details="";
  $("form input[type='checkbox']").each(function() {

    if($(this).prop('checked')) {
      var dname = $(this).attr('name');
      other_payment_details += $('label[for=' + dname + ']').text();
      other_payment_details += ";"; 
   }
  });

  if (typeof num_children === "undefined" || num_children === null) { 
      other_payment_details += "The number of children 3 - 5 yrs old = " + '';
  } else {
      other_payment_details += "The number of children 3 - 5 yrs old = " + num_children;
  }

  // alert("the other payment detail: " + other_payment_details);
  $('#other_payment_details').val(other_payment_details);
}




//shared functions
function get_package_value() {

    for(i=0;i<pkdescription.length;i++) {
      if(pkdescription[i] == $('#package_name option:selected').val()) return i;
    }
}

function set_payment_mode( pmode ) {

        var computed_total = $('#computed_total_price').val();
        var payment_amount = computed_total;
        //alert("computed total is : " + computed_total);
        if(pmode == "20" ) {
          payment_amount = computed_total *.2;
          $('#downpayment').val(payment_amount);
        }
        else if(pmode == "50") {
          payment_amount = computed_total *.5;
          $('#downpayment').val(payment_amount);
        } 
        else {
          $('#downpayment').val(payment_amount);
        }
        $('#payment_amount_for_paypal').val(payment_amount);
    
}

function set_num_select(control_name,begin_num,end_num) {
  // alert("inside the maze!" + control_name);

  var $el = $(control_name);
  $el.empty();
   for(i=begin_num;i<=end_num;i++) {
     $el.append('<option val="'+i+'">'+i+'</option>');
   }
 
}

</script>

<?php // spacious_sidebar_select(); ?>
<?php get_footer(); ?>
