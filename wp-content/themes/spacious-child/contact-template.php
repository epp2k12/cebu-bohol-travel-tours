<?php
/*
Template Name: contact-template
*/
get_header();
?>

<div class="col-sm-7">
<div class="mid_blue_title_bars" >SEND US YOUR THOUGHTS!</div>
<?php echo do_shortcode( '[contact-form-7 id="110" title="Contact form 1"]' ); ?>

</div> <!-- packgroup -->

<div class="col-sm-5">

<iframe frameborder="0" scrolling="no" marginheight="0" marginwidth="0"width="700" height="440" src="https://maps.google.com/maps?hl=en&q=Timpolok, Lapu-Lapu City&ie=UTF8&t=m&z=15&iwloc=B&output=embed"><div><small><a href="http://embedgooglemaps.com">
									embed google map
							</a></small></div><div><small><a href="https://www.googlemapsgenerator.com/">generate Google Maps</a></small></div></iframe>

<?php
include 'common/side-bar-pages.php';
?>

  <!--
  <div id="myContent" style="margin:0 auto">
  <ul>
    <li class="active"><h1>HTML 01</h1></li>
        <li><h1>HTML 02</h1></li>
            <li><h1>HTML 03</h1></li>
                <li><h1>HTML 04</h1></li>
                    <li><h1>HTML 05</h1></li>
  </ul>
  </div>
  -->

</div>

<script type="text/javascript">
$(document).ready(function(){

setInterval('swapImages("myGallery")', 5000);

	$('.white_box').hover(function(){
		$(this).css(
		{
			'background-color': 'rgba(0, 0, 255, 0.7)'
		}

		);
		$(this).find('p').css (
		{
			'color':'#ffffff'
		}
		);
	},function(){
		$(this).css(
		{
			'background-color': 'rgba(255, 255, 255, 0.3)'
		}

		);
		$(this).find('p').css (
		{
			'color':'#000000'
		}
		);
	});

});

</script>

<?php // spacious_sidebar_select(); ?>
<?php get_footer(); ?>
