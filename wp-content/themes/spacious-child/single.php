<?php
/**
 * Theme Single Post Section for our theme.
 *
 * @package ThemeGrill
 * @subpackage Spacious
 * @since Spacious 1.0
 */
?>

<?php get_header(); ?>

	<?php do_action( 'spacious_before_body_content' ); ?>
	<?php
	global $post;
	$this_slug = $post->post_name;
	// echo '<h1> The POST ID: '. $this_slug . '</h1>';
	?>

	<div id="primary">
		<div id="content" class="clearfix">

    <?php if( 'oslob-sumilon' == $this_slug) : ?>
    	<?php get_template_part('parts/package','oslob-sumilon'); ?>

    <?php elseif( 'canyoneering-day-tour' == $this_slug): ?>
		<?php get_template_part('parts/package','kawasan-falls'); ?>

    <?php elseif( 'mactan-island-hopping' == $this_slug): ?>
		<?php get_template_part('parts/package','mactan-island-hopping'); ?>

	<?php elseif( 'oslob-tumalog' == $this_slug): ?>
		<?php get_template_part('parts/package','oslob-tumalog'); ?>

	<?php elseif( 'cebu-tour-package-day' == $this_slug): ?>
		<?php get_template_part('parts/package','city-tour'); ?>

	<?php elseif( 'oslob-whale-shark-and-kawasan-canyoneering' == $this_slug): ?>
		<?php get_template_part('parts/package','oslob-kawasan'); ?>

	<?php elseif( 'canyoneering-cebu-whale-shark-tumalog' == $this_slug): ?>
		<?php get_template_part('parts/package','oslob-kawasan-tumalog'); ?>

	<?php elseif( 'bohol-countryside-tours' == $this_slug): ?>
		<?php get_template_part('parts/package','bohol-countryside'); ?>

	<?php elseif( 'oslob-overnight-2d1n-full-adventure-package' == $this_slug): ?>
		<?php get_template_part('parts/package','oslob-overnight-2d1n-full-adventure-package'); ?>

	<?php elseif( 'oslob-overnight-2d-1n-budget-package' == $this_slug): ?>
		<?php get_template_part('parts/package','oslob-overnight-2d-1n-budget-package'); ?>

	<?php elseif( 'oslob-whale-watching-sumilon-island-and-kawasan-canyoneering' == $this_slug): ?>
		<?php get_template_part('parts/package','oslob-kawasan-sumilon'); ?>

	<?php elseif( 'malapascua-island-day-tour' == $this_slug): ?>
		<?php get_template_part('parts/package','malapascua'); ?>

	<?php elseif( 'kalanggaman-island-day-tour' == $this_slug): ?>
		<?php get_template_part('parts/package','kalanggaman'); ?>

	<?php elseif( 'overnight-kalanggaman-malapascua-island-package' == $this_slug): ?>
		<?php get_template_part('parts/package','kalanggaman-malapascua'); ?>

	<?php elseif( 'moalboal-pescador-island-hopping-kawasan-canyoneering' == $this_slug): ?>
		<?php get_template_part('parts/package','pescador-canyoneering'); ?>

	<?php elseif( 'cebu-and-bohol-3days2nights-package' == $this_slug): ?>
		<?php get_template_part('parts/package','cebubohol3d2n'); ?>

	<?php elseif( '4days-3nights-cebu-tour-package' == $this_slug): ?>
		<?php get_template_part('parts/package','cebu4d3n'); ?>

	<?php elseif( '5days-4nights-cebu-bohol-tour-package' == $this_slug): ?>
		<?php get_template_part('parts/package','cebubohol5d4n'); ?>

	<?php elseif( '6days-5nights-cebu-bohol-ultimate-tour-package' == $this_slug): ?>
		<?php get_template_part('parts/package','cebubohol6d5n'); ?>

    <?php else: ?>


			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', 'single' ); ?>

				<?php get_template_part( 'navigation', 'archive' ); ?>

				<?php
					do_action( 'spacious_before_comments_template' );
					// If comments are open or we have at least one comment, load up the comment template
					if ( comments_open() || '0' != get_comments_number() )
						comments_template();
	      		do_action ( 'spacious_after_comments_template' );
				?>

			<?php endwhile; ?>

    <?php endif; ?>

		</div><!-- #content -->
	</div><!-- #primary -->








	<?php do_action( 'spacious_after_body_content' ); ?>
	<?php spacious_sidebar_select(); ?>

<?php get_footer(); ?>