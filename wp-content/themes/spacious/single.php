<?php 
/**
 * Theme Single Post Section for our theme.
 *
 * @package ThemeGrill
 * @subpackage Spacious
 * @since Spacious 1.0
 */
?>

<?php get_header(); ?>

	<?php do_action( 'spacious_before_body_content' ); ?>
	<?php
	global $post;
	$this_slug = $post->post_name;
	// echo '<h1> The POST ID: '. $this_slug . '</h1>';
	?>


    <?php if( 'oslob-sumilon' == $this_slug) : ?>
    	<?php get_template_part('parts/package','oslob-sumilon'); ?>

    <?php elseif( 'kawasan-falls' == $this_slug): ?>
		<?php get_template_part('parts/package','kawasan-falls'); ?> 

    <?php elseif( 'mactan-island-hopping' == $this_slug): ?>
		<?php get_template_part('parts/package','mactan-island-hopping'); ?> 

	<?php elseif( 'oslob-tumalog' == $this_slug): ?>
		<?php get_template_part('parts/package','oslob-tumalog'); ?> 

    <?php else: ?>

	<div id="primary">
		<div id="content" class="clearfix">
			<?php while ( have_posts() ) : the_post(); ?>
			
				<?php get_template_part( 'content', 'single' ); ?>

				<?php get_template_part( 'navigation', 'archive' ); ?>

				<?php
					do_action( 'spacious_before_comments_template' );
					// If comments are open or we have at least one comment, load up the comment template
					if ( comments_open() || '0' != get_comments_number() )
						comments_template();					
	      		do_action ( 'spacious_after_comments_template' );
				?>

			<?php endwhile; ?>

		</div><!-- #content -->
	</div><!-- #primary -->


    <?php endif; ?>







	

	
	<?php do_action( 'spacious_after_body_content' ); ?>
	<?php spacious_sidebar_select(); ?>

<?php get_footer(); ?>