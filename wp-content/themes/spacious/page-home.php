<?php
get_header();
?>

<style type="text/css">

* {
    box-sizing: border-box;
}

#secondary {
 border: 1px solid #008aa4;
 padding: 20px;	
 width:30%;
}

#packside {
 border: 1px solid #008aa4;
 padding: 20px;
}

.dpackpost {
border: 1px solid #008aa4;
}

.dtitle {
padding: 3px ;
background-color:#008aa4; 
color: #e0e0e0;
font-size:1.2em;
overflow:auto;
}

@media only screen and (min-width: 768px) {

#packgroup {
 float:left;
 width:70%;
}
#packside {
 float:left;
 width:30%;	
}

.dpackpost {
margin:0 5px 5px 5px;
}


	.tbnail {
		float:left;
		width:35%;
	}
	.dcntnt {
		float:left;
		width:63%;
	}

}

@media only screen and (max-width: 768px ) {

#secondary {
 border: 1px solid #008aa4;
 padding: 20px;	
 width:100%;
}

#packgroup {
 width:100%;
 margin-bottom:5px;
}
#packside {
 width:100%;
 margin-bottom:5px;
}

.dpackpost {
margin:0px;
}

.dcntnt {
	width:100%;
}

.tbnail {
	width:100%;
}

}

.dcntnt p{
	font-size: .9em;
	line-height:140%;
	margin:15px;
	color:#474747;
}

.tbnail img {
	border:1px solid gray;
	padding:2px;
	margin:5px;
}




#myCarousel {
	margin:0px;
	padding:0px;
}

.carousel-inner {
	margin:0px;
	padding:0px;
	width:100%;
  	max-height: 475px !important;
}


</style>




<div id="packgroup">

<?php 
$args=array(
'category' => 3
);
$packagePosts = new WP_Query( $args );

	if( $packagePosts->have_posts() ) {
	//loop through related posts based on the tag
		while ( $packagePosts->have_posts() ) :
			$packagePosts->the_post(); 
?>

<div class="dpackpost">

	<a href="<?php the_permalink(); ?>">
	<div class="dtitle">	
		<?php the_title(); ?>
		<!-- <button type="button" class="btn btn-primary" style="float:right">Warning</button> -->
	</div>

	<div class="tbnail">
	<?php the_post_thumbnail(); ?>
	</div>
	</a>
	<div class="dcntnt">
		<?php the_content(); ?>
	</div>
	<div style="clear:both"></div>


</div>



<?php 
endwhile; 
} 
?>

</div> <!-- packgroud -->

<?php wp_reset_postdata(); ?>

<div id="packside" style="margin:0px;">
<h1> THIS SPACE IS STILL UNDER CONSTRUCTION</h1>
<h2> THIS SPACE IS STILL UNDER CONSTRUCTION</h2>
<h3> THIS SPACE IS STILL UNDER CONSTRUCTION</h3>
<h2> THIS SPACE IS STILL UNDER CONSTRUCTION</h2>
<h1> THIS SPACE IS STILL UNDER CONSTRUCTION</h1>
</div>

<?php // spacious_sidebar_select(); ?>
<?php get_footer(); ?>
